//
//  PlaceViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/27/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation



class PlaceViewModel {
    //MARK:- Properties
    fileprivate var place: Place? = nil
    
    
    //MARK:- Initialization
    
    init(withPlace place: Place?) {
        self.place = place
    }
    
}


extension PlaceViewModel {

    
    var getName: String? {
        guard let placeName = place?.name  else { return nil }
        let name = Utils.getLocaleField(dict: placeName)
        return name

    }
    
    var getImageURL: String? {
        if let imageUrl = place?.imgUrl {
            let url = SakeWizUrlUtils.getMainImageUrl(imgUrl: imageUrl)
            return url
        }
        return nil
    }
    
    var getAddress: String? {
        
        if let address = place?.address {
            let components = address.components(separatedBy: ",")
            if components.count > 0 {
                let firstElement = components.first?.convertHtmlToPlainText()
                return firstElement
            }
            
            return address.convertHtmlToPlainText()
        }
        
        return nil
    }
    
    
    var getRating: Float {
        guard let rate = place?.rate else { return 0 }
        return rate
    }
    
    var getHasNote: Bool {
        
        if let hasNote = place?.hasUserNote {
            return hasNote ? true : false
        }
        
        return false
    }
    
    var getCanGainPoints: Bool {
        
        if let canGainPoints = place?.canGainPoints {
            return canGainPoints ? true : false
        }
        
        return false
    }
    
}

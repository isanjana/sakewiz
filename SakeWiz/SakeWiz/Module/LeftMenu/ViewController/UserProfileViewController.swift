//
//  UserProfileViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/23/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class UserProfileViewController: BaseViewController {

    //MARK:- Properties
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var userRankLabel: UILabel!
    @IBOutlet weak var wizImageView: UIImageView!
    @IBOutlet weak var mottoLabel: UILabel!
    @IBOutlet weak var followersCountLabel: UILabel!
    @IBOutlet weak var followingCountLabel: UILabel!
    @IBOutlet weak var reviewCountLabel: UILabel!
    @IBOutlet weak var favProductsCountLabel: UILabel!
    @IBOutlet weak var favPlacesCountLabel: UILabel!
    @IBOutlet weak var followButton: SWButton!
    var userProfileViewModel :UserProfileViewModel!
   
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initailSetUp()
        fetchUserProfile()
    }
    
    //MARK:- Fetch User Profile
    func fetchUserProfile() {
        showProgress(title: "")
        userProfileViewModel.fetchUserProfile { (error) in
            self.hideProgress()
            if error != nil {
                if DEBUG.isDebuggingEnabled {
                    self.showBannerAlert(title: error!.getTitle(), message: error!.getMessage(), style: .warning, delegate: nil)
                }
                return;
            }
            
            self.setUserDataOnUI()
        }
    }
    
    //MARK:- Follow Unfollow
    @IBAction func followUnfollowDidClicked(_ sender: Any) {
        userProfileViewModel.followUnfollowDidClicked()
    }
    
    override func refreshUI() {
        fetchUserProfile()
    }
    
    //MARK:- Initial SetUp
    func initailSetUp() {
        profileImageView.circular(borderColor: .clear, borderWidth: 0)
        if userProfileViewModel.fromMenu {
            addHambergerButton()
        }else{
            addBackButton()
        }
        clearData()
        
    }
    
    //MARK:- set User Data
    func setUserDataOnUI() {
        
        guard let userViewModel = userProfileViewModel.getUserViewModel() else { return }
        
        //Name
        if let name = userViewModel.getName {
            userNameLabel.text = name
            self.title = String(format: "user_profile_title".localized(), name.uppercased())
        }
        
        //Profile Image
        if let userProfileImageUrl = userViewModel.getProfileImageUrl {
            profileImageView.imageFromUrl(userProfileImageUrl, showLoader: true)
        }
        
        //Place
        placeLabel.text = userViewModel.getPlace
        
        //User Rank
        if let rank = userViewModel.getRank {
            userRankLabel.text = rank.localized()
        }
        
        if let rankImageUrl = userViewModel.getRankImageUrl {
            wizImageView.imageFromUrl(rankImageUrl, showLoader: false)
        }
        
        //Motto
        mottoLabel.text = userViewModel.getMotto
        
        //Followers
        if let followesCont = userViewModel.getFollowersCount {
            followersCountLabel.text = followesCont
        }
        
        //Following
        if let follows = userViewModel.getFollowingCont {
            followingCountLabel.text = follows
        }
        
        //Review
        if let reviewCount = userViewModel.getReviewCount {
            reviewCountLabel.text = reviewCount
        }
        
        //Fav Products
        if let favProductCount = userViewModel.getFavProductCount {
            favProductsCountLabel.text = favProductCount
        }
        
        //Fav Places
        if let favPlaceCount = userViewModel.getFavPlaceCount {
            favPlacesCountLabel.text = favPlaceCount
        }
        
        followButton.isHidden = userViewModel.isCurrentUser
        
        // Follow/ Unfollow
        let followTitle = userViewModel.isUserFollowing ? "unfollow".localized() : "follow".localized()
        followButton.setTitle(followTitle, for: .normal)
        
    }
    
    func clearData() {
        userNameLabel.text = ""
        placeLabel.text = ""
        userRankLabel.text = ""
        mottoLabel.text = ""
        followersCountLabel.text = ""
        followingCountLabel.text = ""
        reviewCountLabel.text = ""
        favProductsCountLabel.text = ""
        favPlacesCountLabel.text = ""
        wizImageView.image = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

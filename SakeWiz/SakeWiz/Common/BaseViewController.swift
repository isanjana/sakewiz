//
//  BaseViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class BaseViewController: UIViewController {

    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EventBus.onMainThread(self, name: TaskName.RefreshUI) { (notification) in
            self.refreshUI()
        }
        
        EventBus.onMainThread(self, name: TaskName.ShowUserProfileViewController) { (notification) in
            if let userInfo = notification.userInfo  {
                if let userID = userInfo["id"] as? String {
                    self.showUserProfileVC(userID: userID)
                }
            }
        }
        
        EventBus.onMainThread(self, name: TaskName.ShowProductDetailsController) { (notification) in
            if let userInfo = notification.userInfo  {
                if let prodID = userInfo["id"] as? String {
                    self.showProductDetails(prodID: prodID)
                }
            }
        }
        
        EventBus.onMainThread(self, name: TaskName.ShowPlaceDetailsController) { (notification) in
            if let userInfo = notification.userInfo  {
                if let placeID = userInfo["id"] as? String, let type = userInfo["type"] as? EntityType {
                    self.showPlaceDetails(entityType: type, placeID: placeID)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        EventBus.unregister(self)
    }
    
    func refreshUI() {
        
    }
    
    func setScreenTitle(_ title:String) {
        self.title = title.localized()
    }
    
    //MARK: - Navigation Bar Components
    //MARK:- Back Button
    func addBackButton() {
        
        let image = UIImage(named: "Back")
        let backBtn: UIButton = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: image!.size.width, height: image!.size.height)
        backBtn.setImage(image, for: .normal)
        backBtn.addTarget(self, action:#selector(self.goToLastController), for: .touchUpInside)
        let backBtnItem: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backBtnItem
    }
    
    func goToLastController() {
        
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    //MARK:- Hamberger Button
    func addHambergerButton() {
        self.addLeftBarButtonWithImage(UIImage(named: "Hamberger")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.addLeftGestures()
    }
        
    func addRightBarButton(imageName:String, selector:Selector) {
        
        let image = UIImage(named: imageName)
        let backBtn: UIButton = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: image!.size.width, height: image!.size.height)
        backBtn.setImage(image, for: .normal)
        backBtn.addTarget(self, action:selector, for: .touchUpInside)
        let backBtnItem: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.rightBarButtonItem = backBtnItem
    }
    
    func addRightBarButton(title:String, selector:Selector?) {
        
        let rightBarButton = UIBarButtonItem(title: title, style: .plain, target: self, action: selector)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    
    
    func getVisibleCellForIndex(tableView: UITableView, row:Int, section:Int) -> UITableViewCell? {
        
        let indexPath = IndexPath(row: row, section: section)
        
        if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows {
            if(indexPathsForVisibleRows.contains(indexPath)) {
                let cell = tableView.cellForRow(at: indexPath)
                if (cell != nil){
                    return cell!
                }
            }
        }
        
        return nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension BaseViewController : NotificationBannerDelegate {
    
    //MARK:- NotificatBannerDelegate
    public func notificationBannerWillAppear(_ banner: BaseNotificationBanner) {
    }
    
    public func notificationBannerDidAppear(_ banner: BaseNotificationBanner) {
    }
    
    public func notificationBannerDidDisappear(_ banner: BaseNotificationBanner) {
    }
    
    public func notificationBannerWillDisappear(_ banner: BaseNotificationBanner) {
        onBannerDismiss()
    }
    
    func onBannerDismiss() {
        
    }
    
    func showUserProfileVC(userID:String) {
        let menuStoryBoard = UIStoryboard(name: StoryboardName.LeftMenu, bundle: nil)
        let profileVC = menuStoryBoard.instantiateViewController(withIdentifier: String.className(UserProfileViewController.self)) as! UserProfileViewController
        let userProfileViewModel = UserProfileViewModel(withUserId: userID)
        profileVC.userProfileViewModel = userProfileViewModel
        navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func showProductDetails(prodID:String) {
        let dashboardStoryBoard = UIStoryboard(name: StoryboardName.Dashboard, bundle: nil)
        let productDetailsVC = dashboardStoryBoard.instantiateViewController(withIdentifier: String.className(ProductDetailsViewController.self)) as! ProductDetailsViewController
        navigationController?.pushViewController(productDetailsVC, animated: true)
    }
    
    func showPlaceDetails(entityType:EntityType, placeID: String) {
        let placeDetailsStoryBoard = UIStoryboard(name: StoryboardName.PlaceDeatails, bundle: nil)
        let placeDetailsVC = placeDetailsStoryBoard.instantiateViewController(withIdentifier: String.className(PlaceDetailsViewController.self)) as! PlaceDetailsViewController
        let placeDetailsViewModel = PlaceDetailsViewModel(withEntityType: entityType, palceID: placeID)
        placeDetailsVC.placeDetailsViewModel = placeDetailsViewModel
        navigationController?.pushViewController(placeDetailsVC, animated: true)
    }
    
    
}

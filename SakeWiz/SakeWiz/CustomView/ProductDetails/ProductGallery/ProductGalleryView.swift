//
//  ProductGalleryView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/18/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

@IBDesignable class ProductGalleryView: NibView {
    
    //MARK:- Properties
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageContol: UIPageControl!
    var imageAaray:[String] = []


    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createGallery()
        pageContol.numberOfPages = imageAaray.count
    }
    
    func updateViewForPlaceDetail(viewModel:PlaceDetailsViewModel) {
        if let imageAaray = viewModel.getPlaceImages {
            self.imageAaray = imageAaray
        }
    }
    
    //MARK:- Create Gallery
    func createGallery() {
       
        let size = CGSize(width: 75, height: 75)
        let gap:CGFloat = 10
        
        for  index in stride(from: 0, to: imageAaray.count, by: 1) {
            //calculate frame
            
            var frame = CGRect.zero
            frame.origin.x = CGFloat(index) * (size.width + gap) + gap
            frame.origin.y = gap
            frame.size.width = size.width
            frame.size.height = size.height
            
            //image properties
            let productImageView = UIImageView(frame: frame)
            productImageView.tag = index
            productImageView.isUserInteractionEnabled = true
            if let imageUrl = SakeWizUrlUtils.getMainImageUrl(imgUrl: imageAaray[index]) {
                productImageView.imageFromUrl(imageUrl)
            }
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(profileImageDidClicked(_:)))
            productImageView.addGestureRecognizer(tapGesture)
            
            scrollView.addSubview(productImageView)
        }
        
        //ScrooView contentSize
        scrollView.contentSize = CGSize(width: CGFloat(size.width) * CGFloat(imageAaray.count), height:scrollView.bounds.size.height)
    }
    
    
    //MARK:- Prodcut Image Did Clicked
    func profileImageDidClicked(_ sender:UITapGestureRecognizer) {
        /*if let imageView = sender.view as? UIImageView {
          
        }*/
    }

}

//
//  SWTextFiled.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/6/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import UITextField_Navigation

@IBDesignable class SWTextFiled: UITextField {

    //MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        addDropShadowEffect()
    }

}


//
//  LatestRecommendationCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/18/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import ExpandableLabel

class LatestRecommendationCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var titleLabel: ExpandableLabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var emailImageView: UIImageView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var lngStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    //MARK:- Configure Cell
    func configureCell(viewModel:PlaceDetailsViewModel) {
        titleLabel.text = viewModel.getLongDesc
        titleLabel.layoutIfNeeded()
        titleLabel.shouldCollapse = true
        titleLabel.numberOfLines =  4
        titleLabel.collapsed = true
        placeLabel.text = viewModel.getAddress
        phoneLabel.text = viewModel.getTelephone
        emailLabel.text = viewModel.getEmail
        timeLabel.text = viewModel.getOpeningHours
        
        if let supportedLngs = viewModel.getSupportedLanguages {
            let lngCodes = LocaleConstant.LANGUAGES_CODE
            var index : Int = 0
            var found : Bool
            
            for code in lngCodes {
                found = false
                
                for lng in supportedLngs {
                    if code == lng {
                       found = true
                        break
                    }
                }
                
                if !found {
                    if let subview = lngStackView.subviews[index] as? UIImageView {
                        subview.removeFromSuperview()
                    }
                    
                }
                else {
                    index += 1
                }
            }
        }

    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  OAuthHandler.swift
//  SakeWiz
//
//  Created by sanjana on 12/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import Alamofire

class OAuthHandler: RequestAdapter, RequestRetrier {
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?, _ refreshToken: String?) -> Void
    
    private let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        return SessionManager(configuration: configuration)
    }()
    
    private let lock = NSLock()
    
    private var accessToken: String
    
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    
    // MARK: - Initialization
    
    public init(accessToken: String) {
        self.accessToken = accessToken
    }
    
    // MARK: - RequestAdapter
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
            var urlRequest = urlRequest
            urlRequest.setValue(accessToken, forHTTPHeaderField: ServiceConstant.X_AUTH_TOKEN)
            return urlRequest
    }
    
    // MARK: - RequestRetrier
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)
            
            if !isRefreshing {
                refreshTokens { [weak self] succeeded, accessToken, refreshToken in
                    guard let strongSelf = self else { return }
                    
                    strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }
                    
                    if let accessToken = accessToken, let _ = refreshToken {
                        strongSelf.accessToken = accessToken
//                        strongSelf.refreshToken = refreshToken
                    }
                    
                    strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    strongSelf.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(false, 0.0)
        }
    }
    
    // MARK: - Private - Refresh Tokens
    
    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }
        
        isRefreshing = true
        
//        let urlString = "\(baseURLString)/oauth2/token"
//        
//        let parameters: [String: Any] = [
//            "access_token": accessToken,
//            "refresh_token": refreshToken,
//            "client_id": clientID,
//            "grant_type": "refresh_token"
//        ]
//        
//        sessionManager.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default)
//            .responseJSON { [weak self] response in
//                guard let strongSelf = self else { return }
//                
//                if
//                    let json = response.result.value as? [String: Any],
//                    let accessToken = json["access_token"] as? String,
//                    let refreshToken = json["refresh_token"] as? String
//                {
//                    completion(true, accessToken, refreshToken)
//                } else {
//                    completion(false, nil, nil)
//                }
//                
//                strongSelf.isRefreshing = false
//        }
    }
}

//
//  UnknownSakeResultViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/21/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation

class UnknownSakeResultViewModel {
    
    //MARK:- Properties
   fileprivate var unknownSake: UnIdentifiedSake? = nil
    
    
    //MARK:- Initialization
    
    init(withUnknownSake unknownSake: UnIdentifiedSake?) {
        self.unknownSake = unknownSake
    }
    
}


extension UnknownSakeResultViewModel {
    
    var getScanImageUrl: String? {
        guard let url = SakeWizUrlUtils.getScanImageUrl(imgUrl: unknownSake?.scanImg) else { return nil }
        return url
    }
    
    var getScanedUserHndl: String? {
        guard let hndl = unknownSake?.scannedUserHndl else { return nil }
        return hndl
    }
    
    var getCreatedTime: String? {
        
        guard let createdTime = unknownSake?.created else { return nil }
        let date = Date(timeIntervalSince1970: TimeInterval(createdTime/1000))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        let displayDate = dateFormatter.string(from: date)
        return displayDate
    }
    
}

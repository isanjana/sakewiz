//
//  ProductViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/14/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation

class ProductViewModel {
    
    //MARK:- Properties
    fileprivate var product: Product? = nil
    
    
    //MARK:- Initialization
   
    init(withProdut product: Product?) {
        self.product = product
    }
    
}

extension ProductViewModel {
    
    var productImageUrl: String? {
        
        if product?.mainImgUrl.characters.count == 0 {
            if let mainImg = product?.mainImg {
                if let count = product?.imgs.count {
                    if count > 0 && mainImg < count {
                        return SakeWizUrlUtils.getMainImageUrl(imgUrl: product?.imgs[mainImg])
                    }
                }
            }
        }
        return SakeWizUrlUtils.getMainImageUrl(imgUrl: product?.mainImgUrl)
    }
    
    var getName: String? {
        guard let name = product?.name else { return nil }
        let prodcutName = Utils.getLocaleField(dict: name)
        return prodcutName
    }
    
    var getPalce: String? {
        if let countryName = product?.countryName {
            if let regionName = product?.regionName {
                let region = Utils.getLocaleField(dict: regionName)
                let country = Utils.getLocaleField(dict: countryName)
                let place = region.appending(", ").appending(country)
                return place
            }
        }
        
        if let regionName = product?.regionName {
            let region = Utils.getLocaleField(dict: regionName)
            return region
        }
        
        return nil
    }
    
    var getTime :String? {
        
        if product?.updated == nil {
            if let created = product?.created {
                let date = NSDate(timeIntervalSince1970: TimeInterval(created/1000))
                let timeAgo = Utils.timeAgoSinceDate(date: date, numericDates: true)
                return timeAgo
            }
        }
        
        if let updated = product?.updated {
            let date = NSDate(timeIntervalSince1970: TimeInterval(updated/1000))
            let timeAgo = Utils.timeAgoSinceDate(date: date, numericDates: true)
            return timeAgo
        }
        return nil
    }
    
    var getType: String? {
        guard let type = product?.type else { return nil }
        return type.localized()
    }
    
    var getRate: Float? {
        guard let rate = product?.rate else { return nil }
        return rate
    }
    
    var getReviewCount: String? {
        guard let count = product?.reviewCount else { return nil }
        return String(format: "%d", count)
    }
    
    var getFavouredCount: String? {
        guard let count = product?.favoured else { return nil }
        return String(format: "%d", count)
    }
    
    var hasUserNote: Bool {
        return product?.hasUserNote == true ? true : false
    }
    
    var canGainPoints: Bool {
        return product?.canGainPoints == true ? true : false
    }
    
}





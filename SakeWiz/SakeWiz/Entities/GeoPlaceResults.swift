//
//  GeoPlaceResults.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

class GeoPlaceResults: Mappable {
    
    var results:[GeoPlace] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        results <- map["results"]
    }
}

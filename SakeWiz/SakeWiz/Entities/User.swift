//
//  User.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class User: Mappable {
    
    var id: String?
    var hndl: String?
    var pwd: String?
    var address: String?
    var type: String?
    var email: String?
    var name: String?
    var motto: String?
    var pic: String?
    var pnts: Int?
    var country: String?
    var state: String?
    var lang: String?
    var active: Bool?
    var created: Int64?
    var updated: Int64?
    var userRank: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        hndl <- map["hndl"]
        pwd <- map["pwd"]
        address <- map["address"]
        type <- map["type"]
        email <- map["email"]
        name <- map["name"]
        motto <- map["motto"]
        pic <- map["pic"]
        pnts <- map["pnts"]
        country <- map["country"]
        state <- map["state"]
        lang <- map["lang"]
        active <- map["active"]
        created <- map["created"]
        updated <- map["updated"]
        userRank <- map["userRank"]
    }
    
    
}



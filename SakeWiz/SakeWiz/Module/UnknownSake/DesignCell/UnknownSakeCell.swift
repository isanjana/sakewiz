//
//  UnknownSakeCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/20/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class UnknownSakeCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    
    @IBOutlet weak var unknownSakeView: UnknownSakeView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func configureCell(viewModel: UnknownSakeResultViewModel) {
        unknownSakeView.updateView(viewModel: viewModel)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

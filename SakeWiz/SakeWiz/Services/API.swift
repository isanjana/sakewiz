//
//  API.swift
//  SakeWiz
//
//  Created by sanjana on 08/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

public class API {
    
    static let sharedInstance = API()
    
    let sessionManager = SessionManager()

    func startAuthSession() {
        guard let token = Utils.getAuthToken() else {
            return
        }
        let oauthHandler = OAuthHandler(
            accessToken: token
        )
        sessionManager.adapter = oauthHandler
        sessionManager.retrier = oauthHandler

    }
    
    
    func request<T: BaseMappable>(_ urlRequest: URLRequestConvertible, completion: @escaping (T?, APIError?) -> Void) {
        sessionManager.request(urlRequest).responseJSON { (dataresponse) in
            if (dataresponse.result.isFailure) {
                let dict = [ServiceConstant.ERROR_KEY : ServiceConstant.ERROR_VALUE, ServiceConstant.MESSAGE : dataresponse.error!.localizedDescription]
                let apiError = APIError(JSON: dict)
                completion(nil, apiError)
                return
            }
            
            let result = dataresponse.value as! [String : Any]
            if DEBUG.isDebuggingEnabled {
                print("URL_REQUEST = \(String(describing: urlRequest.urlRequest))")
                print("Response = \(result)")
            }
            
            if let apiError = APIError(JSON:result) {
                completion(nil, apiError)
                return
            }
            let object =  Mapper<T>(context: nil, shouldIncludeNilValues: false).map(JSON: result)
            completion(object,nil)
        }
    }
    
    func requestForCollection<T: BaseMappable>(_ keyPath: String?, urlRequest: URLRequestConvertible, completion: @escaping ([T]?, APIError?) -> Void) {
        sessionManager.request(urlRequest).responseJSON { (dataresponse) in
            if (dataresponse.result.isFailure) {
                let dict = [ServiceConstant.ERROR_KEY : ServiceConstant.ERROR_VALUE, ServiceConstant.MESSAGE : dataresponse.error!.localizedDescription]
                let apiError = APIError(JSON: dict)
                completion(nil, apiError)
                return
            }
            
            let result = dataresponse.value as! [String : Any]
            if DEBUG.isDebuggingEnabled {
                print("URL_REQUEST = \(String(describing: urlRequest.urlRequest))")
                print("Response = \(result)")
            }
            
            if let apiError = APIError(JSON:result) {
                completion(nil, apiError)
                return
            }
            let JSONObject = self.processResponse(result: dataresponse, keyPath: keyPath)

            if let parsedObject = Mapper<T>(context: nil, shouldIncludeNilValues: false).mapArray(JSONObject: JSONObject){
                return completion(parsedObject, nil)
            } else {
                let dict = [ServiceConstant.ERROR_KEY : ServiceConstant.ERROR_VALUE, ServiceConstant.MESSAGE : "someting_wrong".localized()]
                let apiError = APIError(JSON: dict)
                completion(nil, apiError)
            }
        }
    }
    
     func processResponse(result: DataResponse<Any>, keyPath: String?) -> Any? {
        let JSON: Any?
        if let keyPath = keyPath , keyPath.isEmpty == false {
            JSON = (result.value as AnyObject?)?.value(forKeyPath: keyPath)
        } else {
            JSON = result.value
        }
        
        return JSON
    }
}

//
//  SplashViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class SplashViewController: BaseViewController {

    //MARK:- Properties
    @IBOutlet weak var imageView: UIImageView!
   
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.setGifImage(imageName: "ic_logo_gif")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            let userLoggedIn = Utils.isUserLoggedIn()
            if userLoggedIn {
                AppDelegate.sharedAppDelegate().setDashboardRootController()
            } else {
                 AppDelegate.sharedAppDelegate().setSignInRootController()
            }
        }
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

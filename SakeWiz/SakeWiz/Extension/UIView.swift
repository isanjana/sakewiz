//
//  UIView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/6/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit
import ACProgressHUD_Swift

extension UIView {
    
    /**
     *  Update layer properties of UIView.
     */
    
    func updateLayer(borderColor:UIColor,cornerRadius:CGFloat, borderWidth:CGFloat) {
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    
    /**
     *  Animate the view based on given animation direction type
     */
    func animate(type:String){
        
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.subtype = type
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        
        // Add the animation to the View's layer
        self.layer.add(transition, forKey:kCATransition)
    }
    
    func addSpringAnimation() {
        
        self.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        UIView.animate(withDuration: 0.8,delay: 0,usingSpringWithDamping: 0.4,initialSpringVelocity: 7.0,
                       options: UIViewAnimationOptions.allowUserInteraction,animations: {
                        
                        self.transform  = CGAffineTransform.identity
                        
                        
        }, completion: nil)
    }
    
    /**
     *  Makes the circular view
     */
    
    func circular(borderColor:UIColor, borderWidth:CGFloat) {
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.bounds.size.width / 2
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    
    /**
     *  Makes fade in-out effect on view
     */
    func fadeInOut(duration:CFTimeInterval) {
        
        let transition = CATransition()
        transition.type = kCATransitionFade
        transition.duration = duration
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        // Add the animation to the View's layer
        self.layer.add(transition, forKey:kCATransition)
    }
    
    /**
     To add a drop shadow effect on self
     
     Make a drop shadow effect using shadowRadius, shadowColor, shadowOffset etc .
     
     */
    func addDropShadowEffect(){
        
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2.0
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowOpacity = 0.7
    }
    
    /**
     To update pager view layer
     */
    func updatePagerViewLayer() {
        updateLayer(borderColor: UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0), cornerRadius: 5, borderWidth: 1)
    }
    
    /**
     Add tap gesture on view
     
     This method add a UITapGestureRecognizer.
     
     
     :parameters:
     * target: - The object that recieves the action message.
     * action: - The action to send to target when view taped.
     
     */
    func addTapGesture(target:Any?, action:Selector?) {
        isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: target, action: action)
        addGestureRecognizer(tapGesture)
    }
    
    
}



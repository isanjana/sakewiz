//
//  ForgotPasswordViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/6/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import UITextField_Navigation

class ForgotPasswordViewController: SignInBaseViewController {
    
    //MARK:- Properties
    @IBOutlet weak var handleTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var hintLable: UILabel!
    var viewModel = ForgotPasswordViewModel()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- Submit
    @IBAction func submitAction(_ sender: Any) {
        
        switch viewModel.validate() {
        case .Valid:
            showProgress(title: "forgotPass_progress_message".localized())
            viewModel.resetPassword(completion: { (successMsg, error) in
                self.hideProgress()
                if error != nil {
                    self.showBannerAlert(title: error!.getTitle(), message: error!.getMessage(), style: .warning, delegate: nil)
                    return;
                }
                
                self.showBannerAlert(title: "forgot_pwd_success".localized(), message: successMsg!, style: .success, delegate: self)
            })
            break
        
        case .Invalid(let errorString):
             showBannerAlert(title: "error".localized(), message: errorString, style: .warning, delegate: nil)
            break
        
        }
    }
    
    //MARK:-  View SetUp
    override func viewSetUP() {
        
        super.viewSetUP()
        setScreenTitle("forgot_pwd")
        emailTF.nextNavigationField = handleTF
    }
    
    override func onBannerDismiss() {
        goToLastController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ForgotPasswordViewController {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text! as NSString
        let updatedText = currentText.replacingCharacters(in: range, with:string)
        
        if textField == handleTF {
            viewModel.updateUsername(username: updatedText)
            return true
        }
        
        if textField == emailTF {
            viewModel.updateEmail(email: updatedText)
            return true
        }
        
        
        return true
    }
}

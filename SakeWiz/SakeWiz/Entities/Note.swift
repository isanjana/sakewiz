//
//  Note.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class Note: Mappable {
    
    var id: String?
    var userId: String?
    var entity: String?
    var entityId: String?
    var entityUrl: String?
    var note: String?
    var updated: Int64?
    var entityName: [String:Any] = [:]
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        userId <- map["userId"]
        entity <- map["entity"]
        entityId <- map["entityId"]
        entityUrl <- map["entityUrl"]
        note <- map["note"]
        updated <- map["updated"]
        entityName <- map["entityName"]
       
    }
}

//
//  SakeUploadStatusCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/8/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import ScalingCarousel


class SakeUploadStatusCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var carousel: ScalingCarouselView!
    @IBOutlet weak var pageControl: UIPageControl!
    fileprivate var userDashboardViewModel: UserDashboardViewModel? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pageControl.numberOfPages = 1
    }
    
    //MARK:- Configure Cell
    func configureCell(userDashboardViewModel:UserDashboardViewModel) {
        self.userDashboardViewModel = userDashboardViewModel
        carousel.reloadData()
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}


extension SakeUploadStatusCell: UICollectionViewDataSource, UICollectionViewDelegate {
    //MARK:- CarouselDatasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SakeUploadCarouselCell.identifier, for: indexPath) as! SakeUploadCarouselCell
        guard let userDashboardViewModel = self.userDashboardViewModel else { return cell}
        cell.configureCell(userDashboardViewModel: userDashboardViewModel)
        return cell
    }
    
    //MARK:- CarouselDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        carousel.didScroll()
        let pageWidth: CGFloat = UIScreen.main.bounds.size.width
        let page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
        self.pageControl.currentPage = Int(page)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
    }
    
}



class SakeUploadCarouselCell: ScalingCarouselCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var sakeUplaodView: SakeUplaodStatusView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    //MARK:- Configure Cell
    func configureCell(userDashboardViewModel:UserDashboardViewModel) {
        sakeUplaodView.updateView(userDashboardViewModel: userDashboardViewModel)
    }
    

}




//
//  RegisterViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import UITextField_Navigation


class RegisterViewController: SignInBaseViewController {
    
    //MARK:- Properties
    @IBOutlet weak var handleTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var getStartedButton: UIButton!
    
    var viewModel = RegisterViewModel()
    

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- Get Started
    @IBAction func getStarted(_ sender: Any) {
        
        switch viewModel.validate() {
        case .Valid:
            showProgress(title: "register_progress_message".localized())
            viewModel.register(completion: { (user, error) in
                self.hideProgress()
                if error != nil {
                    self.showBannerAlert(title: error!.getTitle(), message: error!.getMessage(), style: .warning, delegate: nil)
                    return;
                }
            })
            
            break
            
        case .Invalid(let errorString):
            showBannerAlert(title: "error".localized(), message: errorString, style: .warning, delegate: nil)
            break
            
        }
    }
    
    
    //MARK:-  View SetUp
    override func viewSetUP() {
        
        super.viewSetUP()
        setScreenTitle("register_with_email")
        handleTF.nextNavigationField = emailTF
        emailTF.nextNavigationField = passwordTF
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension RegisterViewController {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text! as NSString
        let updatedText = currentText.replacingCharacters(in: range, with:string)
        
        if textField == handleTF {
            viewModel.updateUsername(username: updatedText)
            return true
        }
        
        if textField == emailTF {
            viewModel.updateEmail(email: updatedText)
            return true
        }
        
        if textField == passwordTF {
            viewModel.updatePassword(password: updatedText)
            return true
        }
        
        return true
    }
}

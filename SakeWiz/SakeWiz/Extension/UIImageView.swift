//
//  UIImageView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/6/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit
import STXImageCache

extension UIImageView {
    
    /**
     *  set gif type image from local resource.
     */
    func setGifImage(imageName: String) {
        let gif = UIImage.gif(name: imageName)
        image = gif
    }
    
    /**
     *  Load image from remote url, without placeholder, loader on imageView true.
     */
    func imageFromUrl(_ urlString:String) {
        imageFromUrl(urlString, placeholder: "", showLoader: true)
    }
    
    /**
     *  Load image from remote url, without placeholder, loader on imageView congigurable.
     */
    func imageFromUrl(_ urlString:String, showLoader: Bool ) {
        imageFromUrl(urlString, placeholder: "", showLoader: showLoader)
    }
    
    /**
     *  Load image from remote url, placeholder congigurable, loader on imageView true.
     */
    func imageFromUrl(_ urlString:String, placeholder:String) {
        imageFromUrl(urlString, placeholder: placeholder, showLoader: true)
    }
    
    /**
     *  Load image from remote url, loader on imageView configurable.
     */
    private func imageFromUrl(_ urlString:String, placeholder:String, showLoader:Bool) {
        
        if let photoUrl = URL(string: urlString) {
            
            var placeholderImage:UIImage? = nil
            if !placeholder.isEmptyText() {
                placeholderImage = UIImage(named:placeholder)
            }
            
            if showLoader {
                let loader = UIActivityIndicatorView(activityIndicatorStyle: .white)
                
                loader.translatesAutoresizingMaskIntoConstraints = false
                loader.tag = 111
                self.addSubview(loader)
                
                // layout
                let horizontalConstraint = loader
                    .centerXAnchor.constraint(equalTo: self.centerXAnchor)
                let verticalConstraint = loader
                    .centerYAnchor.constraint(equalTo: self.centerYAnchor)
                NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint])
                
                loader.hidesWhenStopped = true
                loader.startAnimating()
            }
            
            
            
            self.stx.image(atURL: photoUrl, placeholder: placeholderImage, forceRefresh: false, completion: { (image, error) -> (Image?) in
                
                if showLoader {
                    
                    DispatchQueue.main.async {
                        if let loader = self.viewWithTag(111) as? UIActivityIndicatorView {
                            loader.stopAnimating()
                            loader.removeFromSuperview()
                        }
                        
                    }
                }
                
                return image
            })
        }
    }
}

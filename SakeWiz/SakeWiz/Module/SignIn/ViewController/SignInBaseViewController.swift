//
//  SignInBaseViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/6/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import EKKeyboardAvoiding
import UITextField_Navigation

class SignInBaseViewController: BaseViewController {

    //MARK:- Properties
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var logoImageView: UIImageView!
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetUP()
    }
    
    func viewSetUP() {
        addBackButton()
        setLogoGifImage()
        scrollView.setKeyboardAvoidingEnabled(true)
    }
    
    func setLogoGifImage() {
        logoImageView.setGifImage(imageName: "ic_logo_gif")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension SignInBaseViewController: UITextFieldDelegate, NavigationFieldDelegate {
    
    func navigationFieldDidTapPreviousButton(_ navigationField: NavigationField) {
        navigationField.previousNavigationField?.becomeFirstResponder()
    }
    
    func navigationFieldDidTapNextButton(_ navigationField: NavigationField) {
        navigationField.nextNavigationField?.becomeFirstResponder()
    }
    
    func navigationFieldDidTapDoneButton(_ navigationField: NavigationField) {
        navigationField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}

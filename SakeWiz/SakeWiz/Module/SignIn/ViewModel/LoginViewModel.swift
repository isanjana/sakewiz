//
//  LoginViewModel.swift
//  SakeWiz
//
//  Created by sanjana on 07/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation

enum UserValidationState {
    case Valid
    case Invalid(String)
}

class LoginViewModel {
    
    //MARK:- Properties
    var username = ""
    var password = ""
}

extension LoginViewModel {
    
    //MARK:- Update User Data
    func updateUsername(username: String) {
        self.username = username
    }
    
    func updatePassword(password: String) {
        self.password = password
    }
    
    //MARK:- Validate User Data
    func validate() -> UserValidationState {        
        
        if !username.isValidHandle() {
            return .Invalid("handel_invalid".localized())
        }
        
        if !password.isValidPassword() {
            return .Invalid("password_invalid".localized())
        }
                
        return .Valid
    }
    
    //MARK:- Login API
    func login(completion: @escaping (_ error: APIError?) -> Void) {
        UserService.sharedInstance.login(username: self.username.lowercased(), password: self.password) { (user, error) in
            completion(error)
        }
    }
}

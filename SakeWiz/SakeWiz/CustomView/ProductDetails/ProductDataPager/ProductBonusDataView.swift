//
//  ProductBonusDataView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/12/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

@IBDesignable class ProductBonusDataView: NibView {

    //MARK:- Properties
    @IBOutlet weak var pressLabel: UILabel!
    @IBOutlet weak var filterationLabel: UILabel!
    @IBOutlet weak var yeastLabel: UILabel!
    @IBOutlet weak var postorizationLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLable: UILabel!
    @IBOutlet weak var wizPointsLabel: UILabel!
   
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updatePagerViewLayer()
        profileImageView.circular(borderColor: .clear, borderWidth: 0)
    }

}

//
//  ScanLineAnimation.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/26/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit

class ScanLineAnimation: UIImageView {
    
    var isAnimationing = false
    var animationRect: CGRect = CGRect.zero
    private var parentView:UIView?
    private var xScanRetangleOffset:CGFloat = 0
    private var whRatio:CGFloat = 1.0
    private var centerUpOffset:CGFloat = 0
    
    func startAnimating(parentView: UIView, image: UIImage?) {
        
        self.image = image
        self.parentView = parentView
        self.animationRect = getScanRectForAnimation()
        parentView.addSubview(self)
        
        self.isHidden = false;
        isAnimationing = true;
        
        if image != nil {
            stepAnimation()
        }
        
    }
    
    func stepAnimation() {
        
        if (!isAnimationing) {
            return;
        }
        
        var frame:CGRect = animationRect;
        let hImg = self.image!.size.height * animationRect.size.width / self.image!.size.width;
        
        frame.origin.y -= hImg;
        frame.size.height = hImg;
        self.frame = frame;
        //self.alpha = 0.0;
        
        UIView.animate(withDuration: 2, animations: { () -> Void in
            
            // self.alpha = 1.0;
            
            var frame = self.animationRect;
            let hImg = self.image!.size.height * self.animationRect.size.width / self.image!.size.width;
            
            frame.origin.y += (frame.size.height -  hImg);
            frame.size.height = hImg;
            
            self.frame = frame;
            
        }, completion:{ (value: Bool) -> Void in
            self.perform(#selector(ScanLineAnimation.stepAnimation), with: nil, afterDelay: 0)
        })
        
    }
    
    func stopStepAnimating() {
        self.isHidden = true;
        isAnimationing = false;
    }
    
    
    func getScanRectForAnimation() -> CGRect {
        
        let XRetangleLeft = xScanRetangleOffset
        var sizeRetangle = CGSize(width: parentView!.frame.size.width - XRetangleLeft*2, height: parentView!.frame.size.width - XRetangleLeft*2)
        
        if whRatio != 1
        {
            let w = sizeRetangle.width
            var h = w / whRatio
            
            
            let hInt:Int = Int(h)
            h = CGFloat(hInt)
            
            sizeRetangle = CGSize(width: w, height: h)
        }
        
        
        let YMinRetangle = parentView!.frame.size.height / 2.0 - sizeRetangle.height/2.0 - centerUpOffset
        
        let cropRect =  CGRect(x: XRetangleLeft, y: YMinRetangle, width: sizeRetangle.width, height: sizeRetangle.height)
        
        return cropRect;
    }
    
    deinit
    {
        stopStepAnimating()
    }
    
}

//
//  ProductDetail.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class ProductDetail: Mappable {
    
    var avgRate: Float?
    var countryName: [String:Any] = [:]
    var regionName: [String:Any] = [:]
    var totalFavourites: Int?
    var totalRates: Int?
    var userFavourite: Bool?
    var userHasNote: Bool?
    var userRate: Float?
    var pastorizeTankStorage: String?
    var userReview: [String:Any] = [:]
    var contributedUser1Name: String?
    var contributedUser1ImgUrl: String?
    var contributedUser2Name: String?
    var contributedUser2ImgUrl: String?
    var product: Product?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        avgRate <- map["avgRate"]
        countryName <- map["countryName"]
        regionName <- map["regionName"]
        totalFavourites <- map["totalFavourites"]
        totalRates <- map["totalRates"]
        userFavourite <- map["userFavourite"]
        userHasNote <- map["userHasNote"]
        userRate <- map["userRate"]
        pastorizeTankStorage <- map["pastorizeTankStorage"]
        userReview <- map["userReview"]
        contributedUser1Name <- map["contributedUser1Name"]
        contributedUser1ImgUrl <- map["contributedUser1ImgUrl"]
        contributedUser2Name <- map["contributedUser2Name"]
        contributedUser2ImgUrl <- map["contributedUser2ImgUrl"]
        product <- map["product"]
     
    }
}

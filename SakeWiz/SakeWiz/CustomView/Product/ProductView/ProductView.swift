//
//  ProductView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/9/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

@IBDesignable class ProductView: NibView {
    
    //MARK:- Properties
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var sakeTitleLabel: UILabel!
    @IBOutlet weak var sakeTypeLable: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var chatCountLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var likeViewWidth: NSLayoutConstraint!
    @IBOutlet weak var noteImageView: UIImageView!
    @IBOutlet weak var coinImageView: UIImageView!
    
    @IBInspectable var isDeleteRequired: Bool = false {
        didSet {
            hideDeleteButton()
        }
    }
    
    //MARK:- Initializaion
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        productImageView.updateLayer(borderColor: .clear, cornerRadius: ValueConstant.DEFAULT_RADIUS, borderWidth: 0)
        hideDeleteButton()
    }
    
    func updateView(productViewModel:ProductViewModel) {
        
        //Product Name
        sakeTitleLabel.text = productViewModel.getName
        
        //Product Type
        if let type = productViewModel.getType {
            sakeTypeLable.text = type
        }
        
        //Product Place
        placeLabel.text = productViewModel.getPalce
        
        //Product Image
        if let productImageUrl = productViewModel.productImageUrl {
            productImageView.imageFromUrl(productImageUrl, showLoader: true)
        }
        
        //Product Rating
        if let rate = productViewModel.getRate {
            ratingView.value = CGFloat(rate)
        }
        
        //Product Reviews
        if let reviewCount = productViewModel.getReviewCount {
            chatCountLabel.text = reviewCount
        }
        
        //Product Likes
        if let favoured = productViewModel.getFavouredCount {
            likeCountLabel.text = favoured
        }
        
        //Product updated time
        timeLabel.text = productViewModel.getTime
        
        //Has Note
        noteImageView.isHidden = !productViewModel.hasUserNote
        
        //gainPoints
        coinImageView.isHidden = !productViewModel.canGainPoints
        
    }
    
    @IBAction func deleteButtonDidClicked(_ sender: Any) {
        
    }
    
    private func hideDeleteButton() {
        deleteButton.isHidden = !isDeleteRequired
        likeViewWidth.constant = deleteButton.isHidden ? likeViewWidth.constant - 5 : likeViewWidth.constant
    }

}

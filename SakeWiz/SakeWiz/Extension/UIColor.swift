//
//  UIColor.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/6/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
    
    static func appThemColor() -> UIColor {
        return UIColor(red: 105/255, green: 165/255, blue: 116/255, alpha: 1.0)
    }
    
    static func profileImageBorderColor() -> UIColor {
        return .white
    }
    
    static func appRedColor() -> UIColor {
        return UIColor(red: 225/255, green: 133/255, blue: 124/255, alpha: 1.0)
    }
    
    static func appGrayColor() -> UIColor {
        return UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
    }
    
}

//
//  LeftMenuViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/7/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

struct MenuItem {
    
    var type = 1
    var title = ""
    var icon = ""
    
    init(type: Int, title: String, icon: String) {
        
        self.type = type
        self.title = title
        self.icon = icon
    }
}

class LeftMenuViewController: UIViewController {
    
    //MARK:- Menu Items
     static let dashboard = MenuItem(type: MenuConstant.Dashboard, title: "menu_item_dashboard".localized(), icon: "Menu_Search")
    static let searchPlaces = MenuItem(type: MenuConstant.Search, title: "menu_item_search_places".localized(), icon: "Menu_Search")
    static let myProfile   = MenuItem(type: MenuConstant.Profile, title: "menu_item_my_profile".localized(), icon: "Menu_Profile")
    static let followers  = MenuItem(type: MenuConstant.Followers, title: "menu_item_followers".localized(), icon: "Menu_Followers")
    static let privateNotes = MenuItem(type: MenuConstant.Private_Notes, title: "menu_item_private_notes".localized(), icon: "Menu_Private_Notes")
    static let savedSake    = MenuItem(type: MenuConstant.Saved_Sake, title: "menu_item_saved_sakes".localized(), icon: "Menu_Sake")
    static let newsFeed = MenuItem(type: MenuConstant.News_Feed, title: "menu_item_newsfeed".localized(), icon: "Menu_News_Feed")
    static let myPurchses   = MenuItem(type: MenuConstant.My_Purchases, title: "menu_item_my_purchases".localized(), icon: "Menu_Purchases")
    static let settings  = MenuItem(type: MenuConstant.Settings, title: "menu_item_settings".localized(), icon: "Menu_Settings")
    static let logout    = MenuItem(type: MenuConstant.Logout, title: "menu_item_logout".localized(), icon: "Menu_Logout")
    
    
    //MARK:- Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scanButton: SWButton!
    var menuItems:[MenuItem] = []
    

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateDataSource()
    }
    
    func updateDataSource() {
        menuItems.removeAll()
        menuItems.append(LeftMenuViewController.dashboard)
        menuItems.append(LeftMenuViewController.searchPlaces)
        menuItems.append(LeftMenuViewController.myProfile)
        menuItems.append(LeftMenuViewController.followers)
        menuItems.append(LeftMenuViewController.privateNotes)
        menuItems.append(LeftMenuViewController.savedSake)
        menuItems.append(LeftMenuViewController.newsFeed)
        menuItems.append(LeftMenuViewController.settings)
        menuItems.append(LeftMenuViewController.logout)
        tableView.reloadData()
        scanButton.setTitle("scan_label".localized(), for: .normal)
    
    }

    @IBAction func scanLableDidClicked(_ sender: Any) {
        
    }

    @IBAction func closeButtonDidClicked(_ sender: Any) {
        closeleftMenu()
    }
    
    fileprivate func closeleftMenu() {
        self.slideMenuController()?.closeLeft()
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension LeftMenuViewController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: LeftMenuCell.identifier, for: indexPath) as! LeftMenuCell
        
        let menuItem = menuItems[indexPath.row]
        cell.titleLabel.text = menuItem.title
        cell.thumbImage.image = UIImage(named: menuItem.icon)
        
        switch menuItem.type {
            
        case MenuConstant.News_Feed:
            cell.feedCountLabel.isHidden = false
            
        default:
            cell.feedCountLabel.isHidden = true
            break
            
        }
        
        
        return cell
    }
    
    //MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let menuItem = menuItems[indexPath.row]
        let dashBoardStoryBoard = UIStoryboard(name: StoryboardName.Dashboard, bundle: nil)
        
        switch menuItem.type {
            
        case MenuConstant.Dashboard:
            let dashboardVC = dashBoardStoryBoard.instantiateViewController(withIdentifier: String.className(DashboardViewController.self)) as! DashboardViewController
            let navController = UINavigationController(rootViewController: dashboardVC)
            self.slideMenuController()?.changeMainViewController(navController, close: true)
            break
            
        case MenuConstant.Search:
            
            break
            
        case MenuConstant.Profile:
            let profileVC = storyboard?.instantiateViewController(withIdentifier: String.className(UserProfileViewController.self)) as! UserProfileViewController
            let navController = UINavigationController(rootViewController: profileVC)
            let curretUserID = Utils.getUserID()
            let userProfileViewModel = UserProfileViewModel(withUserId: curretUserID)
            userProfileViewModel.fromMenu = true
            profileVC.userProfileViewModel = userProfileViewModel
            self.slideMenuController()?.changeMainViewController(navController, close: true)
            
            break
            
        case MenuConstant.Followers:
            let followVC = dashBoardStoryBoard.instantiateViewController(withIdentifier: String.className(FollowViewController.self)) as! FollowViewController
            let navController = UINavigationController(rootViewController: followVC)
            followVC.fromMenu = true
            self.slideMenuController()?.changeMainViewController(navController, close: true)
           
            break
            
        case MenuConstant.Private_Notes:
           
            break
            
        case MenuConstant.Saved_Sake:
            let favStoryBoard = UIStoryboard(name: StoryboardName.Favourite, bundle: nil)
            let favVC = favStoryBoard.instantiateViewController(withIdentifier: String.className(FavouriteViewController.self)) as! FavouriteViewController
            let navController = UINavigationController(rootViewController: favVC)
            favVC.fromMenu = true
            self.slideMenuController()?.changeMainViewController(navController, close: true)
            
            break
            
        case MenuConstant.News_Feed:
            
            break
            
        case MenuConstant.My_Purchases:
            
            break
            
        case MenuConstant.Settings:
            
            break
            
        case MenuConstant.Logout:
            showLogoutAlert()
            break
            
        default:
            break
        }
        
        
    }
    
    //MARK: - Show Logout Alert
    func showLogoutAlert() {
        
        let alertView = UIAlertController(title: "", message: "alert_logout_msg".localized(), preferredStyle: .alert)
        
        alertView.addAction(UIAlertAction(title: "no".localized(), style: .default, handler: {(alertAction) -> Void in
            self.dismiss(animated: true, completion: nil)
        }))
        
        alertView.addAction(UIAlertAction(title: "yes".localized(), style: .default, handler: {(alertAction) -> Void in
            self.userLogout()
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertView, animated: true, completion: nil)
        
    }
    
    //MARK:- User Logout
    func userLogout() {
        Utils.removeObjectFromDefaults(key: UserDefaultConstant.kAuthToken)
        Utils.removeObjectFromDefaults(key: UserDefaultConstant.kUserID)
        Utils.removeObjectFromDefaults(key: UserDefaultConstant.kHNDL)
        Utils.removeObjectFromDefaults(key: UserDefaultConstant.kPassword)
        closeleftMenu()
        AppDelegate.sharedAppDelegate().setSignInRootController()
        
    }
    
    
    
}



//
//  ReviewResults.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class ReviewResults: Mappable {
    
    var avgRate: Float?
    var lastId: String?
    var totalReviews: Int?
    var likes: Int?
    var reviews: [Review] = []
   
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        avgRate <- map["avgRate"]
        lastId <- map["lastId"]
        totalReviews <- map["totalReviews"]
        likes <- map["likes"]
        reviews <- map["reviews"]
        
    }
}


//
//  FavouritePlaceView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/21/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit



@IBDesignable class FavouritePlaceView: NibView {
    
    //MARK:- Properties
    @IBOutlet weak var tableView: UITableView!
     var favPlaceViewModel: FavouritePlaceViewModel? = nil
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        registerNibFile()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        registerNibFile()
    }
    
    convenience init(withEntityType type: EntityType) {
        self.init()
        favPlaceViewModel = FavouritePlaceViewModel(withEntityType: type)
    }
    
    func registerNibFile() {
        tableView.configureTableView()
        tableView?.register(UINib(nibName: "FavouritePlaceCell", bundle: nil), forCellReuseIdentifier: FavouritePlaceCell.identifier)
    }
    
    
    
    func refreshUI(refresh:Bool, completion: @escaping () -> Void) {
        self.showLoader()
        if refresh || favPlaceViewModel?.getPlaceCount() == 0 {
            favPlaceViewModel?.loadPlaceList(completion: { (error) in
                completion()
                self.hideLoader()
                self.tableView.reloadData()
            })
        } else{
            self.hideLoader()
            completion()
        }
        
    }
}


extension FavouritePlaceView : UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = favPlaceViewModel?.getPlaceCount() {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let placeCell = self.tableView.dequeueReusableCell(withIdentifier: FavouritePlaceCell.identifier, for: indexPath) as! FavouritePlaceCell
        guard let placeViewModel = favPlaceViewModel?.favPlaceForIndex(index: indexPath.row) else { return placeCell }
        placeCell.configureCell(placeViewModel: placeViewModel)
        
        return placeCell
    }
    
    //MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let placeID = favPlaceViewModel?.getPlaceIdForIndex(index: indexPath.row) {
            EventBus.postToMainThread(TaskName.ShowPlaceDetailsController, userInfo: ["id": placeID, "type": favPlaceViewModel!.entityType])
        }
    }
        
}

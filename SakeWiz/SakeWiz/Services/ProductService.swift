//
//  ProductService.swift
//  SakeWiz
//
//  Created by sanjana on 27/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import Alamofire

class ProductService {
    
    static let sharedInstance = ProductService()

    func getReviewsForProduct(productID: String , lastID: String, completion: @escaping (_ reviewResult: ReviewResults?, _ error: APIError?) -> Void) {
        let parameteres = ["lastId" : lastID, "size" : 10] as [String : Any]

        API.sharedInstance.request(APIEndpoint.getReviewsForEntity(entityID: productID, parameters: parameteres)) { (reviewResult: ReviewResults?, apiError: APIError?) in
            completion(reviewResult, apiError)
        }

    }
}

//
//  ReviewsCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/18/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class ReviewsCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var reviewView: ReviewView!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- Configure Cell
    func configureCell(hideTitleView:Bool, reviewViewModel:ReviewViewModel) {
        titleHeight.constant = hideTitleView ? 0 : 38
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

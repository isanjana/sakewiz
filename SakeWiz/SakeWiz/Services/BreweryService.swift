//
//  BreweryService.swift
//  SakeWiz
//
//  Created by sanjana on 25/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import Alamofire

class BreweryService {
    
    static let sharedInstance = BreweryService()

    func getBreweryDetails(breweryID: String, completion: @escaping (_ placeDetail: PlaceDetail?, _ error: APIError?) -> Void) {
        API.sharedInstance.request(APIEndpoint.fetchBreweryDetails(breweryID: breweryID)) { (placeDetail: PlaceDetail?, error: APIError?) in
            completion(placeDetail, error)
        }
    }
    
    
    func markFav(breweryID: String, completion: @escaping (_ responseDict: [String: Any]?, _ error: APIError?) -> Void) {
        API.sharedInstance.sessionManager.request(APIEndpoint.markBreweryAsFavour(breweryID: breweryID)).responseJSON { (dataResponse) in
            ServiceUtils.processResponse(dataResponse: dataResponse, completion: completion)
        }
    }
    
    func markUnFav(breweryID: String, completion: @escaping (_ responseDict: [String: Any]?, _ error: APIError?) -> Void) {
        API.sharedInstance.sessionManager.request(APIEndpoint.markBreweryAsUnFavour(breweryID: breweryID)).responseJSON { (dataResponse) in
            ServiceUtils.processResponse(dataResponse: dataResponse, completion: completion)
        }
    }
    
    func postReview(breweryID: String, comment: String ,rate: Double, completion: @escaping (_ responseDict: [String: Any]?, _ error: APIError?) -> Void) {
        let commentInfo = [Utils.getLanguageCode() : comment]
        let parameteres = [ServiceConstant.RATE : rate, ServiceConstant.COMMENT : commentInfo] as [String : Any]

        API.sharedInstance.sessionManager.request(APIEndpoint.postBreweryReview(breweryID: breweryID, parameters: parameteres)).responseJSON { (dataResponse) in
            ServiceUtils.processResponse(dataResponse: dataResponse, completion: completion)
        }
    }
    
}

//
//  FavouriteSakeViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/27/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation


class FavouriteSakeViewModel {
    //MARK:- Properties
    fileprivate var favProducts:[Product] = []
    
}


extension FavouriteSakeViewModel {
    
    func getUserFavouriteProducts(completion: @escaping (_ error: APIError?) -> Void) {
    
        UserService.sharedInstance.userFavouriteProducts { (products, error) in
            
            if let error = error {
                completion(error)
                return;
            }
            self.favProducts.removeAll()
            if let favProducts = products {
                self.favProducts.append(contentsOf: favProducts)
            }
            completion(nil)
        }
    }
    
    //MARK:- Helper Methods
    func favProductForIndex(index:Int) -> ProductViewModel? {
        
        if favProducts.count > 0 {
            let productViewModel = ProductViewModel(withProdut: favProducts[index])
            return productViewModel
        }
        
        return nil
    }
   
    
    func getProductsCount() -> Int {
        return favProducts.count > 0 ? favProducts.count : 0
    }
    
    func getProductIdForIndex(index:Int) -> String? {
        return favProducts[index].id
    }
}



//
//  UnknownSakeViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/20/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class UnknownSakeViewController: BaseViewController {
    
    //MARK:- Properties
    @IBOutlet weak var tableView: UITableView!
    var unknownSakeViewModel = UnknownSakeViewModel()
    var loadMoreComplete = false

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.configureTableView()
        addBackButton()
        setScreenTitle("unkown_sake")
        fetchUnknownSakes(loadMore: false)
    }
    
    func fetchUnknownSakes(loadMore: Bool) {
        showProgress(title: "")
        unknownSakeViewModel.fetchUnIdentifiedSake(loadMore: loadMore) { (error) in
            
            self.hideProgress()
            if error != nil {
                if DEBUG.isDebuggingEnabled {
                    self.showBannerAlert(title: error!.getTitle(), message: error!.getMessage(), style: .warning, delegate: nil)
                }
                return;
            }
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


extension UnknownSakeViewController : UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    //MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return unknownSakeViewModel.getUnknownSakeCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let unknownSakeCell = self.tableView.dequeueReusableCell(withIdentifier: UnknownSakeCell.identifier, for: indexPath) as! UnknownSakeCell
        guard let unknownSakeResultViewModel = unknownSakeViewModel.unknownSakeForIndex(index: indexPath.row) else { return unknownSakeCell }
        unknownSakeCell.configureCell(viewModel: unknownSakeResultViewModel)
        return unknownSakeCell
        
    }
    
    //MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    // MARK:=======>Auto Loading=======
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if loadMoreComplete {
            return;
        }
        
        // calculate scrolling offset
        let currentOffset = scrollView.contentOffset.y;
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        
        // Change 10.0 to adjust the distance from bottom
        if (maximumOffset - currentOffset <= 10.0) {
            fetchUnknownSakes(loadMore: true)
        }
    }

}

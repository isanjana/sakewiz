//
//  FollowView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/23/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit



@IBDesignable class FollowView: NibView {

    //MARK:- Properties
    @IBOutlet weak var tableView: UITableView!
    var userListViewModel: UserListViewModel? = nil
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        registerNibFile()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        registerNibFile()
    }
    
    convenience init(withListType type: UserListType) {
        self.init()
         userListViewModel = UserListViewModel(withListType: type)
        
    }
    
    func registerNibFile() {
        tableView.configureTableView()
        tableView?.register(UINib(nibName: "FollowViewCell", bundle: nil), forCellReuseIdentifier: FollowViewCell.identifier)
    }
    
    func loadUserList(completion: @escaping (_ count:Int, _ error: APIError?) -> Void) {
        userListViewModel?.loadUserList(completion: { (count, error) in
            if let error = error {
                completion(0, error)
                return;
            }
            completion(count, nil)
            self.tableView.reloadData()
        })
    }

}


extension FollowView : UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = userListViewModel?.getUserCount() {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let followCell = self.tableView.dequeueReusableCell(withIdentifier: FollowViewCell.identifier, for: indexPath) as! FollowViewCell
        guard let userViewModel = userListViewModel?.getUsersForIndex(index: indexPath.row) else { return followCell }
        followCell.configureCell(userViewModel)
        return followCell
        
    }
    
    //MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let userID = userListViewModel?.getUserIdForIndex(index: indexPath.row) {
            EventBus.postToMainThread(TaskName.ShowUserProfileViewController, userInfo: ["id":userID])
        }
    }
    
    
}

//
//  ServiceAvailabilityCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/19/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class ServiceAvailabilityCell: UITableViewCell {
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
     @IBOutlet weak var serviceAvailablityView: ServiceAvailablityView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- Configure Cell
    func configureCell(viewModel:PlaceDetailsViewModel) {
        serviceAvailablityView.updateView(viewModel: viewModel)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

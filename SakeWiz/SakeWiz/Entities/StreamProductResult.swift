//
//  StreamProductResult.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

class StreamProductResult: Mappable {
    
    var products: [Product] = []
    
    required init?(map: Map) {
        
        
    }
    
    func mapping(map: Map) {
        
        products <- map["products"]
    }
}

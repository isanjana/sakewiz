//
//  UITableView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/23/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func configureTableView() {
        estimatedRowHeight = 80.0
        rowHeight = UITableViewAutomaticDimension
        tableFooterView = UIView()
    }
}

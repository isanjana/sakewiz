//
//  ReviewView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/18/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

@IBDesignable class ReviewView: NibView {
    
    //MARK:- Properties
    
    @IBOutlet weak var userNameLable: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var affltImageView: UIImageView!
    @IBOutlet weak var rankImageView: UIImageView!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var timesAgoLable: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var flagLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    @IBAction func likeButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    func updateView(viewModel:ReviewViewModel) {
        userNameLable.text = viewModel.getUserHandle
        addressLabel.text = viewModel.getAddress
        if let rate = viewModel.getRate {
            ratingView.value = CGFloat(rate)
        }
        
        commentsLabel.text = viewModel.getReview
        likeCountLabel.text = viewModel.getLikeCount
        timesAgoLable.text = viewModel.getCreatedTime
        likeButton.isSelected = viewModel.isAbleToLike
        if let rankImageUrl = viewModel.getRankImageUrl {
            rankImageView.imageFromUrl(rankImageUrl)
        }
        if let affltImageUrl = viewModel.getUserAffltImageUrl {
            affltImageView.imageFromUrl(affltImageUrl)
        }
        
        flagImageView.isHidden = viewModel.isCurrentUserFlagged
        flagLabel.isHidden = viewModel.isCurrentUserFlagged
    }
}

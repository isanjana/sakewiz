//
//  UnknownSakeViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/20/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation


class UnknownSakeViewModel {
    
    //MARK:- Properties
    fileprivate var unknownSakes: [UnIdentifiedSake] = []
    var lastId: String = ""
    
}


extension UnknownSakeViewModel {
    
    func fetchUnIdentifiedSake(loadMore:Bool, completion: @escaping (_ error: APIError?) -> Void) {
        
        if !loadMore {
            lastId = ""
        }
        else {
            if(lastId.isEmptyText()) {
                completion(nil)
                return
            }
        }
        
        UserService.sharedInstance.fetchUnIdentifiedSakes(lastId: lastId, size: ValueConstant.UNKNOWN_SAKE_COUNT) { (stremUserResult, error) in
            if let error = error {
                completion(error)
                return;
            }
            
            if self.lastId.isEmptyText() {
                self.unknownSakes.removeAll()
            }
            
            if let results = stremUserResult?.results {
                self.unknownSakes.append(contentsOf: results)
            }
            completion(nil)
        }
    }
    
    //MARK:- Helper Methods
    
    func unknownSakeForIndex(index:Int) -> UnknownSakeResultViewModel? {
        
        if unknownSakes.count > 0 {
            let viewModel = UnknownSakeResultViewModel(withUnknownSake: unknownSakes[index])
            return viewModel
        }
        
        return nil
    }
    
    func getUnknownSakeCount() -> Int {
        return unknownSakes.count > 0 ? unknownSakes.count : 0
    }

}

//
//  FavouriteViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/21/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class FavouriteViewController: BaseViewController {
    
    //MARK:- Properties
    @IBOutlet var scrollPager: ScrollPager!
    var fromMenu:Bool = false
    var isBreweries:Bool = false
    var sakeView: FavouriteSakeView? = nil
    var breweryView: FavouritePlaceView? = nil
    var barView: FavouritePlaceView? = nil

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUP()
        pagerSetUp()
        
    }
        
    func initialSetUP() {
        setScreenTitle("my_favourites")
        if fromMenu {
            addHambergerButton()
        }else{
            addBackButton()
        }
        
    }
    
    //MARK:- Pager SetUp
    func pagerSetUp() {
        scrollPager.delegate = self
        sakeView = FavouriteSakeView(withViewModel: FavouriteSakeViewModel())
        breweryView = FavouritePlaceView(withEntityType: .brewery)
        barView = FavouritePlaceView(withEntityType: .bar)
        
        scrollPager.addSegment(segments:[
            ("Sake", "Fave_Sake_Gray", "Fave_Sake_Green", sakeView!),
            ("Breweries", "Brewery_Gray", "Brewery_Green", breweryView!),
            ("Places", "Fav_Palce_Gray", "Fav_Palce_Green", barView!)
            ])
        
        self.setSelectedIndex()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        onTabChange(index: scrollPager.getSelectedIndex, refresh: true)
    }
    
    
    func setSelectedIndex() {
        if isBreweries {
            scrollPager.setSelectedIndex(index: EntityType.brewery.rawValue, animated: true)
        }else {
            scrollPager.setSelectedIndex(index: EntityType.sake.rawValue, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension FavouriteViewController : ScrollPagerDelegate {
    
    // MARK: - ScrollPagerDelegate -
    
    func scrollPager(scrollPager: ScrollPager, changedIndex: Int) {
       onTabChange(index: changedIndex, refresh: false)
    }
    
    func onTabChange(index: Int, refresh: Bool) {
        switch index {
        case EntityType.sake.rawValue:
            sakeView?.refreshUI(refresh: refresh, completion: {
            })
            break
        case EntityType.brewery.rawValue:
            breweryView?.refreshUI(refresh: refresh, completion: {
                if refresh {
                    self.setSelectedIndex()
                }
                
            })
            break
        case EntityType.bar.rawValue:
            barView?.refreshUI(refresh: refresh, completion: {
            })
            break
        default:
            break
        }
    }
    
}

//
//  DetailsBaseViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/20/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class DetailsBaseViewController: BaseViewController {
    
    //MARK:- Properties
    @IBOutlet weak var tableView: UITableView!
    var expand:Bool = false
    var detailViewModel:DetailViewModel? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.configureTableView()
        addBackButton()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension DetailsBaseViewController : SubmitReviewDelegate, DetailsViewDelegate {
    
    
    //MARK: - ReviewView Delegate
    func reviewValueDidChange(value: CGFloat) {
        expandCollaps(expand: true)
    }
    
    func submitReviewDidClicked() {
        expandCollaps(expand: false)
    }
    
    func reviewCommentsDone() {
        expandCollaps(expand: false)
    }
    
    func expandCollaps(expand:Bool) {
        self.expand = expand
        let indexPath = IndexPath(row: 3, section: 0)
        updateTableViewToExpandCollapsCell(indexPath)
    }
    
    func updateTableViewToExpandCollapsCell(_ indexPath:IndexPath) {
        
        if let cell = getVisibleCellForIndex(tableView: self.tableView, row: indexPath.row, section: indexPath.section) as? SubmitReviewCell {
            guard let viewModel = detailViewModel else { return }
            cell.configureCell(expand, viewModel: viewModel)
            let offset = self.tableView.contentOffset
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            self.tableView.layer.removeAllAnimations()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                self.tableView.setContentOffset(offset, animated: false)
                self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    //MARK: - DetailsView Delegate Delegate
    func favouriteDidClicked() {
        
    }
    
    func viewNoteDidClicked() {
        let sakeStoryBoard = UIStoryboard(name: StoryboardName.PlaceDeatails, bundle: nil)
        let noteVC = sakeStoryBoard.instantiateViewController(withIdentifier: String.className(ViewNoteViewController.self)) as! ViewNoteViewController
        navigationController?.pushViewController(noteVC, animated: true)
    }
    
    func addNoteDidClicked() {
        
    }
    
    func tasteChartDidClicked() {
        
    }
    
}

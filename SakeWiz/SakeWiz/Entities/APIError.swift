//
//  APIError.swift
//  SakeWiz
//
//  Created by sanjana on 10/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

public class APIError: Mappable {
    
    var message: String?
    var title: String?
    
    required public init?(map: Map) {
        if map.JSON["status"] == nil {
            return nil
        }
        if let statusCode = map.JSON["status"] as? Int {
            if case 200 ... 300 = statusCode {
                return nil
            }
        }
    }
    
    public func mapping(map: Map) {
        message <- map["message"]
        title <- map["error"]
    }
    
    func getMessage() -> String {
        if let msg  = self.message {
            return msg
        } else {
            return "someting_wrong".localized()
        }
    }
    
    func getTitle() -> String {
        if let t  = self.title {
            return t
        } else {
            return "error".localized()
        }
    }
    
}

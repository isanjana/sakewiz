//
//  FavouriteSakeView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/21/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

@IBDesignable class FavouriteSakeView: NibView {

   //MARK:- Properties
    @IBOutlet weak var tableView: UITableView!
     var favSakeViewModel: FavouriteSakeViewModel? = nil
    
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        registerNibFile()

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        registerNibFile()
    }
    
    func registerNibFile() {
        tableView.configureTableView()
        tableView?.register(UINib(nibName: "FavouriteSakeCell", bundle: nil), forCellReuseIdentifier: FavouriteSakeCell.identifier)
    }
    
    convenience init(withViewModel viewModel: FavouriteSakeViewModel) {
        self.init()
        favSakeViewModel = viewModel
    }
    
        
    func refreshUI(refresh:Bool, completion: @escaping () -> Void) {
        self.showLoader()
        if refresh || favSakeViewModel?.getProductsCount() == 0 {
            favSakeViewModel?.getUserFavouriteProducts(completion: { (error) in
                completion()
                self.hideLoader()
                self.tableView.reloadData()
            })
        } else{
            completion()
            self.hideLoader()
            self.tableView.reloadData()
        }
        
    }
}


extension FavouriteSakeView : UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = favSakeViewModel?.getProductsCount() {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sakeCell = self.tableView.dequeueReusableCell(withIdentifier: FavouriteSakeCell.identifier, for: indexPath) as! FavouriteSakeCell
        guard let productViewModel = favSakeViewModel?.favProductForIndex(index: indexPath.row) else { return sakeCell }
        sakeCell.configureCell(productViewModel: productViewModel)
        return sakeCell
        
    }
    
    //MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let prodID = favSakeViewModel?.getProductIdForIndex(index: indexPath.row) {
            EventBus.postToMainThread(TaskName.ShowProductDetailsController, userInfo: ["id":prodID])
        }
    }
}

//
//  DashboardCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/8/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class DashboardCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var wizImageView: UIImageView!
    @IBOutlet weak var userRankLable: UILabel!
    @IBOutlet weak var userPointsLabel: UILabel!
    @IBOutlet weak var userRankSeperator: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var favSakeCountLabel: UILabel!
    @IBOutlet weak var favPlaceCountLabel: UILabel!
    @IBOutlet weak var followersCountLabel: UILabel!
    @IBOutlet weak var followingCountLabel: UILabel!
    @IBOutlet weak var labelsCountLabel: UILabel!
    @IBOutlet weak var wizCreditsCountLabel: UILabel!
    @IBOutlet weak var favSakeView: UIView!
    @IBOutlet weak var favPlaceView: UIView!
    @IBOutlet weak var followersView: UIView!
    @IBOutlet weak var followingView: UIView!
    static let FAV_SAKE_TAG =  111
    static let FAV_PLACE_TAG = 112
    static let FOLLOWERS_TAG = 113
    static let FOLLOWING_TAG = 114

    //MARK:- Nib init
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImageView.circular(borderColor: .profileImageBorderColor(), borderWidth: 2)
        favSakeView.addTapGesture(target: self, action: #selector(openFavouriteVC(_:)))
        favSakeView.tag = DashboardCell.FAV_SAKE_TAG
        favPlaceView.addTapGesture(target: self, action: #selector(openFavouriteVC(_:)))
        favPlaceView.tag = DashboardCell.FAV_PLACE_TAG
        followersView.addTapGesture(target: self, action: #selector(openFollowVC(_:)))
        followersView.tag = DashboardCell.FOLLOWERS_TAG
        followingView.addTapGesture(target: self, action: #selector(openFollowVC(_:)))
        followingView.tag = DashboardCell.FOLLOWING_TAG
    }
    
    //MARK:- Configure Cell
    func configureCell(userDashboardViewModel:UserDashboardViewModel) {
        
        guard let userDashboard = userDashboardViewModel.userDashboard else { return }
        
        //User Rank
        if let userRank = userDashboard.userRank {
            userRankLable.text = userRank.localized()
        }
        if let rankImageUrl = SakeWizUrlUtils.rankImageUrl(rank: userDashboard.userRank) {
            wizImageView.imageFromUrl(rankImageUrl, showLoader: false)
        }
        
        //User Points
        if let points = userDashboard.pointsNeededForNextRank {
            userPointsLabel.text = String(format: "user_points".localized(), points)
            if points == 0 {
                userPointsLabel.text = "no_rank_text".localized()
            }
        } else{
            userPointsLabel.text = "no_rank_text".localized()
        }
        
        //Profile Image
        if let userProfileImageUrl = SakeWizUrlUtils.getMainImageUrl(imgUrl: userDashboard.userProfileImg) {
            profileImageView.imageFromUrl(userProfileImageUrl, showLoader: true)
        }
        
        
        //Fav Sake Count
        if let favSakeCount = userDashboard.favSakeCount {
            favSakeCountLabel.text = String(format: "%d", favSakeCount)
        }
        
        if let barCount = userDashboard.favBarCount, let breweryCount = userDashboard.favBreweryCount {
            //Fave Place Count
            favPlaceCountLabel.text = String(format: "%d", barCount + breweryCount)
        }
        
        //Followers
        if let folowers = userDashboard.followerCount {
            followersCountLabel.text = String(format: "%d", folowers)
        }
        //Following
        if let following = userDashboard.followingCount {
            followingCountLabel.text = String(format: "%d", following)
        }
        //Labels
        if let lables = userDashboard.labelCount {
            labelsCountLabel.text = String(format: "%d", lables)
        }
        //Wiz-Credits
        if let wizPoints = userDashboard.wizPoints {
            wizCreditsCountLabel.text = String(format: "%d", wizPoints)
        }
        
    }
    

    func openFavouriteVC(_ sender:UITapGestureRecognizer) {
        if let view = sender.view {
            EventBus.postToMainThread(TaskName.ShowFavouriteController, userInfo: ["view":view])
        }
    }
    
    func openFollowVC(_ sender:UITapGestureRecognizer) {
        if let view = sender.view {
            EventBus.postToMainThread(TaskName.ShowFollowViewController, userInfo: ["view":view])
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

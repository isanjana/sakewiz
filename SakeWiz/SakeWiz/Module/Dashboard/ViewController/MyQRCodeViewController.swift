//
//  MyQRCodeViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/25/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import QRCode

class MyQRCodeViewController: BaseViewController {

    //MARK:- Properties
    @IBOutlet weak var qrCodeImgV: UIImageView!
    
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initailSetUp()
    }
    
    func initailSetUp() {
        addBackButton()
        setScreenTitle("my_qr_code".localized())
        if let hndl = Utils.getUserHandel() {
            if let qrCode = QRCode(hndl) {
                qrCodeImgV.image = qrCode.image
            }
        }
    }
    
    
    @IBAction func scanQRCode(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: false)
            let qrCodeScannerVC = storyboard?.instantiateViewController(withIdentifier: String.className(QRCodeScannerViewController.self)) as! QRCodeScannerViewController
            navController.pushViewController(qrCodeScannerVC, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

//
//  BarService.swift
//  SakeWiz
//
//  Created by sanjana on 26/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import Alamofire

class BarService {
    
    static let sharedInstance = BarService()

    func getBarDetails(barID: String, completion: @escaping (_ placeDetail: PlaceDetail?, _ error: APIError?) -> Void) {
        API.sharedInstance.request(APIEndpoint.fetchBarDetails(barID: barID)) { (placeDetail: PlaceDetail?, error: APIError?) in
            completion(placeDetail, error)
        }
    }
    
    
    func markFav(barID: String, completion: @escaping (_ responseDict: [String: Any]?, _ error: APIError?) -> Void) {
        API.sharedInstance.sessionManager.request(APIEndpoint.markBarAsFavour(barID: barID)).responseJSON { (dataResponse) in
            ServiceUtils.processResponse(dataResponse: dataResponse, completion: completion)
        }
    }
    
    func markUnFav(barID: String, completion: @escaping (_ responseDict: [String: Any]?, _ error: APIError?) -> Void) {
        API.sharedInstance.sessionManager.request(APIEndpoint.markBarAsUnFavour(barID: barID)).responseJSON { (dataResponse) in
            ServiceUtils.processResponse(dataResponse: dataResponse, completion: completion)
        }
    }
    
    func postReview(barID: String, comment: String ,rate: Double, completion: @escaping (_ responseDict: [String: Any]?, _ error: APIError?) -> Void) {
        let commentInfo = [Utils.getLanguageCode() : comment]
        let parameteres = [ServiceConstant.RATE : rate, ServiceConstant.COMMENT : commentInfo] as [String : Any]
        API.sharedInstance.sessionManager.request(APIEndpoint.postBarReview(barID: barID, parameters: parameteres)).responseJSON { (dataResponse) in
            ServiceUtils.processResponse(dataResponse: dataResponse, completion: completion)
        }
    }
    
}

//
//  ViewNoteViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/22/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class ViewNoteViewController: BaseViewController {
    //MARK:- Properties
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var sakeImageView: UIImageView!
    @IBOutlet weak var sakeName: UILabel!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var createdLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    var isAddNote = true
    

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
    }
    
    func initialSetUp() {
        addBackButton()
        setScreenTitle("view_note".localized())
        noteTextView.applyPlaceholderStyle("write_note_here".localized())
        noteTextView.addToolBar(target: self, action: #selector(self.hideNoteKeyboard))
        noteTextView.isUserInteractionEnabled = noteTextView.text == "write_note_here".localized()  ? true : false
        view.addTapGesture(target: self, action: #selector(hideNoteKeyboard))
        if isAddNote {
            addRightBarButton(title: "save".localized(), selector: #selector(saveNote))
            bottomView.isHidden = true
            setScreenTitle("add_note".localized())
            makeNoteEditable()
        }
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    //MARK:- Delete Note
    @IBAction func deleteNoteDidClicked(_ sender: Any) {
    }
    
    //MARK:- Edit Note
    @IBAction func editNoteDidClicked(_ sender: Any) {
        makeNoteEditable()
    }
    
    func saveNote() {
        
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ViewNoteViewController: UITextViewDelegate {
    
    func hideNoteKeyboard() {
        noteTextView.resignFirstResponder()
    }
    
    // MARK:=======>TextView Delagate
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "comments".localized() {
            textView.moveCursorToStart()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:NSString = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with:text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.characters.count == 0 {
            textView.applyPlaceholderStyle("write_note_here".localized())
            textView.moveCursorToStart()
            return false
        }
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, clear
            // the text view and set its color to black to prepare for
            // the user's entry
        else
            if textView.textColor == .lightGray && text.characters.count > 0 {
                textView.text = nil
                textView.textColor = .darkGray
        }
        
        return true
    }
    
    func makeNoteEditable() {
        noteTextView.isUserInteractionEnabled = true
        noteTextView.becomeFirstResponder()
        noteTextView.updateLayer(borderColor: .appGrayColor(), cornerRadius: ValueConstant.DEFAULT_RADIUS, borderWidth: 1)
        
        if navigationItem.rightBarButtonItem != nil {
            addRightBarButton(title: "save".localized(), selector: #selector(saveNote))
        }
    }
    
    func setScrollViewOffset() {
        scrollView.setContentOffset(CGPoint(x: 0, y: 150), animated: true)
    }
    
    func resetScrollViewOffset() {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
     func adjustForKeyboard(notification: Notification) {
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            resetScrollViewOffset()
        } else {
           setScrollViewOffset()
        }
        
        
    }

}

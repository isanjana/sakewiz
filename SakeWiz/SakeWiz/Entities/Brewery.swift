//
//  Brewery.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class Brewery: Mappable {
    
    var id: String?
    var userId: String?
    var name: [String:Any] = [:]
    var lat: Double?
    var lon: Double?
    var email: String?
    var address: String?
    var estbYear: String?
    var isTasting: Bool?
    var canBuy: Bool?
    var cityId: String?
    var countryId: String?
    var created: Int64?
    var fax: String?
    var imgs: [String] = []
    var disable: Bool?
    var isOpenToPublic: Bool?
    var isToursAvail: Bool?
    var longDesc: [String:Any] = [:]
    var shortDesc: [String:Any] = [:]
    var mainImg: Int?
    var openingHours: String?
    var other: String?
    var postcode: String?
    var repr: String?
    var reservationRequired: Bool?
    var subzone: String?
    var supportedLangs:[String] = []
    var tel:String?
    var vdoUrl: String?
    var web: String?
    var yearVisits: Int?
    
 
    
    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        
        id <- map["id"]
        userId <- map["userId"]
        name <- map["name"]
        lat <- map["lat"]
        lon <- map["lon"]
        email <- map["email"]
        address <- map["address"]
        estbYear <- map["estbYear"]
        isTasting <- map["isTasting"]
        canBuy <- map["canBuy"]
        cityId <- map["cityId"]
        countryId <- map["countryId"]
        created <- map["created"]
        fax <- map["fax"]
        imgs <- map["imgs"]
        disable <- map["disable"]
        isOpenToPublic <- map["isOpenToPublic"]
        longDesc <- map["longDesc"]
        shortDesc <- map["shortDesc"]
        mainImg <- map["mainImg"]
        openingHours <- map["openingHours"]
        other <- map["other"]
        postcode <- map["postcode"]
        repr <- map["repr"]
        reservationRequired <- map["reservationRequired"]
        subzone <- map["subzone"]
        tel <- map["tel"]
        vdoUrl <- map["vdoUrl"]
        web <- map["web"]
        yearVisits <- map["yearVisits"]
    }
}

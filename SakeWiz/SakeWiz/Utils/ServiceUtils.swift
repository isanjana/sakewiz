//
//  ServiceUtils.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/26/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import Alamofire

class ServiceUtils: NSObject {
    
   class func processResponse(dataResponse: DataResponse<Any>, completion: @escaping (_ responseDict: [String: Any]?, _ error: APIError?) -> Void) {
        if (dataResponse.result.isSuccess) {
            if let resultDict = dataResponse.result.value as? [String: Any] {
                completion(resultDict, nil)
            } else {
                //TODO: SAN Test if it works ok  - when you send request without token, control should go here
                let rp = dataResponse.value as! [String : Any]
                if let apiError = APIError(JSON:rp) {
                    completion(nil, apiError)
                }
                
            }
        } else {
            //TODO: SAN Test if it works ok  - When you are offline for example, control should come here
             let dict = [ServiceConstant.ERROR_KEY : ServiceConstant.ERROR_VALUE, ServiceConstant.MESSAGE : dataResponse.error!.localizedDescription]
             let apiError = APIError(JSON: dict)
             completion(nil, apiError)

        }
    }
    
    class func stringArrayFromResponse(dataResponse: DataResponse<Any>) -> Array<String>? {
        if (dataResponse.result.isSuccess) {
            if let resultArray = dataResponse.result.value as? Array<String> {
                return resultArray
            }
        }
        return nil
    }
}

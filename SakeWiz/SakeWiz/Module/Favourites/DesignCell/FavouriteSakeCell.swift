//
//  FavouriteCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/21/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class FavouriteSakeCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var productView: ProductView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- Configure Cell
    func configureCell(productViewModel:ProductViewModel) {
        productView.updateView(productViewModel: productViewModel)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  FavouritePlaceCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/21/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class FavouritePlaceCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var placeView: PlaceView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- Configure Cell
    func configureCell(placeViewModel:PlaceViewModel) {
        placeView.updateView(placeViewModel: placeViewModel)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

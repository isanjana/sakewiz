//
//  SubmitReviewCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/15/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class SubmitReviewCell: UITableViewCell {
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var submitReviewView: SubmitReviewView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ selected:Bool, viewModel:DetailViewModel) {
        if selected {
            submitReviewView.comentBoxGrow()
        } else {
            submitReviewView.commentBoxSrink()
        }
        
        submitReviewView.updateView(viewModel: viewModel)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



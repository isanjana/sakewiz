//
//  Product.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class Product: Mappable {
    
    var id: String?
    var userId: String?
    var name: [String:Any] = [:]
    var mainImgUrl: String = ""
    var category: String?
    var type: String?
    var filterWater: String?
    var yeast: String?
    var pastorizeTankStorage: String?
    var riceKoji: String?
    var riceKake: String?
    var kakeRatio: Float?
    var kojiRatio: Float?
    var alcPerc: Float?
    var brewYear: Int?
    var aminoAcid: Int?
    var acidity: Float?
    var inProduction: Bool?
    var avlToPublic: Bool?
    var live: Bool?
    var createType: String?
    var labelExist: Bool?
    var rate: Float?
    var reviewCount: Int?
    var favoured: Int?
    var countryCd: String?
    var countryName: [String:Any] = [:]
    var regionCd: String?
    var regionName: [String:Any] = [:]
    var subRegionCd: String?
    var created: Int64?
    var updated: Int64?
    var smv: Int?
    var aromaObservation:[String] = []
    var bonusUserId: String?
    var createStatus: String?
    var deleted: Bool?
    var flavorType: String?
    var imgs:[String] = []
    var lblImg: String?
    var lblImgs:[String] = []
    var longDesc: [String:Any] = [:]
    var mainImg: Int?
    var scanJobId: String?
    var shortDesc: [String:Any] = [:]
    var size: Int?
    var sizeType: String?
    var tasteObservation: [String] = []
    var tempChilled: Bool?
    var tempHot: Bool?
    var tempIce: Bool?
    var tempRoom: Bool?
    var tempVHot:Bool?
    var tempWarm: Bool?
    var createState: String?
    var hasUserNote: Bool?
    var canGainPoints: Bool?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        userId <- map["userId"]
        name <- map["name"]
        mainImgUrl <- map["mainImgUrl"]
        category <- map["category"]
        type <- map["type"]
        filterWater <- map["filterWater"]
        yeast <- map["yeast"]
        pastorizeTankStorage <- map["pastorizeTankStorage"]
        riceKoji <- map["riceKoji"]
        riceKake <- map["riceKake"]
        kakeRatio <- map["kakeRatio"]
        alcPerc <- map["alcPerc"]
        brewYear <- map["brewYear"]
        aminoAcid <- map["aminoAcid"]
        acidity <- map["acidity"]
        inProduction <- map["inProduction"]
        avlToPublic <- map["avlToPublic"]
        live <- map["live"]
        createType <- map["createType"]
        labelExist <- map["labelExist"]
        rate <- map["rate"]
        reviewCount <- map["reviewCount"]
        favoured <- map["favoured"]
        countryCd <- map["countryCd"]
        countryName <- map["countryName"]
        regionName <- map["regionName"]
        regionCd <- map["regionCd"]
        subRegionCd <- map["subRegionCd"]
        created <- map["created"]
        updated <- map["updated"]
        smv <- map["smv"]
        aromaObservation <- map["aromaObservation"]
        bonusUserId <- map["bonusUserId"]
        createStatus <- map["createStatus"]
        deleted <- map["deleted"]
        flavorType <- map["flavorType"]
        imgs <- map["imgs"]
        lblImg <- map["lblImg"]
        lblImgs <- map["lblImgs"]
        longDesc <- map["longDesc"]
        mainImg <- map["mainImg"]
        scanJobId <- map["scanJobId"]
        shortDesc <- map["shortDesc"]
        size <- map["size"]
        sizeType <- map["sizeType"]
        tasteObservation <- map["tasteObservation"]
        tempChilled <- map["tempChilled"]
        tempHot <- map["tempHot"]
        tempIce <- map["tempIce"]
        tempRoom <- map["tempRoom"]
        tempVHot <- map["tempVHot"]
        tempWarm <- map["tempWarm"]
        createState <- map["createState"]
        hasUserNote <- map["hasUserNote"]
        canGainPoints <- map["canGainPoints"]

        
    }
}

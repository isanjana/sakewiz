//
//  PlaceDetailsViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/18/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import ExpandableLabel

class PlaceDetailsViewController: DetailsBaseViewController {
    
    //MARK:-  Properties 
    var placeDetailsViewModel:PlaceDetailsViewModel? = nil
    

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailViewModel = placeDetailsViewModel
        loadPlaceDetails()
        
    }
    
    func loadPlaceDetails() {
        showProgress(title: "")
        placeDetailsViewModel?.loadPlaceDetails(completion: { (error) in
            self.hideProgress()
            if error != nil {
                if DEBUG.isDebuggingEnabled {
                    self.showBannerAlert(title: error!.getTitle(), message: error!.getMessage(), style: .warning, delegate: nil)
                }
                return;
            }
            self.tableView.reloadData()
            self.placeDetailsViewModel?.getPlaceReviews(loadMore: false, completion: { (error) in
                 self.tableView.reloadData()
            })
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


extension PlaceDetailsViewController : UITableViewDataSource, UITableViewDelegate, ExpandableLabelDelegate {
    
    //MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = placeDetailsViewModel else { return 0 }
        return viewModel.getRowCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let viewModel = placeDetailsViewModel else { return PlaceDetailsCell() }
        let sections = viewModel.getSections()
        var sectionType = PlaceDetailsSection.reviews
        if indexPath.row < sections.count {
            sectionType = sections[indexPath.row]
        }
        
        switch sectionType {
        case .placeDetailHeader:
            
            let placeDetailsCell = self.tableView.dequeueReusableCell(withIdentifier: PlaceDetailsCell.identifier, for: indexPath) as! PlaceDetailsCell
            guard let viewModel = placeDetailsViewModel else { return placeDetailsCell }
            placeDetailsCell.configureCell(viewModel: viewModel)
            placeDetailsCell.placeDetailsView.delegate = self
            return placeDetailsCell
            
        case .recommend:
            
            let recommendationCell = self.tableView.dequeueReusableCell(withIdentifier: LatestRecommendationCell.identifier, for: indexPath) as! LatestRecommendationCell
            guard let viewModel = placeDetailsViewModel else { return recommendationCell }
            recommendationCell.configureCell(viewModel: viewModel)
            recommendationCell.titleLabel.delegate = self
            return recommendationCell
            
        case .gallery:
            
            let productGalleryCell = self.tableView.dequeueReusableCell(withIdentifier: ProductGallleryCell.identifier, for: indexPath) as! ProductGallleryCell
            guard let viewModel = placeDetailsViewModel else { return productGalleryCell }
            productGalleryCell.configureCell(viewModel: viewModel)
            return productGalleryCell
            
        case .rating:
            
            let submitReviewCell = self.tableView.dequeueReusableCell(withIdentifier: SubmitReviewCell.identifier, for: indexPath) as! SubmitReviewCell
            submitReviewCell.submitReviewView.delegate = self
            guard let viewModel = detailViewModel else { return submitReviewCell }
            submitReviewCell.configureCell(expand, viewModel: viewModel)
            return submitReviewCell
            
        case .availablity:
            
            let serviceCell = self.tableView.dequeueReusableCell(withIdentifier: ServiceAvailabilityCell.identifier, for: indexPath) as! ServiceAvailabilityCell
            guard let viewModel = placeDetailsViewModel else { return serviceCell }
            serviceCell.configureCell(viewModel: viewModel)
            return serviceCell
            
        default:
            let reviewCell = self.tableView.dequeueReusableCell(withIdentifier: ReviewsCell.identifier, for: indexPath) as! ReviewsCell
            guard let reviewViewModel = placeDetailsViewModel?.getReviewForIndex(index: indexPath.row - sections.count) else { return reviewCell }
            reviewCell.configureCell(hideTitleView: indexPath.row == PlaceDetailsSection.reviews.rawValue ? false : true, reviewViewModel: reviewViewModel)
            return reviewCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.row == PlaceDetailsSection.rating.rawValue {
            //toggel the expand value.
            expand = !expand
            updateTableViewToExpandCollapsCell(indexPath)
        }
    }
    
    //MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK:- ExpandableLabel Delegate
    func willExpandLabel(_ label: ExpandableLabel) {
        tableView.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tableView.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
    }
       
}

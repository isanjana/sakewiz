//
//  UserSearchViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/25/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation

class UserSearchViewModel {
    
    //MARK:- Properties
    fileprivate var wizUser: WizUser? = nil
    var fromMenu:Bool = false
}

extension UserSearchViewModel {
    
    //MARK:- Search User By Handle
    func searchUserByHandle(userHandle: String, completion: @escaping (_ error: APIError?) -> Void) {
        UserService.sharedInstance.searchUserWithHandle(userHandle: userHandle) { (wizUser, error) in
            
            if let error = error {
                completion(error)
                return;
            }
            self.wizUser = wizUser
            completion(nil)
        }
    }
    
    
    //MARK:- Helper Methods
    func getUserViewModel() -> UserViewModel? {
        
        if let wizUser = self.wizUser {
            let userViewModel = UserViewModel(withUser: wizUser)
            return userViewModel
        }
        
        return nil
    }
    
    func getUserId() -> String? {
        return wizUser?.userId
    }
    
    func getRowCount() -> Int {
        return wizUser != nil ? 1 : 0
    }

}

//
//  ProductMandatoryDataView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/12/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

@IBDesignable class ProductMandatoryDataView: NibView {
    
    //MARK:- Properties
    @IBOutlet weak var gardeLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var byLabel: UILabel!
    @IBOutlet weak var polishRateLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var smvLabel: UILabel!
    @IBOutlet weak var alcoholContentLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var wizPointsLabel: UILabel!
    
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updatePagerViewLayer()
        profileImageView.circular(borderColor: .clear, borderWidth: 0)
    }

}

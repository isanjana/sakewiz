//
//  ProductDetailsView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/12/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

@IBDesignable class ProductDetailsView: NibView {
    
    //MARK:- Properties
    @IBOutlet weak var productDetailImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var breweryNameLabel: UILabel!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var ratingCountLabel: UILabel!
    public weak var delegate: DetailsViewDelegate!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK:- Favourite
    @IBAction func favouriteAction(_ sender: UIButton) {
        delegate?.favouriteDidClicked?()
        
    }
    
    //MARK:- Taste Chart
    @IBAction func tasteChartAction(_ sender: UIButton) {
        delegate?.tasteChartDidClicked?()
    }
    
    //MARK:- View Note
    @IBAction func addNoteAction(_ sender: UIButton) {
        delegate?.viewNoteDidClicked?()
    }
    
    
}



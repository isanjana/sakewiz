//
//  LoginViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import UITextField_Navigation

class LoginViewController: SignInBaseViewController {

    //MARK:- Properties
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPwdButton: UIButton!
    
    var viewModel = LoginViewModel()
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- Login
    @IBAction func loginAction(_ sender: Any) {
        
        switch viewModel.validate() {
        case .Valid:
            showProgress(title: "login_progress_message".localized())
            viewModel.login { (error) in
                self.hideProgress()
                if error != nil {
                    self.showBannerAlert(title: error!.getTitle(), message: error!.getMessage(), style: .warning, delegate: nil)
                    return;
                }
                
                //if succes go to dasboard screen.
                AppDelegate.sharedAppDelegate().setDashboardRootController()
            }
        case .Invalid(let errorString):
            showBannerAlert(title: "error".localized(), message: errorString, style: .warning, delegate: nil)
        }

    }
    
    //MARK:- Forgot Password
    @IBAction func forgotPassword(_ sender: Any) {
        
        let forgotPwdVC = storyboard?.instantiateViewController(withIdentifier: String.className(ForgotPasswordViewController.self)) as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotPwdVC, animated: true)
    }
    
    //MARK:-  View SetUp
    override func viewSetUP() {
        super.viewSetUP()
        setScreenTitle("sign_in_with_email")
        userNameTF.nextNavigationField = passwordTF
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension LoginViewController {
        
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text! as NSString
        let updatedText = currentText.replacingCharacters(in: range, with:string)
        
        if textField == userNameTF {
            viewModel.updateUsername(username: updatedText)
            return true
        }
        
        if textField == passwordTF {
            viewModel.updatePassword(password: updatedText)
            return true
        }
        
        return true
    }
}

//
//  DashboardViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/7/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class DashboardViewController: BaseViewController {
    
    //MARK:- Properties
    @IBOutlet weak var tableView: UITableView!
    var userDashboardViewModel = UserDashboardViewModel()

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addHambergerButton()
        tableView.configureTableView()
        addRightBarButton(imageName: "Search", selector: #selector(searchButtonDidClicked))
        setScreenTitle("dashboard")
        fetchUserDashboard()
    }
    
    //MARK:- Fetch User Dashboard
    func fetchUserDashboard() {
        
        showProgress(title: "")
        self.userDashboardViewModel.fetchUserDashboard { error in
            self.hideProgress()
            if error != nil {
                if DEBUG.isDebuggingEnabled {
                    self.showBannerAlert(title: error!.getTitle(), message: error!.getMessage(), style: .warning, delegate: nil)
                }
                return;
            }
            self.tableView.reloadData()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EventBus.onMainThread(self, name: TaskName.ShowUnknownSakeController) { (notification) in
            self.openUnkonwnSakeVC()
        }
        
        EventBus.onMainThread(self, name: TaskName.ShowFavouriteController) { (notification) in
            if let userInfo = notification.userInfo  {
                if let view = userInfo["view"] as? UIView {
                    if view.tag == DashboardCell.FAV_SAKE_TAG {
                        self.openFavouriteVC(isBreweries: false)
                    } else {
                        self.openFavouriteVC(isBreweries: true)
                    }
                    
                }
            }
        }
        
        EventBus.onMainThread(self, name: TaskName.ShowFollowViewController) { (notification) in
            if let userInfo = notification.userInfo  {
                if let view = userInfo["view"] as? UIView {
                    let followersVC = self.storyboard?.instantiateViewController(withIdentifier: String.className(FollowViewController.self)) as! FollowViewController
                    if view.tag == DashboardCell.FOLLOWERS_TAG {
                        followersVC.isFollowers = true
                    }
                    self.navigationController?.pushViewController(followersVC, animated: true)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


extension DashboardViewController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let userDashboard = userDashboardViewModel.userDashboard {
            //we need extra two rows for dasboard, and sake upload.
           return userDashboard.recommendedProducts.count + 2
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0: //Dashboard
            
            let dashboardCell = self.tableView.dequeueReusableCell(withIdentifier: DashboardCell.identifier, for: indexPath) as! DashboardCell
            dashboardCell.configureCell(userDashboardViewModel: userDashboardViewModel)
            return dashboardCell
            
        case 1: //SakeUploadStatus
            
            let sakeUploadCell = self.tableView.dequeueReusableCell(withIdentifier: SakeUploadStatusCell.identifier, for: indexPath) as! SakeUploadStatusCell
            sakeUploadCell.configureCell(userDashboardViewModel: userDashboardViewModel)
            return sakeUploadCell
            
        default: //Recommended Products
            
            let productCell = self.tableView.dequeueReusableCell(withIdentifier: ProductCell.identifier, for: indexPath) as! ProductCell
            let index = indexPath.row - 2 //we reduce two cell, for dashboard, and sake upload status
            guard let productViewModel = userDashboardViewModel.recommendedProductForIndex(index: index) else { return productCell }
            let isHideTitle = indexPath.row == 2 ? false : true
            productCell.configureCell(productViewModel: productViewModel, hideTitleView: isHideTitle)
            return productCell
        }
        
    }
    
    //MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //need to select only product cell.
        if indexPath.row > 1 {
            let index = indexPath.row - 2 //we reduce two cell, for dashboard, and sake upload status
            if let prodID = userDashboardViewModel.getProductIdForIndex(index: index) {
                EventBus.postToMainThread(TaskName.ShowProductDetailsController, userInfo: ["id":prodID])
            }
        }
        
    }
    
    func openUnkonwnSakeVC() {
       
        let sakeStoryBoard = UIStoryboard(name: StoryboardName.UnknownSake, bundle: nil)
        let unknownSakeVC = sakeStoryBoard.instantiateViewController(withIdentifier: String.className(UnknownSakeViewController.self)) as! UnknownSakeViewController
        navigationController?.pushViewController(unknownSakeVC, animated: true)
    }
    
    func openFavouriteVC(isBreweries:Bool) {
        
        let favStoryBoard = UIStoryboard(name: StoryboardName.Favourite, bundle: nil)
        let favSakeVC = favStoryBoard.instantiateViewController(withIdentifier: String.className(FavouriteViewController.self)) as! FavouriteViewController
        favSakeVC.isBreweries = isBreweries
        navigationController?.pushViewController(favSakeVC, animated: true)
    }
    
    
    func searchButtonDidClicked() {
        
    }
}



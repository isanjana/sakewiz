//
//  String.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    /**
     Localized String.
     
     This method returns localized string based on preferred localizations contained in the bundle.
     
     @return
     * localized string.
     
     */
    func localized() -> String {
        
        var defaultLanguage = "en"
        
        if let language = Bundle.main.preferredLocalizations.first {
            defaultLanguage  = language
        }
        
        if let path = Bundle.main.path(forResource: defaultLanguage, ofType: "lproj"){
            let bundle = Bundle(path: path)
            return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        }
        
        return ""
    }
    
    /**
     Class Name.
     
     This method returns class name.
     
     @return
     * string.
     
     */
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    /**
     Validate E-Mail ID.
     
     This method accepts a string value representing the 'E-Mail' ID of user and validate based on given regex pattern.
     
     Hint:
     * The 'E-Mail' ID should not be empty.
     
     @return
     * 'TRUE' if predicate evaluate the given string based on define regex pattern,
     - else 'FALSE'.
     
     */
    func isValidEmail() -> Bool {
        
        if isEmptyText() {
            return false
        }
        
        var emailRegEx =  "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.(?:[A-Z]{2,}|com|co)*(\\.(?:[A-Z]{2,}|us|in|ch|jp|uk)))+$"
        
        var emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let success =  emailTest.evaluate(with: self)
        
        if(!success) {
            
            emailRegEx = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{3,})$";
            
            emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            
            return emailTest.evaluate(with: self)
            
        }
        
        return success
        
    }
    
    func isValidHandle() -> Bool {
        
        if isEmptyText() {
            return false
        }
        
        let regex = "^(?=.{4,})[a-z0-9\\-\\_]+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        let success =  predicate.evaluate(with: self)
        return success
    }
    
    func isValidPassword() -> Bool {
        
        if isEmptyText() {
            return false
        }
        
        let regex = "^(?=.{4,})[a-zA-Z0-9.!@#$%^&*()_+-=\\s\\-,]+.\\*?$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        let success =  predicate.evaluate(with: self)
        return success
    }
    
    func isEmptyText() -> Bool {
        
        if self.characters.count == 0 || self.isEmpty {
            return true
        }
        
        return false
    }
    
    func convertHtmlToPlainText() -> String? {
        
        if let data = self.data(using: .unicode, allowLossyConversion: true) {
            
            let plainText = try? NSAttributedString(
                data: (data),
                options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                documentAttributes: nil)
            
            return plainText?.string
        }
        
        return nil
        
    }
    
}

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

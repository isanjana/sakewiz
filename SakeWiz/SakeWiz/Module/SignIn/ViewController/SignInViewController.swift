//
//  SignInViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class SignInViewController: SignInBaseViewController {

    //MARK:- Properties
    @IBOutlet weak var twitterView: UIView!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var fbView: UIView!
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
        UIView.animate(withDuration: 0.4) {
            self.navigationController?.isNavigationBarHidden = true
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIView.animate(withDuration: 0.4) {
            UIApplication.shared.isStatusBarHidden = false
            self.navigationController?.isNavigationBarHidden = false
        }
        
    }
    
    
    //MARK:- SignIn With Twitter
    @IBAction func signInWithTwitter(_ sender: Any) {
    
    }
    
    //MARK:- SignIn With Facebook
    @IBAction func signInWithFacebook(_ sender: Any) {
        
    }
    
    //MARK:- SignIn With Email
    @IBAction func signInWithEmail(_ sender: Any) {
        
        let loginVC = storyboard?.instantiateViewController(withIdentifier: String.className(LoginViewController.self)) as! LoginViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    //MARK:- Register With Email
    @IBAction func registerWithEmail(_ sender: Any) {
        
        let registerVC = storyboard?.instantiateViewController(withIdentifier: String.className(RegisterViewController.self)) as! RegisterViewController
        self.navigationController?.pushViewController(registerVC, animated: true)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-  View SetUp
    override func viewSetUP() {
        setLogoGifImage()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}

//
//  PlaceDetail.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class PlaceDetail: Mappable {
    
    var avgRate: Float?
    var totalRates: Int?
    var totalFavourites: Int?
    var userRate: Float?
    var userReview: [String:Any] = [:]
    var userFavourite: Bool?
    var userHasNote: Bool?
    var brewery: Brewery?
    var bar: Bar?
 
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        avgRate <- map["avgRate"]
        totalRates <- map["totalRates"]
        totalFavourites <- map["totalFavourites"]
        userRate <- map["userRate"]
        userReview <- map["userReview"]
        userFavourite <- map["userFavourite"]
        userHasNote <- map["userHasNote"]
        brewery <- map["brewery"]
        bar <- map["bar"]
        
    }
}

//
//  UnknownSakeView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/20/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

@IBDesignable class UnknownSakeView: NibView {

    //MARK:- Properties
    
    @IBOutlet weak var unknownSakeImageV: UIImageView!
    @IBOutlet weak var scanUserLabel: UILabel!
    @IBOutlet weak var scanTimeLabel: UILabel!
    
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         unknownSakeImageV.updateLayer(borderColor: .clear, cornerRadius: ValueConstant.DEFAULT_RADIUS, borderWidth: 0)
    }
    
    func updateView(viewModel: UnknownSakeResultViewModel) {
        
        if let scanImg = viewModel.getScanImageUrl {
            unknownSakeImageV.imageFromUrl(scanImg)
        }
        
        if let scannedUserHndl = viewModel.getScanedUserHndl {
            scanUserLabel.text = scannedUserHndl
        }
        
        if let createdTime = viewModel.getCreatedTime {
            scanTimeLabel.text = createdTime
        }
    }
    
    //MARK:- Add Data Clicked
    @IBAction func addDataClicked(_ sender: Any) {
        
    }
    
    
}

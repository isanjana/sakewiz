//
//  InviteUserViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/23/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class InviteUserViewController: BaseViewController {

    //MARK:- Properties
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var userSearchViewModel = UserSearchViewModel()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        addBackButton()
        setScreenTitle("search".localized())
        tableView.configureTableView()
        searchBar.becomeFirstResponder()
        searchBar.addToolBar(target: self, action: #selector(hideSeachKeyboard))
        
    }
    
    func searchUserByHandle() {
        showProgress(title: "")
        if let searchText = searchBar.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) {
            userSearchViewModel.searchUserByHandle(userHandle: searchText.lowercased()) { (error) in
                self.hideProgress()
                if error != nil {
                    self.showBannerAlert(title: error!.getTitle(), message: error!.getMessage(), style: .warning, delegate: nil)
                    return;
                }
                self.tableView.reloadData()
            }
        }
    }
    
    override func refreshUI() {
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension InviteUserViewController : UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    //MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userSearchViewModel.getRowCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let followCell = self.tableView.dequeueReusableCell(withIdentifier: FollowViewCell.identifier, for: indexPath) as! FollowViewCell
         guard let userViewModel = userSearchViewModel.getUserViewModel() else { return followCell }
        followCell.configureCell(userViewModel)
        
        return followCell
        
    }
    
    //MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let userID = userSearchViewModel.getUserId() {
            EventBus.postToMainThread(TaskName.ShowUserProfileViewController, userInfo: ["id":userID])
        }
    }
    
    
    
    //MARK: - UISearchBarDelegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        hideSeachKeyboard()
        searchUserByHandle()
    }
    
    func hideSeachKeyboard() {
        searchBar.resignFirstResponder()
    }
    
    
    
}

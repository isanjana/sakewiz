//
//  ProductDetailResult.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

class ProductDetailResult: Mappable {
    
    var products:[ProductDetail] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        products <- map["products"]
    }
}


//
//  Notification.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class NotificationEntity: Mappable {
    
    var id: String?
    var type: String?
    var created: Int64?
    var commentId: String?
    var likedUserId: String?
    var likedUserHandle: String?
    var likedUserName: String?
    var followerUserImg: String?
    var followerUserName: String?
    var followerUserHandle: String?
    var followerUserId: String?
    var likedUserImg: String?
    var likedBreweryId: String?
    var likedBreweryName:[String:Any] = [:]
    var likedProductId: String?
    var likedBarId: String?
    var createdProductId: String?
    var likedBarName:[String:Any] = [:]
    var likedProductName:[String:Any] = [:]
    var points: Int?
    var reviewedBreweryId: String?
    var breweryName:[String:Any] = [:]
    var reviewedProductId: String?
    var productName:[String:Any] = [:]
    var reviewedBarId: String?
    var barName:[String:Any] = [:]
    var matchedProductId: String?
    var matchedProductName:[String:Any] = [:]
    var matchedProductImg: String?
    
    
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        type <- map["type"]
        created <- map["created"]
        commentId <- map["commentId"]
        likedUserId <- map["likedUserId"]
        likedUserHandle <- map["likedUserHandle"]
        likedUserName <- map["likedUserName"]
        followerUserImg <- map["followerUserImg"]
        followerUserName <- map["followerUserName"]
        followerUserHandle <- map["followerUserHandle"]
        followerUserId <- map["followerUserId"]
        likedUserImg <- map["likedUserImg"]
        likedBreweryId <- map["likedBreweryId"]
        likedBreweryName <- map["likedBreweryName"]
        likedProductId <- map["likedProductId"]
        likedBarId <- map["likedBarId"]
        createdProductId <- map["createdProductId"]
        likedBarName <- map["likedBarName"]
        likedProductName <- map["likedProductName"]
        points <- map["points"]
        reviewedBreweryId <- map["reviewedBreweryId"]
        breweryName <- map["breweryName"]
        reviewedProductId <- map["reviewedProductId"]
        productName <- map["productName"]
        reviewedBarId <- map["reviewedBarId"]
        barName <- map["barName"]
        matchedProductId <- map["matchedProductId"]
        matchedProductName <- map["matchedProductName"]
        matchedProductImg <- map["matchedProductImg"]
        
    }
}

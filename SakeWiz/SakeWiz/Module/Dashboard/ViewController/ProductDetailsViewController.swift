//
//  ProductDetailsViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/11/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class ProductDetailsViewController: DetailsBaseViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setScreenTitle("product_details")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension ProductDetailsViewController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            
            let prodDetailsCell = self.tableView.dequeueReusableCell(withIdentifier: ProductDeatailsCell.identifier, for: indexPath) as! ProductDeatailsCell
            prodDetailsCell.productDetailsView.delegate = self
            return prodDetailsCell
            
        case 1:
            
            let productGalleryCell = self.tableView.dequeueReusableCell(withIdentifier: ProductGallleryCell.identifier, for: indexPath) as! ProductGallleryCell
            return productGalleryCell
            
        case 2:
            
            let productMandatoryDataCell = self.tableView.dequeueReusableCell(withIdentifier: ProductMandatoryDataCell.identifier, for: indexPath) as! ProductMandatoryDataCell
            return productMandatoryDataCell
            
        case 3:
            
            let submitReviewCell = self.tableView.dequeueReusableCell(withIdentifier: SubmitReviewCell.identifier, for: indexPath) as! SubmitReviewCell
            submitReviewCell.configureCell(expand, viewModel: DetailViewModel())
            submitReviewCell.submitReviewView.delegate = self
            return submitReviewCell
            
        default:
            let reviewCell = self.tableView.dequeueReusableCell(withIdentifier: ReviewsCell.identifier, for: indexPath) as! ReviewsCell
            //reviewCell.configureCell(hideTitleView: indexPath.row == 4 ? false : true)
            return reviewCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.row == 3 {
            //toggel the expand value.
            expand = !expand
            updateTableViewToExpandCollapsCell(indexPath)
        }
    }
    
    //MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

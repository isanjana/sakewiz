//
//  UIViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/20/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit
import NotificationBannerSwift
import ACProgressHUD_Swift

extension UIViewController {
    
    //MARK:- Show Banner Alert
    func showBannerAlert(title:String, message:String, style:BannerStyle, delegate:UIViewController?) {
        
        switch style {
            
        case .warning:
            let leftView = UIImageView(image: UIImage(named: "Banner_Warning"))
            let warningBanner = NotificationBanner(title: title, subtitle: message, leftView: leftView, style: style)
            warningBanner.delegate = delegate.self as? NotificationBannerDelegate
            warningBanner.show()
            break
            
        case .success:
            let leftView = UIImageView(image: UIImage(named: "Banner_Success"))
            let successBanner = NotificationBanner(title: title, subtitle: message, leftView: leftView, style: style)
            successBanner.delegate = delegate.self as? NotificationBannerDelegate
            successBanner.show()
            break
            
        case .info:
            break
            
        case .none:
            break
            
        case .danger:
            break
            
        }
        
    }
    
    //MARK:- Progress Loader
    func showProgress(title:String) {
        let progressView = ACProgressHUD.shared
        progressView.progressText = title
        ACProgressHUD.shared.indicatorColor = .white
        ACProgressHUD.shared.progressTextColor = .white
        ACProgressHUD.shared.hudBackgroundColor = .appThemColor()
        progressView.showHUD()
    }
    
    func hideProgress() {
        ACProgressHUD.shared.hideHUD()
    }
    
    
    //MARK:- keyboard Show/Hide
    func setupViewResizerOnKeyboardShown() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(UIViewController.keyboardWillShowForResizing),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(UIViewController.keyboardWillHideForResizing),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func keyboardWillShowForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let window = self.view.window?.frame {
            // We're not just minusing the kb height from the view height because
            // the view could already have been resized for the keyboard before
            self.view.frame = CGRect(x: self.view.frame.origin.x,
                                     y: self.view.frame.origin.y,
                                     width: self.view.frame.width,
                                     height: window.origin.y + window.height - keyboardSize.height)
        }
    }
    
    func keyboardWillHideForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.view.frame.height
            self.view.frame = CGRect(x: self.view.frame.origin.x,
                                     y: self.view.frame.origin.y,
                                     width: self.view.frame.width,
                                     height: viewHeight + keyboardSize.height)
        }
    }
    
    func removeKeyboardObserver() {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
}

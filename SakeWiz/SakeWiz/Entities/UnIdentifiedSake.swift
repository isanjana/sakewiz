//
//  UnIdentifiedSake.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

class UnIdentifiedSake: Mappable {
    
    var scanId: String?
    var scanImg: String?
    var scannedUserHndl: String?
    var created: Int64?
    var scannedUserId: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        scanId <- map["scanId"]
        scanImg <- map["scanImg"]
        scannedUserHndl <- map["scannedUserHndl"]
        created <- map["created"]
        scannedUserId <- map["scannedUserId"]
    }
}

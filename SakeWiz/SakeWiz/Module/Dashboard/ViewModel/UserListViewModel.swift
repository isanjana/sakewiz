//
//  UserListViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/25/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation


class UserListViewModel {
    
    //MARK:- Properties
    fileprivate var userList:[WizUser] = []
    var userListType:UserListType = .notSet

    
    //MARK:- Initialization
    convenience init(withListType type: UserListType) {
        self.init()
        userListType = type
    }
    
}


extension UserListViewModel {
    
    func loadUserList(completion: @escaping (_ count:Int, _ error: APIError?) -> Void) { // count
        
        switch userListType {
        case .following:
            UserService.sharedInstance.fetchUserFollowingList(userID: Utils.getUserID()!) { (stremUserResult, error) in
                if let error = error {
                    completion(0, error)
                    return;
                }
                
                if let users = stremUserResult?.results {
                    self.handleUserList(users: users)
                    completion(users.count, nil)
                    return
                }
                
                completion(0, nil)
            }
            break
            
        case .followers:
            UserService.sharedInstance.fetchUserFollowersList(userID: Utils.getUserID()!) { (stremUserResult, error) in
                if let error = error {
                    completion(0, error)
                    return;
                }
                
                if let users = stremUserResult?.results {
                    self.handleUserList(users: users)
                    completion(users.count, nil)
                    return
                }
                
                completion(0, nil)
            }
            break
        
        default:
            break
        }
        
    }
    
    private func handleUserList(users:[WizUser]) {
        self.userList.removeAll()
        self.userList.append(contentsOf: users)
    }
    
    //MARK:- Helper Methods
    
    func getUsersForIndex(index:Int) -> UserViewModel? {
        
        if userList.count > 0 {
            let userViewModel = UserViewModel(withUser: userList[index])
            return userViewModel
        }
        
       return nil
    }

    func getUserCount() -> Int {
        return userList.count > 0 ? userList.count : 0
    }
    
    func getUserIdForIndex(index:Int) -> String? {
        return userList[index].userId
    }
}



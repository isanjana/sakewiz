//
//  NibView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/6/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit


class NibView: UIView {
    
    //MARK:- Properties
    var loader:UIActivityIndicatorView? = nil
    var view: UIView!
    
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Setup view from .xib file
        xibSetup()
    }
    
    func showLoader() {
        
        if loader == nil {
            
            loader = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            loader?.frame = CGRect(x: (UIScreen.main.bounds.size.width - 50) / 2, y: (UIScreen.main.bounds.size.height - 50) / 2, width: 50, height: 50)
            loader?.color = .appThemColor()
            loader?.hidesWhenStopped = true
            loader?.startAnimating()
            AppDelegate.sharedAppDelegate().window?.addSubview(loader!)
        }
        
    }
    
    func hideLoader() {
        
        if loader != nil {
            loader?.stopAnimating()
            loader?.removeFromSuperview()
            loader = nil
        }
    }
}


private extension NibView {
    
    func xibSetup() {
        backgroundColor = UIColor.clear
        view = loadNib()
        // use bounds not frame or it'll be offset
        view.frame = bounds
        // Adding custom subview on top of our view
        addSubview(view)
    }
}


extension UIView {
    /** Loads instance from nib with the same name. */
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}

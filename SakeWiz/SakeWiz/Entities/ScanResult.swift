//
//  ScanResult.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

class ScaResult: Mappable {
    
    var created: Int64?
    var id: String?
    var matches: [String] = []
    var stat: String?
    var updated: Int64?
    var userId: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        created <- map["created"]
        id <- map["id"]
        matches <- map["matches"]
        stat <- map["stat"]
        updated <- map["updated"]
        userId <- map["userId"]
    }
}

//
//  APIEndPoint.swift
//  SakeWiz
//
//  Created by sanjana on 08/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import Alamofire

enum APIEndpoint: URLRequestConvertible {
    
    static let baseURLString = APIEndPointConstant.BASE_URL

    //User Related
    case login(parameters: Parameters)
    case register(parameters: Parameters, token: String)
    case resetPassword(parameters: Parameters, token: String)
    case followUser(userID: String)
    case unfollowUser(userID: String)

    
    //Facade
    case fetchLoginProfile()
    case fetchUserDashboard()
    case fetchUnIdentifiedSakes(parameters: Parameters)
    case fetchUserDetails(userID: String)
    case searchUserWithHandle(handle: String)
    case fetchBreweryDetails(breweryID: String)
    case fetchBarDetails(barID: String)
    case fetchUserFollowingList(userID: String)
    case fetchUserFollowersList(userID: String)
    case userFavouriteProducts()
    case userFavouriteBars()
    case userFavouriteBreweries()
    case userNotifications(lastID: String, size: Int, clearCount: Bool)
    case fetchUserBars(userID: String)
    case fetchProductsForBar(barID: String, lastID: String, size: Int)
    
    //Search
    case suggestProducts(parameters: Parameters)
    case suggestBars(parameters: Parameters)
    case suggestBreweries(parameters: Parameters)

    //Like/Fav/Review
    case markBreweryAsFavour(breweryID: String)
    case markBreweryAsUnFavour(breweryID: String)
    case postBreweryReview(breweryID: String, parameters: Parameters)
    
    case markBarAsFavour(barID: String)
    case markBarAsUnFavour(barID: String)
    case postBarReview(barID: String, parameters: Parameters)
    
    case getReviewsForEntity(entityID: String, parameters: Parameters)
    case flagReview(reviewID: String)
    
    var method: HTTPMethod {
        switch self {
            
        //User Related
        case .login:
            return .post
        case .register:
            return .put
        case .resetPassword:
            return .post
        case .followUser:
            return .post
        case .unfollowUser:
            return .post

           // FACADE API
        case .fetchLoginProfile:
            return .get
        case .fetchUserDashboard:
            return .get
        case .fetchUnIdentifiedSakes:
            return .get
        case .fetchUserDetails:
            return .get
        case .searchUserWithHandle:
            return .get
        case .fetchBreweryDetails:
            return .get
        case .fetchBarDetails:
            return .get
        case .fetchUserFollowingList:
            return .get
        case .fetchUserFollowersList:
            return .get
        case .userFavouriteProducts:
            return .get
        case .userFavouriteBars:
            return .get
        case .userFavouriteBreweries:
            return .get
        case .userNotifications:
            return .get
        case .fetchUserBars:
            return .get
        case .fetchProductsForBar:
            return .get

            
            // Search
        case .suggestProducts:
            return .get
        case .suggestBars:
            return .get
        case .suggestBreweries:
            return .get
            
            
        //Like/Fav/Review
        case .markBreweryAsFavour:
            return .post
        case .markBreweryAsUnFavour:
            return .post
        case .postBreweryReview:
            return .post
            
        case .markBarAsFavour:
            return .post
        case .markBarAsUnFavour:
            return .post
        case .postBarReview:
            return .post
        case .getReviewsForEntity:
            return .get
        case .flagReview:
            return .post

        }
        
    }
    
    var path: String {
        switch self {
            
            //User Related
        case .login:
            return APIEndPointConstant.LOGIN
        case .register:
            return APIEndPointConstant.REGISTER
        case .resetPassword:
            return APIEndPointConstant.RESET_PASSWORD
        case .followUser(let userID):
            return "/user/follow/" + userID
        case .unfollowUser(let userID):
            return "/user/unfollow/" + userID


            //Facade
        case .fetchLoginProfile:
            return APIEndPointConstant.LOGIN_PROFILE
        case .fetchUserDashboard:
            return APIEndPointConstant.DASHBOARD
        case .fetchUnIdentifiedSakes:
            return "/facade/products/unidentified"
        case .fetchUserDetails(let userID):
            return "/facade/user/" + userID
        case .searchUserWithHandle(let handle):
            return "/facade/user/handle/" + handle
        case .fetchBreweryDetails(let breweryID):
            return "/facade/brewery/" + breweryID
        case .fetchBarDetails(let barID):
            return "/facade/bar/" + barID
        case .fetchUserFollowingList(let userID):
            return "/facade/user/\(userID)/follow"
        case .fetchUserFollowersList(let userID):
            return "/facade/user/\(userID)/followedBy"
        case .userFavouriteProducts:
            return "/facade/user/fav/products"
        case .userFavouriteBars:
            return "/facade/user/fav/bars"
        case .userFavouriteBreweries:
            return "/facade/user/fav/breweries"
        case .userNotifications:
            return "/facade/user/notifications"
        case .fetchUserBars(let userID):
            return "/facade/user/\(userID)/bars"
        case .fetchProductsForBar(let barID, _, _):
            return "/facade/bar/\(barID)/products"



            
            //Search
        case .suggestProducts:
            return "/search/suggest/products"
        case .suggestBars:
            return "/search/suggest/bars"
        case .suggestBreweries:
            return "/search/suggest/breweries"
            
            
            //Like/Fav/Review
        case .markBreweryAsFavour(let breweryID):
            return "/favour/brewery/" + breweryID
        case .markBreweryAsUnFavour(let breweryID):
            return "/unfavour/brewery/" + breweryID
        case .postBreweryReview(let breweryID, _):
            return "/review/brewery/" + breweryID
            
        case .markBarAsFavour(let barID):
            return "/favour/bar/" + barID
        case .markBarAsUnFavour(let barID):
            return "/unfavour/bar/" + barID
        case .postBarReview(let barID, _):
            return "/review/bar/" + barID

        case .getReviewsForEntity(let entityID, _):
            return "/reviews/" + entityID

        case .flagReview(let reviewID):
            return "/flag/review/\(reviewID)"

            
        }
    }
    
    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try APIEndpoint.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
            
            //User Related
        case .login(let parameters):
            let username = parameters[ServiceConstant.HNDL] as! String
            let password = parameters[ServiceConstant.PWD] as! String
            let str = username + ":" + password
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
            urlRequest.setValue(Utils.getBase64Key(str: str), forHTTPHeaderField: ServiceConstant.AUTHORIZATION)
        case .register(let parameters, let token):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            urlRequest.setValue(token, forHTTPHeaderField: ServiceConstant.X_AUTH_TOKEN)
        case .resetPassword(let parameters, let token):
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
            urlRequest.setValue(token, forHTTPHeaderField: ServiceConstant.X_AUTH_TOKEN)
            urlRequest.setValue(ServiceConstant.ACCEPT_VALUE, forHTTPHeaderField: ServiceConstant.ACCEPT_KEY)
            
        case .followUser:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .unfollowUser:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)

            //Facade
        case .fetchUserDashboard():
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .fetchLoginProfile():
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .fetchUnIdentifiedSakes(let parameters):
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        case .fetchUserDetails:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .searchUserWithHandle:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .fetchBreweryDetails:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .fetchBarDetails:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .fetchUserFollowingList:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .fetchUserFollowersList:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .userFavouriteProducts:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .userFavouriteBars:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .userFavouriteBreweries:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .userNotifications(let lastID, let size, let clearCount):
            let parameters = ["lastId" : lastID, "size" : size, "clearCount" : clearCount] as [String : Any]
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        case .fetchUserBars:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .fetchProductsForBar(_, let lastID, let size):
            let parameters = ["lastId" : lastID, "size" : size] as [String : Any]
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)

            //Search
        case .suggestProducts(let parameters):
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        case .suggestBars(let parameters):
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        case .suggestBreweries(let parameters):
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
            
        //Like/Fav/Review
        case .markBreweryAsFavour:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .markBreweryAsUnFavour:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .postBreweryReview(_, let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            
        case .markBarAsFavour:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .markBarAsUnFavour:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .postBarReview(_, let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            
        case .getReviewsForEntity(_, let parameters):
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        case .flagReview:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)

        }
        
        urlRequest.setValue(ServiceConstant.SECURITY_HEADRE_VALUE, forHTTPHeaderField: ServiceConstant.X_SECURITY_HEADRE)

        return urlRequest
    }
    
    
}



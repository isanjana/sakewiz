//
//  ProductCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/8/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    @IBOutlet weak var productView: ProductView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Configure Cell
    func configureCell(productViewModel:ProductViewModel, hideTitleView:Bool) {
        titleHeight.constant = hideTitleView ? 0 : 38
        productView.updateView(productViewModel: productViewModel)
    }

}

//
//  StreamUserResult.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

class StreamUserResult: Mappable {
    
    var lastId: String?
    var results: [WizUser] = []
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        lastId <- map["lastId"]
        results <- map["results"]
    }
}

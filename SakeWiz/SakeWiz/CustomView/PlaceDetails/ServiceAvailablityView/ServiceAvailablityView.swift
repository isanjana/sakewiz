//
//  ServiceAvailablityView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/19/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

@IBDesignable class ServiceAvailablityView: NibView {
    
    //Properties
    @IBOutlet weak var publicImageView: UIImageView!
    @IBOutlet weak var publicLabel: UILabel!
    @IBOutlet weak var onSiteLable: UILabel!
    @IBOutlet weak var onSiteImageView: UIImageView!
    @IBOutlet weak var reservationImageView: UIImageView!
    @IBOutlet weak var reservationLabel: UILabel!
    @IBOutlet weak var salesCenterImageView: UIImageView!
    @IBOutlet weak var salesCenterLabel: UILabel!
    @IBOutlet weak var tourImageView: UIImageView!
    @IBOutlet weak var tourLabel: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func updateView(viewModel:PlaceDetailsViewModel) {
        let check =  UIImage(named: "Check_Green")
        let cross = UIImage(named: "Cross_Red")
        let yes = "yes".localized()
        let no  = "no".localized()
        publicImageView.image = viewModel.isOpenToPublic ? check : cross
        publicLabel.text = viewModel.isOpenToPublic ? yes : no
        onSiteImageView.image = viewModel.isTasting ? check : cross
        onSiteLable.text = viewModel.isTasting ? yes : no
        reservationImageView.image = viewModel.isReservationRequired ? check : cross
        reservationLabel.text = viewModel.isReservationRequired ? yes : no
        salesCenterImageView.image = viewModel.canBuy ? check : cross
        salesCenterLabel.text = viewModel.canBuy ? yes : no
        tourImageView.image = viewModel.isToursAvail ? check : cross
        tourLabel.text = viewModel.isToursAvail ? yes : no
    }
}

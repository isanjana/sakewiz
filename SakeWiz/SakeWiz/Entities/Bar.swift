//
//  Bar.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

class Bar: Mappable {
    
    var id: String?
    var userId: String?
    var name: [String:Any] = [:]
    var shortDesc: [String:Any] = [:]
    var longDesc: [String:Any] = [:]
    var lat: Double?
    var lon: Double?
    var openingHours: String?
    var supportedLangs:[String] = []
    var imgs:[String] = []
    var mainImg: Int?
    var email: String?
    var tel: String?
    var address: String?
    var web: String?
    var postcode: String?
    var venueType: String?
    
    
    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        
        id <- map["id"]
        userId <- map["userId"]
        name <- map["name"]
        shortDesc <- map["shortDesc"]
        longDesc <- map["longDesc"]
        lat <- map["lat"]
        lon <- map["lon"]
        openingHours <- map["openingHours"]
        supportedLangs <- map["supportedLangs"]
        imgs <- map["imgs"]
        mainImg <- map["mainImg"]
        email <- map["email"]
        tel <- map["tel"]
        address <- map["address"]
        web <- map["web"]
        postcode <- map["postcode"]
        venueType <- map["venueType"]
        
    }
}

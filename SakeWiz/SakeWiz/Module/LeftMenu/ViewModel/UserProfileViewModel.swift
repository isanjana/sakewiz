//
//  UserProfileViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/25/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation


class UserProfileViewModel {
    
    //MARK:- Properties
    var wizUser: WizUser? = nil
    var fromMenu:Bool = false
    fileprivate var userID:String = ""
    fileprivate var userViewModel:UserViewModel? = nil
    
    //MARK:- Initialization
    
    init(withUserId id: String?) {
        if let id = id {
            userID = id
        }
    }
    
}


extension UserProfileViewModel {
        
    func fetchUserProfile(completion: @escaping (_ error: APIError?) -> Void) {
    
        UserService.sharedInstance.fetchUserDetails(userID: userID) { (wizUser, error) in
            
            if let error = error {
                completion(error)
                return;
            }
            self.wizUser = wizUser
            self.userViewModel = UserViewModel(withUser: wizUser)
            completion(nil)
        }
    }
    
    
    //MARK:- Helper Methods
    func getUserViewModel() -> UserViewModel? {
        return userViewModel
    }
    
    func followUnfollowDidClicked() {
        userViewModel?.followUnfollowDidClicked()
    }
    
    
}

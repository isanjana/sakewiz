//
//  SearchService.swift
//  SakeWiz
//
//  Created by sanjana on 14/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import Alamofire

class SearchService {
    
    static let sharedInstance = SearchService()

    func suggestProducts(query: String, completion: @escaping (_ suggestions: Array<String>?) -> Void) {
        let parameteres = [ServiceConstant.TEXT : query, ServiceConstant.COUNT : ValueConstant.SEARCH_COUNT] as [String : Any] 
        API.sharedInstance.sessionManager.request(APIEndpoint.suggestProducts(parameters: parameteres)).responseJSON { (dataResponse) in
            completion(ServiceUtils.stringArrayFromResponse(dataResponse: dataResponse))
        }
    }
    
    
    func suggestBars(query: String, completion: @escaping (_ suggestions: Array<String>?) -> Void) {
        let parameteres = [ServiceConstant.TEXT : query, ServiceConstant.COUNT : ValueConstant.SEARCH_COUNT] as [String : Any]
        API.sharedInstance.sessionManager.request(APIEndpoint.suggestBars(parameters: parameteres)).responseJSON { (dataResponse) in
            completion(ServiceUtils.stringArrayFromResponse(dataResponse: dataResponse))
        }
    }
    
    func suggestBreweries(query: String, completion: @escaping (_ suggestions: Array<String>?) -> Void) {
        let parameteres = [ServiceConstant.TEXT : query, ServiceConstant.COUNT : ValueConstant.SEARCH_COUNT] as [String : Any]
        API.sharedInstance.sessionManager.request(APIEndpoint.suggestBreweries(parameters: parameteres)).responseJSON { (dataResponse) in
            completion(ServiceUtils.stringArrayFromResponse(dataResponse: dataResponse))
        }
    }
    
    
}

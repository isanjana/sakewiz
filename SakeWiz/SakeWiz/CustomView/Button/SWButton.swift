//
//  SWButton.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/6/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

@IBDesignable class SWButton: UIButton {
    
    //MARK:- Properties
    @IBInspectable var fontSize: CGFloat = 15 {
        didSet {
            titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        }
    }
    
    @IBInspectable var bgColor: UIColor = .appThemColor() {
        didSet {
            self.backgroundColor = bgColor
        }
    }
    
    @IBInspectable var textColor: UIColor = .white {
        didSet {
            setTitleColor(textColor, for: .normal)
        }
    }

    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        self.backgroundColor = bgColor
        titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        setTitleColor(textColor, for: .normal)
        updateLayer(borderColor: .clear, cornerRadius: ValueConstant.DEFAULT_RADIUS, borderWidth: 0)
        addDropShadowEffect()
    }

}


@IBDesignable class LeftAlignedIconButton: UIButton {
    
    //MARK:- Properties
    @IBInspectable var cornerRadiusValue: CGFloat = ValueConstant.DEFAULT_RADIUS {
        didSet {
            updateLayer()
        }
    }
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        addDropShadowEffect()
        updateLayer()
    }
   
    override func layoutSubviews() {
        super.layoutSubviews()
        contentHorizontalAlignment = .left
        let availableSpace = UIEdgeInsetsInsetRect(bounds, contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.right - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: availableWidth / 2, bottom: 0, right: 0)
    }
    
    private func updateLayer() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = cornerRadiusValue
    }
}

@IBDesignable class CenteredButton: UIButton {
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        centerTitleLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        centerTitleLabel()
    }
    
    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let rect = super.titleRect(forContentRect: contentRect)
        let imageRect = super.imageRect(forContentRect: contentRect)
        
        return CGRect(x: 0, y: imageRect.maxY - 5,
                      width: contentRect.width, height: rect.height)
    }
    
    override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        let rect = super.imageRect(forContentRect: contentRect)
        let titleRect = self.titleRect(forContentRect: contentRect)
        
        return CGRect(x: contentRect.width/2.0 - rect.width/2.0,
                      y: (contentRect.height - titleRect.height)/2.0 - rect.height/2.0,
                      width: rect.width, height: rect.height)
    }
    
    private func centerTitleLabel() {
        self.titleLabel?.textAlignment = .center
    }
}

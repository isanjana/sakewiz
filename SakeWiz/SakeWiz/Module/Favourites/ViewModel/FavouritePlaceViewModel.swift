//
//  FavouritePlaceViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/27/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation


class FavouritePlaceViewModel {
    //MARK:- Properties
    var entityType:EntityType = .notSet
    fileprivate var placeList:[Place] = []
   
    
    //MARK:- Initialization
    convenience init(withEntityType type: EntityType) {
        self.init()
        entityType = type
    }
    
}


extension FavouritePlaceViewModel {
    
    func loadPlaceList(completion: @escaping (_ error: APIError?) -> Void) {
        
        switch entityType {
        case .brewery:
            UserService.sharedInstance.userFavouriteBreweries(completion: { (placeArray, error) in
                if let error = error {
                    completion(error)
                    return;
                }
                
                if let palceList = placeArray {
                    self.handlePlaceList(placeArray: palceList)
                }
                
                completion(nil)
            })
            break
        case .bar:
            UserService.sharedInstance.userFavouriteBars(completion: { (placeArray, error) in
                if let error = error {
                    completion(error)
                    return;
                }
                
                if let palceList = placeArray {
                    self.handlePlaceList(placeArray: palceList)
                }
                
                completion(nil)
            })
            break
        default:
            break
        }
    }
    
    private func handlePlaceList(placeArray:[Place]) {
        self.placeList.removeAll()
        self.placeList.append(contentsOf: placeArray)
    }
    
    
    //MARK:- Helper Methods
    func favPlaceForIndex(index:Int) -> PlaceViewModel? {
        
        if placeList.count > 0 {
            let placeViewModel = PlaceViewModel(withPlace: placeList[index])
            return placeViewModel
        }
        
        return nil
    }
    
    func getPlaceCount() -> Int {
        return placeList.count > 0 ? placeList.count : 0
    }
    
    func getPlaceIdForIndex(index:Int) -> String? {
        return placeList[index].id
    }
    
}

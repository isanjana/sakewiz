//
//  UserDashboardViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/13/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation

class UserDashboardViewModel {
    
    //MARK:- Properties
    var userDashboard :UserDashboard? = nil
    
}

extension UserDashboardViewModel {
    
    //MARK:- Fetch User Dashboard
   
    func fetchUserDashboard(completion: @escaping (_ error: APIError?) -> Void) {
        
        UserService.sharedInstance.login(username: Utils.getUserHandel()!, password: Utils.getUserPassword()!) { (user, error) in
            
            UserService.sharedInstance.fetchUserDashboard { (userDashboard, error) in
                if let error = error {
                    completion(error)
                    return;
                }
                self.userDashboard = userDashboard
                completion(nil)
            }
        }
    }
    
    //MARK:- Helper Methods
   
    func recommendedProductForIndex(index:Int) -> ProductViewModel? {
        
        if let userDashboard = self.userDashboard {
            let productViewModel = ProductViewModel(withProdut: userDashboard.recommendedProducts[index])
            return productViewModel
        }
        
        return nil
    }
    
    func getProductIdForIndex(index:Int) -> String? {
        return userDashboard?.recommendedProducts[index].id
    }
}

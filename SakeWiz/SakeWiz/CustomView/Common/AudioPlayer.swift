//
//  AudioPlayer.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/26/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class AudioPlayer: NSObject {
    
    // MARK: - Properties
    public static let shared: AudioPlayer = {
        return AudioPlayer()
    }()
    
   fileprivate var player: AVAudioPlayer? = nil
    
    // MARK: - Class Initialization
    required override public init () {
        super.init()
        initialize()
    }
    
    //MARK:- Sound SetUp
    func initialize() {
        
        guard let url = Bundle.main.url(forResource: "beep", withExtension: "mp3") else {
            if DEBUG.isDebuggingEnabled {print("Oops, something went wrong when playing sound")}
            return
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(contentsOf: url)
            
        } catch let error {
            if DEBUG.isDebuggingEnabled {print("Error_Play_Sound = \(error.localizedDescription)")}
            print(error.localizedDescription)
        }
    }
}

extension AudioPlayer {
    
    func playSound() {
        guard let player = player else { return }
        player.play()
    }
    
    func pauseSound() {
        guard let player = player else { return }
        player.pause()
    }
    
    func releasePlayer() {
        if player != nil {
            player?.pause()
            player = nil
        }
    }
    
}

//
//  SWButtonContainer.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/6/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

@IBDesignable class SWButtonContainer: UIView {
    
    //MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        updateLayer(borderColor: .clear, cornerRadius: ValueConstant.DEFAULT_RADIUS, borderWidth: 0)
        addDropShadowEffect()
    }
    
}

//
//  UserViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/25/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation

class UserViewModel {
    
    //MARK:- Properties
    fileprivate var wizUser: WizUser? = nil
    
    
    //MARK:- Initialization
    
    init(withUser wizUser: WizUser?) {
        self.wizUser = wizUser
    }
    
}


extension UserViewModel {
    
    var isCurrentUser:Bool {
        
        if let currentUserHandel = Utils.getUserHandel() {
            if(wizUser?.userHandle?.caseInsensitiveCompare(currentUserHandel) == .orderedSame){
                return true
            }
        }
        
        return false
    }
    
    var getPlace: String {
        if wizUser?.country?.characters.count == 0 {
            if let stateStr = wizUser?.state {
                return stateStr
            }
        }
        
        if let stateStr = wizUser?.state {
            return stateStr.appending(", ").appending(wizUser!.country!)
        }
        
        return ""
    }
    
    var getName: String? {
        if let userName = wizUser?.name {
            return userName
        }
        return wizUser?.userHandle
    }
    
    var getRank: String? {
        if let rank = wizUser?.rank {
            return rank
        }
        return nil
    }
    
    var getProfileImageUrl: String? {
        
        if let imageUrl = wizUser?.imgUrl {
            return SakeWizUrlUtils.getMainImageUrl(imgUrl: imageUrl)
        }
        
        return nil 
    }
    
    var getRankImageUrl: String? {
        
        if let rank = wizUser?.rank {
            return SakeWizUrlUtils.rankImageUrl(rank: rank)
        }
        
        return nil
    }
    
    var getWizPoints: String? {
        if let points = wizUser?.wizPoints {
            return String(format: "%d", points)
        }
        return nil
    }
    
    var getFollowersCount: String? {
        if let followers = wizUser?.followedBy {
            return String(format: "%d", followers)
        }
        return nil
    }
    
    var getFollowingCont: String? {
        if let follows = wizUser?.follows {
            return String(format: "%d", follows)
        }
        return nil
    }

    
    var getReviewCount: String? {
        if let count = wizUser?.reviewCount {
            return String(format: "%d", count)
        }
        return nil
    }
    
    var getFavProductCount: String? {
        if let count = wizUser?.favProductCount {
            return String(format: "%d", count)
        }
        return nil
    }
    
    var getFavPlaceCount: String? {
        if let count = wizUser?.favPlaceCount {
            return String(format: "%d", count)
        }
        return nil
    }

    var getMotto: String? {
        if let motto = wizUser?.motto {
            return motto
        }
        return nil
    }
    
    var isUserFollowing: Bool {
        
        if wizUser?.userIsFollowing == true {
            return true
        }
        return false
    }
    
    func followUnfollowDidClicked() {
        
        if let userIsFollowing = wizUser?.userIsFollowing {
            if userIsFollowing {
                //1. Need to call user unfollow api
                //2. on api succes toggle the userIsFollowing flag.
                //3. post a notification to refresh UI.
                UserService.sharedInstance.unfollowUser(userID: wizUser!.userId!, completion: { (responseDict, errror) in
                    
                    self.wizUser?.userIsFollowing = !userIsFollowing
                    EventBus.postToMainThread(TaskName.RefreshUI)
                })
               
                
            } else {
                UserService.sharedInstance.followUser(userID: wizUser!.userId!, completion: { (responseDict, error) in
                    self.wizUser?.userIsFollowing = !userIsFollowing
                    EventBus.postToMainThread(TaskName.RefreshUI)
                })
                
            }
        }
    }
}

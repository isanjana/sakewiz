//
//  ScrollPager.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/21/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit

@objc public protocol ScrollPagerDelegate: NSObjectProtocol {
    @objc optional func scrollPager(scrollPager: ScrollPager, changedIndex: Int)
}

@IBDesignable public class ScrollPager: UIView, UIScrollViewDelegate{
    
    private var selectedIndex = 0
    private let indicatorView = UIView()
    private var buttons = [UIButton]()
    private var seperatorLines = [UIView]()
    private var views = [UIView]()
    private var animationInProgress = false
    @IBOutlet public weak var delegate: ScrollPagerDelegate!
    
    @IBOutlet public var scrollView: UIScrollView? {
        didSet {
            scrollView?.delegate = self
            scrollView?.isPagingEnabled = true
            scrollView?.showsHorizontalScrollIndicator = false
        }
    }
    
    @IBInspectable public var textColor: UIColor = UIColor.lightGray {
        didSet { redrawComponents() }
    }
    
    @IBInspectable public var selectedTextColor: UIColor = UIColor.darkGray {
        didSet { redrawComponents() }
    }
    
    @IBInspectable public var font: UIFont = UIFont.systemFont(ofSize: 13) {
        didSet { redrawComponents() }
    }
    
    @IBInspectable public var selectedFont: UIFont = UIFont.boldSystemFont(ofSize: 13) {
        didSet { redrawComponents() }
    }
    
    @IBInspectable public var indicatorColor: UIColor = UIColor.black {
        didSet { indicatorView.backgroundColor = indicatorColor }
    }
    
    @IBInspectable public var indicatorIsAtBottom: Bool = true {
        didSet { redrawComponents() }
    }
    
    @IBInspectable public var indicatorSizeMatchesTitle: Bool = false {
        didSet { redrawComponents() }
    }
    
    @IBInspectable public var indicatorHeight: CGFloat = 2.0 {
        didSet { redrawComponents() }
    }
    
    @IBInspectable public var borderColor: UIColor? {
        didSet { self.layer.borderColor = borderColor?.cgColor }
    }
    
    @IBInspectable public var borderWidth: CGFloat = 0 {
        didSet { self.layer.borderWidth = borderWidth }
    }
    
    @IBInspectable public var animationDuration: CGFloat = 0.2
    
    var getSelectedIndex:Int {
        return selectedIndex
    }
    // MARK: - Initializarion -
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    private func initialize() {
        #if TARGET_INTERFACE_BUILDER
            addSegmentsWithTitles(["One", "Two", "Three", "Four"])
        #endif
        
        font = UIFont.systemFont(ofSize: 12)
        selectedFont = UIFont.systemFont(ofSize: 12)
    }
    
    // MARK: - UIView Methods -
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        redrawComponents()
        //moveToIndex(selectedIndex, animated: false, moveScrollView: true)
    }
    
    // MARK: - Public Methods -
    
    public func addSegmentsWithTitlesAndViews(segments: [(title: String, view: UIView)]) {
        
        addButtons(titleOrImages: segments.map { $0.title as AnyObject })
        addViews(segmentViews: segments.map { $0.view })
        
        redrawComponents()
    }
    
    public func addSegment(segments: [(title: String, image: String, selectedImage: String, view: UIView)]) {
        
        addButtonsWithTitleAndImage(titles: segments.map {$0.title}, images: segments.map {$0.image}, selectedImages: segments.map {$0.selectedImage})
        addViews(segmentViews: segments.map { $0.view })
        
        redrawComponents()
    }
    
    public func addSegmentsWithImagesAndViews(segments: [(image: UIImage, view: UIView)]) {
        
        addButtons(titleOrImages: segments.map { $0.image })
        addViews(segmentViews: segments.map { $0.view })
        
        redrawComponents()
    }
    
    public func addSegmentsWithTitles(segmentTitles: [String]) {
        addButtons(titleOrImages: segmentTitles as [AnyObject])
        redrawComponents()
    }
    
    public func addSegmentsWithImages(segmentImages: [UIImage]) {
        addButtons(titleOrImages: segmentImages)
        redrawComponents()
    }
    
    public func setSelectedIndex(index: Int, animated: Bool) {
        setSelectedIndex(index: index, animated: animated, moveScrollView: true)
        
    }
    
    // MARK: - Private -
    
    private func setSelectedIndex(index: Int, animated: Bool, moveScrollView: Bool) {
        selectedIndex = index
        
        moveToIndex(index: index, animated: animated, moveScrollView: moveScrollView)
    }
    
    private func addViews(segmentViews: [UIView]) {
        guard let scrollView = scrollView else { fatalError("trying to add views but the scrollView is nil") }
        
        for view in scrollView.subviews {
            view.removeFromSuperview()
        }
        
        for i in 0..<segmentViews.count {
            let view = segmentViews[i]
            scrollView.addSubview(view)
            views.append(view)
        }
    }
    
    private func addButtonsWithTitleAndImage(titles:[String], images:[String], selectedImages:[String]) {
        
        for button in buttons {
            button.removeFromSuperview()
        }
        
        for seperator in seperatorLines {
            seperator.removeFromSuperview()
        }
        
        buttons.removeAll()
        seperatorLines.removeAll()
        
        for i in 0..<titles.count {
            let button = CenteredButton(type: .custom)
            button.tag = i
            button.addTarget(self, action: #selector(ScrollPager.buttonSelected(sender:)), for: .touchUpInside)
            buttons.append(button)
            
            button.setTitle(titles[i], for: .normal)
            let image = UIImage(named: images[i])
            button.setImage(image, for: .normal)
            let selectedImage = UIImage(named: selectedImages[i])
            button.setImage(selectedImage, for: .selected)
            
            addSubview(button)
            addSubview(indicatorView)
        }
        
        addSeperators()
    }
    
    func addSeperators() {
        
        for _ in 0..<buttons.count-1 {
            let seperatorLine = UIView()
            seperatorLine.backgroundColor = borderColor
            seperatorLines.append(seperatorLine)
            addSubview(seperatorLine)
            
        }
    }
    
    private func addButtons(titleOrImages: [AnyObject]) {
        for button in buttons {
            button.removeFromSuperview()
        }
        for seperator in seperatorLines {
            seperator.removeFromSuperview()
        }
        
        buttons.removeAll()
        seperatorLines.removeAll()
        
        for i in 0..<titleOrImages.count {
            let button = UIButton(type: .custom)
            button.tag = i
            button.addTarget(self, action: #selector(ScrollPager.buttonSelected(sender:)), for: .touchUpInside)
            buttons.append(button)
            
            if let title = titleOrImages[i] as? String {
                button.setTitle(title, for: .normal)
            }
            else if let image = titleOrImages[i] as? UIImage {
                button.setImage(image, for: .normal)
            }
            
            addSubview(button)
            addSubview(indicatorView)
        }
        
        addSeperators()
    }
    
    func setTitleForTabIndex(title:String, index:Int) {
        if index < buttons.count {
            let button = buttons[index]
            button.setTitle(title, for: .normal)
        }
    }
    
    private func moveToIndex(index: Int, animated: Bool, moveScrollView: Bool) {
        animationInProgress = true
        
        UIView.animate(withDuration: animated ? TimeInterval(animationDuration) : 0.0, delay: 0.0, options: .curveEaseOut, animations: { [weak self] in
            
            guard let strongSelf = self else { return }
            let width = strongSelf.frame.size.width / CGFloat(strongSelf.buttons.count)
            let button = strongSelf.buttons[index]
            
            strongSelf.redrawButtons()
            
            let indicatorY: CGFloat = strongSelf.indicatorIsAtBottom ? strongSelf.frame.size.height - strongSelf.indicatorHeight : 0
            
            if strongSelf.indicatorSizeMatchesTitle {
                guard let string = button.titleLabel?.text else { fatalError("missing title on button, title is required for width calculation") }
                guard let font = button.titleLabel?.font else { fatalError("missing dont on button, title is required for width calculation")  }
                let size = string.size(attributes: [NSFontAttributeName: font])
                let x = width * CGFloat(index) + ((width - size.width) / CGFloat(2))
                strongSelf.indicatorView.frame = CGRect(x: x, y: indicatorY, width: size.width, height: strongSelf.indicatorHeight)
            }
            else {
                strongSelf.indicatorView.frame = CGRect(x: width * CGFloat(index), y: indicatorY, width: button.frame.size.width, height: strongSelf.indicatorHeight)
            }
            
            if let scrollView = strongSelf.scrollView {
                if moveScrollView {
                    scrollView.contentOffset = CGPoint(x: CGFloat(index) * scrollView.frame.size.width, y: 0)
                }
            }
            
            }, completion: { [weak self] finished in
                // Storyboard crashes on here for some odd reasons, do a nil check
                self?.animationInProgress = false
        })
    }
    
    private func redrawComponents() {
        redrawButtons()
        
        if buttons.count > 0 {
            moveToIndex(index: selectedIndex, animated: false, moveScrollView: false)
        }
        
        if let scrollView = scrollView {
            scrollView.contentSize = CGSize(width: scrollView.frame.size.width * CGFloat(buttons.count), height: scrollView.frame.size.height)
            
            for i in 0..<views.count {
                views[i].frame = CGRect(x: scrollView.frame.size.width * CGFloat(i), y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
            }
        }
    }
    
    private func redrawButtons() {
        if buttons.count == 0 {
            return
        }
        
        let width = frame.size.width / CGFloat(buttons.count)
        let height = frame.size.height - indicatorHeight
        let y: CGFloat = indicatorIsAtBottom ? 0 : indicatorHeight
        var seperatorX: CGFloat = 0
        let seperatorHeight: CGFloat = frame.size.height - indicatorHeight * 6
        let seperatorY: CGFloat = (height - seperatorHeight) / 2
        
        
        for i in 0..<buttons.count {
            let button = buttons[i]
            button.frame = CGRect(x: width * CGFloat(i), y: y, width: width, height: height)
            button.setTitleColor((i == selectedIndex) ? selectedTextColor : textColor, for: .normal)
            button.isSelected = (i == selectedIndex)
            button.titleLabel?.font = (i == selectedIndex) ? selectedFont : font
            
            if i < buttons.count - 1 {
                let button = buttons[i]
                seperatorX = button.frame.size.width * CGFloat(i+1)
                let seperatorLine = seperatorLines[i]
                seperatorLine.frame = CGRect(x: seperatorX, y: seperatorY, width: borderWidth, height: seperatorHeight)
            }
        }
        
    }
    
    internal func buttonSelected(sender: UIButton) {
        if sender.tag == selectedIndex {
            return
        }
        
        delegate?.scrollPager?(scrollPager: self, changedIndex: sender.tag)
        
        setSelectedIndex(index: sender.tag, animated: true, moveScrollView: true)
    }
    
    // MARK: - UIScrollView Delegate -
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !animationInProgress {
            var page = scrollView.contentOffset.x / scrollView.frame.size.width
            
            if page.truncatingRemainder(dividingBy: 1) > 0.5 {
                page = page + CGFloat(1)
            }
            
            if Int(page) != selectedIndex {
                setSelectedIndex(index: Int(page), animated: true, moveScrollView: false)
                delegate?.scrollPager?(scrollPager: self, changedIndex: Int(page))
            }
        }
    }
    
}



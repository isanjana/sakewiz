//
//  Review.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class Review: Mappable {
    
    var id: String?
    var currentUserFlagged: Bool?
    var currentUserLiked: Bool?
    var likes: Int?
    var rate: Float?
    var userAddress: String?
    var userHandle: String?
    var userId: String?
    var created: Int64?
    var review: [String:Any] = [:]
    var userAffltCode: Int?
    var userRank: String?
    
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        currentUserFlagged <- map["currentUserFlagged"]
        currentUserLiked <- map["currentUserLiked"]
        likes <- map["likes"]
        rate <- map["rate"]
        userAddress <- map["userAddress"]
        userHandle <- map["userHandle"]
        userId <- map["userId"]
        created <- map["created"]
        userAffltCode <- map["userAffltCode"]
        userRank <- map["userRank"]
        
    }
}

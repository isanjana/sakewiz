//
//  PlaceView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/21/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

import SwiftyStarRatingView

@IBDesignable class PlaceView: NibView {
    
    //MARK:- Properties
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var addressLable: UILabel!
    @IBOutlet weak var placeTypeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var noteImageView: UIImageView!
    @IBOutlet weak var coinImageView: UIImageView!
       
    @IBInspectable var isDeleteRequired: Bool = false {
        didSet {
            hideControls()
        }
    }
    

    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        placeImageView.updateLayer(borderColor: .clear, cornerRadius: ValueConstant.DEFAULT_RADIUS, borderWidth: 0)
        hideControls()
        
    }
    
    
    func updateView(placeViewModel:PlaceViewModel) {
        placeNameLabel.text = placeViewModel.getName
        addressLable.text = placeViewModel.getAddress
        if let imageUrl = placeViewModel.getImageURL {
            placeImageView.imageFromUrl(imageUrl)
        }
        ratingView.value = CGFloat(placeViewModel.getRating)
        distanceLabel.text = ""
        placeTypeLabel.text = ""
        
        noteImageView.isHidden = !placeViewModel.getHasNote
        coinImageView.isHidden = !placeViewModel.getCanGainPoints
    }
    
    
    @IBAction func deleteButtonDidClicked(_ sender: Any) {
        
    }
    
    private func hideControls() {
        deleteButton.isHidden = !isDeleteRequired
    }

}

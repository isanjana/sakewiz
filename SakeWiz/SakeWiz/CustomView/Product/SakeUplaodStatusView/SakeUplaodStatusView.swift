//
//  SakeUplaodStatusView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/9/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit


@IBDesignable class SakeUplaodStatusView: NibView {
    
    //MARK:- Properties
    
    @IBOutlet weak var sakeIdentfiedCountLabel: UILabel!
    @IBOutlet weak var unIdentifiedCountLabel: UILabel!
    @IBOutlet weak var unIdentifiedView: UIView!
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updatePagerViewLayer()
        unIdentifiedView.addTapGesture(target: self, action: #selector(unIdentifiedSakeClicked))
    }
    
    func unIdentifiedSakeClicked() {
        EventBus.postToMainThread(TaskName.ShowUnknownSakeController)
    }
    
    func updateView(userDashboardViewModel:UserDashboardViewModel) {
        
        if let userDashboard = userDashboardViewModel.userDashboard {
            if let sakeIdentified = userDashboard.sakeIdentified {
                sakeIdentfiedCountLabel.text = String(format: "%d", sakeIdentified)
            }
            
            if let sakeUnidentified = userDashboard.sakeUnidentified {
                unIdentifiedCountLabel.text = String(format: "%d", sakeUnidentified)
            }
        }
    }

}

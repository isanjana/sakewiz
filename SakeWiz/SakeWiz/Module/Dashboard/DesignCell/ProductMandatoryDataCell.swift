//
//  ProductMandatoryDataCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/14/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import ScalingCarousel

class ProductMandatoryDataCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var carousel: ScalingCarouselView!
    @IBOutlet weak var pageControl: UIPageControl!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pageControl.numberOfPages = 3
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension ProductMandatoryDataCell: UICollectionViewDataSource, UICollectionViewDelegate {
    //MARK:- CarouselDatasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.item {
        case 0:
            let mandatoryDataCell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductMandatoryDataCarouselCell.identifier, for: indexPath) as! ProductMandatoryDataCarouselCell
            return mandatoryDataCell
            
        case 1:
            let bonusDataCell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductBonusDataCarouselCell.identifier, for: indexPath) as! ProductBonusDataCarouselCell
            return bonusDataCell
        
        default:
            let servingCell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductServingSuggestionCarouselCell.identifier, for: indexPath) as! ProductServingSuggestionCarouselCell
            return servingCell
        }
        
    }
    
    //MARK:- CarouselDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        carousel.didScroll()
        let pageWidth: CGFloat = UIScreen.main.bounds.size.width
        let page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
        self.pageControl.currentPage = Int(page)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
    }
}

class ProductMandatoryDataCarouselCell: ScalingCarouselCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var mandatoryDataView: ProductMandatoryDataView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

class ProductBonusDataCarouselCell: ScalingCarouselCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var bonusDataView: ProductBonusDataView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

class ProductServingSuggestionCarouselCell: ScalingCarouselCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var servingDataView: ProductServingSuggestionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}



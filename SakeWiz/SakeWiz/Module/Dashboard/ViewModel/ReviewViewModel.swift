//
//  ReviewViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/28/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation

class ReviewViewModel {
    
    //MARK:- Properties
    fileprivate var review: Review? = nil
    
    
    //MARK:- Initialization
    
    init(withReview review: Review?) {
        self.review = review
    }
    
}

extension ReviewViewModel {

    var getUserHandle: String? {
        guard let hndl = review?.userHandle else { return nil }
        return hndl
    }
    
    var getAddress: String? {
        guard let address = review?.userAddress else { return nil }
        return address
    }
    
    var getRate: Float? {
        guard let rate = review?.rate else { return nil }
        return rate
    }
    
    var getReview: String? {
        guard let review = review?.review else { return nil }
        let reviewStr = Utils.getLocaleField(dict: review)
        return reviewStr
    }
    
    var getLikeCount: String? {
        guard let likes = review?.likes else { return nil }
        return String(format: "%d", likes)
    }
    
    var getCreatedTime :String? {
        guard let created = review?.created else { return nil }
        let date = NSDate(timeIntervalSince1970: TimeInterval(created/1000))
        let timeAgo = Utils.timeAgoSinceDate(date: date, numericDates: true)
        return timeAgo
    }
    
    var isCurrentUserLiked: Bool {
        return review?.currentUserLiked == true ? true : false
    }
    
    var isCurrentUserFlagged: Bool {
        return review?.currentUserFlagged == true || !isAbleToLike ? true : false
    }
    
    var getUserAffltCode: Int? {
        guard let code = review?.userAffltCode else { return nil }
        return code
    }

    
    var isAbleToLike: Bool {
        return review?.userId != Utils.getUserID()! ? true : false
    }
    
    var getRankImageUrl: String? {
        
        if let rank = review?.userRank {
            return SakeWizUrlUtils.rankImageUrl(rank: rank)
        }
        
        return nil
    }
    
    var getUserAffltImageUrl: String? {
        
        if let code = review?.userAffltCode {
            return SakeWizUrlUtils.userAffltImageUrl(affltCode: code)
        }
        
        return nil
    }

}



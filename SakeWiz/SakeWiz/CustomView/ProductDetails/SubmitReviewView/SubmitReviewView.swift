//
//  SubmitReviewView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/15/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

protocol SubmitReviewDelegate {
    func reviewValueDidChange(value:CGFloat)
    func submitReviewDidClicked()
    func reviewCommentsDone()
}

@IBDesignable class SubmitReviewView: NibView {

    //MARK:- Properties
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var submitButton: SWButton!
    @IBOutlet weak var submitButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var commentBoxHeight: NSLayoutConstraint!
    var delegate: SubmitReviewDelegate?
    
   
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetUp()
        
    }
    
    func initialSetUp() {
        textView.updateLayer(borderColor: .clear, cornerRadius: ValueConstant.DEFAULT_RADIUS, borderWidth: 1)
        textView.applyPlaceholderStyle("comments".localized())
        textView.addToolBar(target: self, action: #selector(self.hideCommentBoxKeyboard))
        commentBoxSrink()
    }
    
    func updateView(viewModel:DetailViewModel) {
        ratingView.value = CGFloat(viewModel.getUserRate())
        textView.text = viewModel.getUserReview()
    }
    
    //MARK:- Grow/Shrink
    func comentBoxGrow() {
        UIView.animate(withDuration: 0.3) {
            self.textView.becomeFirstResponder()
            self.commentBoxHeight.constant = 163
            self.textViewHeight.constant = 100
            self.submitButtonHeight.constant = 44
            self.view.layoutIfNeeded()
            
        }
    }
    
    func commentBoxSrink() {
        commentBoxHeight.constant = 0
        textViewHeight.constant = 0
        submitButtonHeight.constant = 0
        resignKeyBoard()
    }
    
    func hideCommentBoxKeyboard() {
        resignKeyBoard()
        if let del = self.delegate {
            del.reviewCommentsDone()
        }
    }
    
    func resignKeyBoard() {
        textView.resignFirstResponder()
    }
    
    //MARK:- Rating Did Change
    @IBAction func ratingValueDidChange(_ sender: SwiftyStarRatingView) {
        let value = sender.value
        if let del = self.delegate {
            del.reviewValueDidChange(value: value)
        }
    }
    
    //MARK:- Submit Review
    @IBAction func submitReview(_ sender: Any) {
        resignKeyBoard()
        if let del = self.delegate {
            del.submitReviewDidClicked()
        }
    }
    
}


extension SubmitReviewView: UITextViewDelegate {
    
    // MARK:=======>TextView Delagate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "comments".localized() {
            textView.moveCursorToStart()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:NSString = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with:text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.characters.count == 0 {
            textView.applyPlaceholderStyle("comments".localized())
            textView.moveCursorToStart()
            return false
        }
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, clear
            // the text view and set its color to black to prepare for
            // the user's entry
        else
            if textView.textColor == .lightGray && text.characters.count > 0 {
                textView.text = nil
                textView.textColor = .darkGray
        }
        
        return true
    }

}

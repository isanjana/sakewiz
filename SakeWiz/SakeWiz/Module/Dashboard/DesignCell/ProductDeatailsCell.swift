//
//  ProductDeatailsCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/12/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class ProductDeatailsCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var productDetailsView: ProductDetailsView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

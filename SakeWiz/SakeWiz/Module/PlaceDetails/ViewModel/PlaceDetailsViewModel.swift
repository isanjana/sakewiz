//
//  PlaceDetailsViewModel.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/28/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation

class PlaceDetailsViewModel: DetailViewModel {
    
    //MARK:- Properties
    fileprivate var placeDetail: PlaceDetail? = nil
    var entityType:EntityType = .notSet
    fileprivate var placeID: String = ""
    var lastId: String = ""
    fileprivate var sections:[PlaceDetailsSection] = []
    fileprivate var reviewList:[Review] = []
    
   
    //MARK:- Initialization
    convenience init(withEntityType type: EntityType, palceID:String) {
        self.init()
        entityType = type
        self.placeID = palceID
    }
    
    override func getUserRate() -> Float {
        guard let rate = placeDetail?.userRate   else { return 0 }
        return rate
    }
    
    override func getUserReview() -> String? {
        guard let userReview = placeDetail?.userReview   else { return nil }
        let review = Utils.getLocaleField(dict: userReview)
        return review
    }
    
}

extension PlaceDetailsViewModel {
    
    func loadPlaceDetails(completion: @escaping (_ error: APIError?) -> Void) {
        
        switch entityType {
        case .brewery:
            BreweryService.sharedInstance.getBreweryDetails(breweryID: placeID, completion: { (breweryDetail, error) in
                
                if let error = error {
                    completion(error)
                    return;
                }
                
                self.placeDetail = breweryDetail
                self.initSection()
                completion(nil)
                
            })
            break
        case .bar:
            BarService.sharedInstance.getBarDetails(barID: placeID, completion: { (barDetail, error) in
                
                if let error = error {
                    completion(error)
                    return;
                }
                self.placeDetail = barDetail
                self.initSection()
                completion(nil)
            })
            break
        default:
            break
        }
    }
    
    
    func getPlaceReviews(loadMore:Bool, completion: @escaping (_ error: APIError?) -> Void) {
        
        if !loadMore {
            lastId = ""
        }
        else {
            if(lastId.isEmptyText()) {
                completion(nil)
                return
            }
        }
        
        ProductService.sharedInstance.getReviewsForProduct(productID: placeID, lastID: lastId) { (reviewResults, error) in
            
            if let error = error {
                completion(error)
                return;
            }
            
            if self.lastId.isEmptyText() {
                self.reviewList.removeAll()
            }
            
            if let reviews = reviewResults?.reviews {
                self.reviewList.append(contentsOf: reviews)
            }
            completion(nil)
        }
    }
    
    //MARK:- Helpers
    
    func getReviewForIndex(index:Int) -> ReviewViewModel? {
        
        if reviewList.count > 0 {
            let reviewViewModel = ReviewViewModel(withReview: reviewList[index])
            return reviewViewModel
        }
        
        return nil
    }
    
    private func initSection() {
        sections.removeAll()
        sections.append(.placeDetailHeader)
        sections.append(.recommend)
        if let _ = getPlaceImages {
            sections.append(.gallery)
        }
        sections.append(.rating)
        
        if case .brewery = self.entityType {
            sections.append(.availablity)
        }
    }
    
    func getSections() -> [PlaceDetailsSection] {
        return sections
    }
    
    func getRowCount() -> Int {
        return sections.count + reviewList.count
    }
    
    var getAvgRate: Float {
        guard let rate = placeDetail?.avgRate   else { return 0 }
        return rate
    }
    
    var hasUserNote: Bool {
        return placeDetail?.userHasNote == true ? true : false
    }
    
    var isFavourite: Bool {
        return placeDetail?.userFavourite == true ? true : false
    }
    
    var getPlaceName: String? {
        
        if case .brewery = self.entityType {
            guard let name = placeDetail?.brewery?.name   else { return nil }
            let breweryName = Utils.getLocaleField(dict: name)
            return breweryName
            
        } else {
            guard let name = placeDetail?.bar?.name   else { return nil }
            let barName = Utils.getLocaleField(dict: name)
            return barName
        }
    }
    
    var getPlaceImageUrl: String? {
        
        if case .brewery = self.entityType {
            if let mainImg = placeDetail?.brewery?.mainImg {
                if let count = placeDetail?.brewery?.imgs.count {
                    if count > 0 && mainImg < count {
                        return SakeWizUrlUtils.getMainImageUrl(imgUrl: placeDetail?.brewery?.imgs[mainImg])
                    }
                }
            }
            return nil
            
        } else {
            if let mainImg = placeDetail?.bar?.mainImg {
                if let count = placeDetail?.bar?.imgs.count {
                    if count > 0 && mainImg < count {
                        return SakeWizUrlUtils.getMainImageUrl(imgUrl: placeDetail?.bar?.imgs[mainImg])
                    }
                }
            }
            return nil
        }
    }
    
    var getLongDesc: String? {
        
        if case .brewery = self.entityType {
            guard let longDesc = placeDetail?.brewery?.longDesc   else { return nil }
            let desc = Utils.getLocaleField(dict: longDesc)
            return desc
            
        } else {
            guard let longDesc = placeDetail?.bar?.longDesc   else { return nil }
            let desc = Utils.getLocaleField(dict: longDesc)
            return desc
        }
    }
    
    var getAddress: String? {
        
        if case .brewery = self.entityType {
            guard let address = placeDetail?.brewery?.address  else { return nil }
            return address
            
        } else {
            guard let address = placeDetail?.bar?.address  else { return nil }
            return address
        }
    }
    
    var getTelephone: String? {
        
        if case .brewery = self.entityType {
            guard let telephone = placeDetail?.brewery?.tel  else { return nil }
            return telephone
            
        } else {
            guard let telephone = placeDetail?.bar?.tel  else { return nil }
            return telephone
        }
    }
    
    var getEmail: String? {
        
        if case .brewery = self.entityType {
            guard let email = placeDetail?.brewery?.email  else { return nil }
            return email
            
        } else {
            guard let email = placeDetail?.bar?.email  else { return nil }
            return email
        }
    }
    
    var getOpeningHours: String? {
        
        if case .brewery = self.entityType {
            guard let openingHours = placeDetail?.brewery?.openingHours  else { return nil }
            return openingHours
            
        } else {
            guard let openingHours = placeDetail?.bar?.openingHours  else { return nil }
            return openingHours
        }
    }
    
    var getSupportedLanguages: [String]? {
        
        if case .brewery = self.entityType {
            guard let _ = placeDetail?.brewery?.supportedLangs.count else { return nil }
            return placeDetail?.brewery?.supportedLangs
            
        } else {
            guard let _ = placeDetail?.bar?.supportedLangs.count else { return nil }
            return placeDetail?.brewery?.supportedLangs
        }
    }
    
    
    var getPlaceImages: [String]? {
        if case .brewery = self.entityType {
            guard let _ = placeDetail?.brewery?.imgs.count else { return nil }
            return placeDetail?.brewery?.imgs
            
        } else {
            guard let _ = placeDetail?.bar?.imgs.count else { return nil }
            return placeDetail?.brewery?.imgs
        }
    }
    
    
    var isOpenToPublic: Bool {
         return placeDetail?.brewery?.isOpenToPublic == true ? true : false
    }
    
    var isTasting: Bool {
        return placeDetail?.brewery?.isTasting == true ? true : false
    }
    
    var isToursAvail: Bool {
        return placeDetail?.brewery?.isToursAvail == true ? true : false
    }
    
    var isReservationRequired: Bool {
        return placeDetail?.brewery?.reservationRequired == true ? true : false
    }
    
    var canBuy: Bool {
        return placeDetail?.brewery?.canBuy == true ? true : false
    }
    
    
    
}



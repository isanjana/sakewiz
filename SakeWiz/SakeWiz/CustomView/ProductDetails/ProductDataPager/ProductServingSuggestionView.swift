//
//  ProductServingSuggestionView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/12/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

@IBDesignable class ProductServingSuggestionView: NibView {

    //MARK:- Properties
    
    @IBOutlet weak var roomTempImageV: UIImageView!
    @IBOutlet weak var iceImageView: UIImageView!
    @IBOutlet weak var chilledImageView: UIImageView!
    @IBOutlet weak var warmImageView: UIImageView!
    @IBOutlet weak var hotImageView: UIImageView!
    @IBOutlet weak var veryHotImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var wizPointsLabel: UILabel!
    
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updatePagerViewLayer()
        profileImageView.circular(borderColor: .clear, borderWidth: 0)
    }

}

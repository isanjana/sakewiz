//
//  NotificationConstants.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/21/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation


public struct TaskName {
    
    /**
     ShowUnknownSakeController
     
     Posted to open UnknownSakeViewContller
     
     :Hint:
     *  When 'Unindentified Sake' did clicked on 'Dasboard Screen' we post this notification.
     *  Posted from 'SakeUplaodStatusView' -> unIdentifiedSakeClicked methode.
     *  Observe On 'DashboardViewController' -> viewWiilAppear methode.
     
     */
    public static let ShowUnknownSakeController = "ShowUnknownSakeController"
    
    /**
     ShowFavouriteController
     
     Posted to open ShowFavouriteController
     
     :Hint:
     *  When 'Fav Sake or Fav Place' did clicked on 'Dasboard Screen' we post this notification.
     *  Posted from 'DashboardCell' -> openFavouriteVC methode.
     *  Observe On 'DashboardViewController' -> viewWiilAppear methode.
     
     */
    public static let ShowFavouriteController = "ShowFavouriteController"
    
    /**
     ShowProductDetailsController
     
     Posted to open ShowProductDetailsController
     
     :Hint:
     *  When 'fav Sake' row did clicked on 'My Favourites Screen' or reccomended prodcut row did clicked from 'Dashboard' we post this notification.
     *  Posted from 'FavouriteSakeView' -> didSelectRowAt methode.
     *  Posted from 'Dashboard Screen' -> didSelectRowAt methode.
     *  Observe On 'BaseViewController' -> viewWiilAppear methode.
     
     */
    public static let ShowProductDetailsController = "ShowProductDetailsController"
    
    /**
     ShowPlaceDetailsController
     
     Posted to open PlaceDetailsController
     
     :Hint:
     *  When 'Breveries' or 'Bar' row did clicked on 'My Favourites Screen' we post this notification.
     *  Posted from 'FavouritePlaceView' -> didSelectRowAt methode.
     *  Observe On 'BaseViewController' -> viewWiilAppear methode.
     
     */
    public static let ShowPlaceDetailsController = "PlaceDetailsController"
    
    /**
     ShowFollowViewController
     
     Posted to open ShowFollowViewController
     
     :Hint:
     *  When 'follow or following' view did clicked on 'Dashboars Screen' we post this notification.
     *  Posted from 'DashboardCell' -> openFollowVC methode.
     *  Observe On 'DashboardViewController' -> viewWiilAppear methode.
     
     */
    public static let ShowFollowViewController = "ShowFollowViewController"
    
    /**
     ShowUserProfileViewController
     
     Posted to open ShowUserProfileViewController
     
     :Hint:
     *  When 'follow or following' view did clicked on 'Follow VC' or Invite User VC we post this notification.
     *  Posted from 'FollowView' -> didSelectRowAt methode and .
     *  Posted from 'InviteUserVC' -> didSelectRowAt methode and .
     *  Observe On 'BaseViewController' -> viewWiilAppear methode.
     
     */
    public static let ShowUserProfileViewController = "ShowUserProfileViewController"
    
    /**
     RefreshUI
     
     Posted to RefreshUI
          
     */
    public static let RefreshUI = "RefreshUI"
    
    
    
    
}


//
//  StreamNotificationResult.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

class StreamNotificationResult: Mappable {
    
    var notifications: [Notification] = []
    var lastId: String?
    
    required init?(map: Map) {
        
        
    }
    
    func mapping(map: Map) {
        
        notifications <- map["notifications"]
        lastId <- map["lastId"]
    }
}

//
//  GeoPlace.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

class GeoPlace: Mappable {
    
    var id: String?
    var name: [String:Any] = [:]
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
    }
}


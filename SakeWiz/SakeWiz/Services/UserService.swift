//
//  UserService.swift
//  SakeWiz
//
//  Created by sanjana on 08/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper


class UserService {
    
    static let sharedInstance = UserService()

    func login(username: String, password: String, completion: @escaping (_ user: User?, _ error: APIError?) -> Void) {
        let parameteres = [ServiceConstant.HNDL : username, ServiceConstant.PWD : password]
        Alamofire.request(APIEndpoint.login(parameters: parameteres)).responseJSON { (dataresponse) in
            if (dataresponse.result.isFailure) {
                let dict = [ServiceConstant.ERROR_KEY : ServiceConstant.ERROR_VALUE, ServiceConstant.MESSAGE : dataresponse.error!.localizedDescription]
                let apiError = APIError(JSON: dict)
                completion(nil, apiError)
                return
            }
            
            if DEBUG.isDebuggingEnabled {
                print("Login Response = \(dataresponse)");
            }
            
            let rp = dataresponse.value as! [String : Any]
            
            if let apiError = APIError(JSON:rp) {
                completion(nil, apiError)
                return
            }
            
            let user = User(JSON: rp)

            if let headers = dataresponse.response?.allHeaderFields as? [String: String] {
                if let token = headers[ServiceConstant.X_AUTH_TOKEN] {
                    Utils.setObjectInUserDefaults(token, forKey: UserDefaultConstant.kAuthToken)
                }
                if let hndl = user?.hndl {
                    if hndl != ServiceConstant.SYSTEM_USER {
                        API.sharedInstance.startAuthSession()
                        Utils.setObjectInUserDefaults(user?.id ?? "", forKey:UserDefaultConstant.kUserID)
                        Utils.setObjectInUserDefaults(username, forKey:UserDefaultConstant.kHNDL)
                        Utils.setObjectInUserDefaults(password, forKey:UserDefaultConstant.kPassword)

                    }
                }
                completion(user, nil)

            } else {
                let rp = dataresponse.value as! [String : Any]
                if let apiError = APIError(JSON:rp) {
                    completion(nil, apiError)
                }

            }
        }
    }
    
    func register(username: String, password: String, email: String, completion: @escaping (_ user: User?, _ error: APIError?) -> Void) {
        self.login(username: ServiceConstant.SYSTEM_USER, password: ServiceConstant.PASSWORD) { (user, apiError) in
            let parameteres = [ServiceConstant.EMAIL : email, ServiceConstant.HNDL : username, ServiceConstant.PWD : password, ServiceConstant.LANG : Utils.getLanguageCode()]
            
            if let token = Utils.getAuthToken() {
                API.sharedInstance.request(APIEndpoint.register(parameters: parameteres, token: token), completion: { (_ user: User?, _ error: APIError?) in
                    completion(user,error)
                })
            } else {
                completion(nil, apiError)
            }

        }
    }
    
    func resetPassword(email: String, handle: String, completion: @escaping (_ result: String?, _ error: APIError?) -> Void) {
        self.login(username: ServiceConstant.SYSTEM_USER, password: ServiceConstant.PASSWORD) { (user, apiError) in
            let parameters = email.isEmpty ? [ServiceConstant.USER_HANDEL : handle] : [ServiceConstant.EMAIL : email]
            if let token = Utils.getAuthToken() {

                Alamofire.request(APIEndpoint.resetPassword(parameters: parameters, token: token)).response(completionHandler: { (responseData) in
                    if (responseData.response?.statusCode == 200) {
                        completion("forgot_pwd_email_sent".localized(), nil)
                    } else {
                        let dict = [ServiceConstant.ERROR_KEY : ServiceConstant.ERROR_VALUE, ServiceConstant.MESSAGE : "someting_wrong".localized()]
                        let apiError = APIError(JSON: dict)
                        completion(nil, apiError)
                    }
                })
            } else {
                completion(nil, apiError)
            }
            
        }
    }
    
    
    func fetchUserDashboard(completion: @escaping (_ userDashboard: UserDashboard?, _ error: APIError?) -> Void) {
        API.sharedInstance.request(APIEndpoint.fetchUserDashboard()) { (userDashboard: UserDashboard?, error: APIError?) in
            completion(userDashboard, error)
        }
    }
    
    func fetchLoginProfile(completion: @escaping (_ user: User?, _ error: APIError?) -> Void) {
        API.sharedInstance.request(APIEndpoint.fetchLoginProfile()) { (user: User?, error: APIError?) in
            completion(user, error)

        }
    }
    
    func fetchUserDetails(userID: String, completion: @escaping (_ user: WizUser?, _ error: APIError?) -> Void) {

        API.sharedInstance.request(APIEndpoint.fetchUserDetails(userID: userID)) { (user: WizUser?, error: APIError?) in
            completion(user, error)
        }
    }
    
    func searchUserWithHandle(userHandle: String, completion: @escaping (_ user: WizUser?, _ error: APIError?) -> Void) {        
        API.sharedInstance.request(APIEndpoint.searchUserWithHandle(handle: userHandle)) { (user: WizUser?, error: APIError?) in
            completion(user, error)
        }
    }
    
    func fetchUnIdentifiedSakes(lastId: String, size: Int, completion: @escaping (_ unIdentifiedStreamResult: UnIdentifiedStreamResult?, _ error: APIError?) -> Void) {
        let parameters = ["lastId" : lastId, "size" : size] as [String : Any]
        API.sharedInstance.request(APIEndpoint.fetchUnIdentifiedSakes(parameters: parameters)) { (unIdentifiedStreamResult: UnIdentifiedStreamResult?, error: APIError?) in
            completion(unIdentifiedStreamResult, error)
        }
    }
    
    //Follow/Unfollow API's
    func followUser(userID: String, completion: @escaping (_ responseDict: [String: Any]?, _ error: APIError?) -> Void) {
        API.sharedInstance.sessionManager.request(APIEndpoint.followUser(userID: userID)).responseJSON { (dataResponse) in
            ServiceUtils.processResponse(dataResponse: dataResponse, completion: completion)
        }
    }
    
    func unfollowUser(userID: String, completion: @escaping (_ responseDict: [String: Any]?, _ error: APIError?) -> Void) {
        API.sharedInstance.sessionManager.request(APIEndpoint.unfollowUser(userID: userID)).responseJSON { (dataResponse) in
            ServiceUtils.processResponse(dataResponse: dataResponse, completion: completion)
        }
    }
    

    func fetchUserFollowingList(userID: String, completion: @escaping (_ streamUserResult: StreamUserResult?, _ error: APIError?) -> Void) {
        API.sharedInstance.request(APIEndpoint.fetchUserFollowingList(userID: userID)) { (streamUserResult: StreamUserResult?, error: APIError?) in
            if let results = streamUserResult?.results {
                for wizUser in results {
                    wizUser.setFollowed(followed: true)
                }
            }

            completion(streamUserResult, error)
        }
    }
    
    
    func fetchUserFollowersList(userID: String, completion: @escaping (_ streamUserResult: StreamUserResult?, _ error: APIError?) -> Void) {
        API.sharedInstance.request(APIEndpoint.fetchUserFollowersList(userID: userID)) { (streamUserResult: StreamUserResult?, error: APIError?) in
            completion(streamUserResult, error)
        }
    }
    
    func userFavouriteProducts(completion: @escaping (_ products: [Product]?, _ error: APIError?) -> Void) {
        
        API.sharedInstance.requestForCollection("products", urlRequest: APIEndpoint.userFavouriteProducts()) { (products: [Product]?, apiError: APIError?) in
            completion(products, apiError)
        }
    }
    
    func userFavouriteBars(completion: @escaping (_ places: [Place]?, _ error: APIError?) -> Void) {
        
        API.sharedInstance.requestForCollection("places", urlRequest: APIEndpoint.userFavouriteBars()) { (places: [Place]?, apiError: APIError?) in
            completion(places, apiError)
        }
    }
    
    func userFavouriteBreweries(completion: @escaping (_ places: [Place]?, _ error: APIError?) -> Void) {
        
        API.sharedInstance.requestForCollection("places", urlRequest: APIEndpoint.userFavouriteBreweries()) { (places: [Place]?, apiError: APIError?) in
            completion(places, apiError)
        }
    }
    
    //TODO: SAN TEST
    func userNotifications(lastID: String, completion: @escaping (_ streamNotificationResult: StreamNotificationResult?, _ error: APIError?) -> Void) {
        API.sharedInstance.request(APIEndpoint.userNotifications(lastID: lastID, size: 10, clearCount: true)) { (streamNotificationResult: StreamNotificationResult?, error: APIError?) in
            completion(streamNotificationResult, error)
        }
    }
    
    //TODO: PR - Make BarResult Entity
//    func fetchUserBars(userID: String, completion: @escaping (_ barResult: BarResult?, _ error: APIError?) -> Void) {
//        API.sharedInstance.request(APIEndpoint.fetchUserBars(userID: userID)) { (barResult: BarResult?, error: APIError?) in
//            completion(barResult, error)
//        }
//    }
    
    //TODO: SAN TEST
    func fetchProductsForBar(barID: String, lastID: String,completion: @escaping (_ streamProductResult: StreamProductResult?, _ error: APIError?) -> Void ) {
        //TODO: PR constant Constants.BAR_PRODUCTS_COUNT instead 10
        API.sharedInstance.request(APIEndpoint.fetchProductsForBar(barID: barID, lastID: lastID, size: 10)) { (streamProductResult: StreamProductResult?, error: APIError?) in
            completion(streamProductResult, error)
        }
    }
    
    //TODO: SAN TEST
    func flagReview(review: Review, completion: @escaping (_ responseDict: [String: Any]?, _ error: APIError?) -> Void) {
         API.sharedInstance.sessionManager.request(APIEndpoint.flagReview(reviewID: review.id!)).responseJSON { (dataResponse) in
         ServiceUtils.processResponse(dataResponse: dataResponse, completion: completion)
         }
    }
    
    func updateUserInfo(user: User) {
        var parameters = [String : Any]()
        parameters["name"] = user.name!
        parameters["email"] = user.email!
//        parameters["gender"] = user.gender! //TODO: PR gender is missing from user object, pls check
        /* //TODO: PR these are also missing in user, pls add.
         @JsonProperty("birthMonth") int birthMonth,
         @JsonProperty("birthDate") int birthDate,
         @JsonProperty("birthYear") int birthYear

 */
        parameters["pwd"] = user.pwd!
        parameters["lang"] = user.lang!
        parameters["country"] = user.country!
        parameters["state"] = user.state!
        parameters["motto"] = user.motto!

        /* TODO: SAN
         if (user.getBirthYear() != 0)
         userInfo.put("birthYear", user.getBirthYear() + "");
         
         if (user.getBirthMonth() != 0)
         userInfo.put("birthMonth", user.getBirthMonth() + "");
         
         if (user.getBirthDate() != 0)
         userInfo.put("birthDate", user.getBirthDate() + "");
 */

    }
}

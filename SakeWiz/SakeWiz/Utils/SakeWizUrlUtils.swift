//
//  SakeWizUrlUtils.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/26/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class SakeWizUrlUtils: NSObject {

    class func rankImageUrl(rank:String?) -> String? {
        
        if let userRank = rank {
            let rankImageUrl = ImageUrlConstant.RANK_IMAGE_URL
            let requiredUrl = rankImageUrl.replacingOccurrences(of: "{rank}", with: userRank)
            return requiredUrl
        }
        return nil
    }
    
    class func getMainImageUrl(imgUrl:String?) -> String? {
    
        if let pic = imgUrl {
            let imageUrl = ImageUrlConstant.BASE_IMAGE_URL.appending(pic)
            return imageUrl
        }
        
        return nil
    
    }
    
    class func getScanImageUrl(imgUrl:String?) -> String? {
        
        if let pic = imgUrl {
            let imageUrl = ImageUrlConstant.SCAN_IMAGE_URL.appending(pic)
            return imageUrl
        }
        
        return nil
        
    }
    
    class func userAffltImageUrl(affltCode:Int?) -> String? {
        
        if let code = affltCode {
            let str = String(format: "%d", code)
            let url = ImageUrlConstant.USERAFFLTCODE_IMAGE_URL
            let requiredUrl = url.replacingOccurrences(of: "{userAffltCode}", with: str)
            return requiredUrl
        }
        return nil
    }
    
    
}

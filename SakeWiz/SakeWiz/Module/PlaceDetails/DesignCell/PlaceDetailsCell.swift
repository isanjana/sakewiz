//
//  PlaceDetailsCell.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/18/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class PlaceDetailsCell: UITableViewCell {
    
    //MARK:- Properties
    class var identifier: String { return String.className(self) }
    @IBOutlet weak var placeDetailsView: PlaceDetailsView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func configureCell(viewModel:PlaceDetailsViewModel) {
        placeDetailsView.updateView(viewModel: viewModel)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

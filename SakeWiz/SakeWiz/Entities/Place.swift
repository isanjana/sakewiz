//
//  Place.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class Place: Mappable {
    
    var id: String?
    var userId: String?
    var type: String?
    var barType: String?
    var lat: Double?
    var lon: Double?
    var address: String?
    var updated: Int64?
    var name: [String:Any] = [:]
    var imgUrl: String?
    var reviewCount: Int?
    var favoured: Int?
    var rate: Float?
    var hasUserNote: Bool?
    var canGainPoints: Bool?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        userId <- map["userId"]
        type <- map["type"]
        type <- map["type"]
        barType <- map["barType"]
        lat <- map["lat"]
        lon <- map["lon"]
        address <- map["address"]
        updated <- map["updated"]
        name <- map["name"]
        imgUrl <- map["imgUrl"]
        reviewCount <- map["reviewCount"]
        rate <- map["rate"]
        hasUserNote <- map["hasUserNote"]
        canGainPoints <- map["canGainPoints"]
        
    }
}

//
//  ForgotPasswordViewModel.swift
//  SakeWiz
//
//  Created by sanjana on 12/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation

class ForgotPasswordViewModel {
    
    //MARK:- Properties
    var username = ""
    var email = ""
}


extension ForgotPasswordViewModel {
    
    //MARK:- Update User Data
    func updateUsername(username: String) {
        self.username = username
    }
    
    func updateEmail(email: String) {
        self.email = email
    }
    
    //MARK:- Validate User Data
    func validate() -> UserValidationState {
        
        //1. first check for empty values.
        if email.isEmptyText() && username.isEmptyText() {
            return .Invalid("forgotPass_alert_msg".localized())
        }
        
        //2. we need only one value at the time eithe email or handel.
        if !email.isEmptyText() && !username.isEmptyText() {
            return .Invalid("forgotPass_alert_msg".localized())
        }
        
        //3.
        if !email.isEmptyText() {
           
            if !email.isValidEmail() {
                return .Invalid("email_invalid".localized())
            }
        }
        else if !username.isEmptyText() {
            if !username.isValidHandle() {
                return .Invalid("handel_invalid".localized())
            }
        }
        
        return .Valid
    }
    
    //MARK:- Forgot Password API
    func resetPassword(completion: @escaping (_ succesMsg:String?, _ error:APIError?) -> Void) {
        UserService.sharedInstance.resetPassword(email: email, handle: username.lowercased()) { (msg, error) in
            
            if let error = error {
                completion(nil, error)
                return;
            }
            
            if let succesMsg = msg {
                completion(succesMsg, nil)
            }
        }
    }
    
    
    
}

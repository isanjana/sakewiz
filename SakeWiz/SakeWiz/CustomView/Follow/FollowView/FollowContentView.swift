//
//  FollowContentView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/23/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

@IBDesignable class FollowContentView: NibView {

    //MARK:- Properties
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var mottoLabel: UILabel!
    @IBOutlet weak var chatCountLabel: UILabel!
    @IBOutlet weak var followCountLabel: UILabel!
    @IBOutlet weak var reviewCountLabel: UILabel!
    @IBOutlet weak var wizPointLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var followHeight: NSLayoutConstraint!
    @IBOutlet weak var wizImageView: UIImageView!
    @IBOutlet weak var placeImageV: UIImageView!
    private var userViewModel:UserViewModel? = nil
    
    
    
    //MARK:- Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        profileImageView.circular(borderColor: .appGrayColor(), borderWidth: 1)
    }
    
    func updateView(userViewModel: UserViewModel) {
        self.userViewModel = userViewModel
        
        followButton.isHidden = userViewModel.isCurrentUser
        followHeight.constant = userViewModel.isCurrentUser ? 0 : 32
        
        //Profile Image
        if let userProfileImageUrl = userViewModel.getProfileImageUrl {
            profileImageView.imageFromUrl(userProfileImageUrl, showLoader: true)
        }
        
        if let rankImageUrl = userViewModel.getRankImageUrl {
            wizImageView.imageFromUrl(rankImageUrl, showLoader: false)
        }
        
        //Name
        userNameLabel.text = userViewModel.getName
        
        //Place
        placeLabel.text = userViewModel.getPlace
        placeImageV.isHidden = userViewModel.getPlace.isEmptyText()
        
        
        //Followers
        if let wizPoints = userViewModel.getWizPoints {
            wizPointLabel.text = wizPoints
        }
        
        //Followers
        if let followersCount = userViewModel.getFollowersCount {
            followCountLabel.text = followersCount
        }
        
        //Review
        if let reviewCount = userViewModel.getReviewCount {
            reviewCountLabel.text = reviewCount
        }
        
        //Motto
        mottoLabel.text = userViewModel.getMotto
        
        // Follow/ Unfollow
        let followTitle = userViewModel.isUserFollowing ? "unfollow".localized() : "follow".localized()
        followButton.setTitle(followTitle, for: .normal)
        
    }
    
    
    @IBAction func followUnfollowAction(_ sender: Any) {
        userViewModel?.followUnfollowDidClicked()
    }

}

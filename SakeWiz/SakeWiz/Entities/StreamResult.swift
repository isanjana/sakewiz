//
//  StreamResult.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper

class StreamResult: Mappable {
    
    var page: Int?
    var size: Int?
    var totalPages: Int?
    var totalSize: Int?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
         page <- map["page"]
         size <- map["size"]
         totalPages <- map["totalPages"]
         totalSize <- map["totalSize"]
    }
}

//
//  FollowViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/23/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit

class FollowViewController: BaseViewController {
    
    //MARK:- Properties
    @IBOutlet var scrollPager: ScrollPager!
    @IBOutlet weak var inviteView: UIView!
    @IBOutlet weak var qrView: UIView!
    var fromMenu:Bool = false
    var isFollowers:Bool = false
    var followingView: FollowView? = nil
    var followersView: FollowView? = nil

    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        pagerSetUp()
    }
    
    //MARK:- Pager SetUp
    func pagerSetUp() {
        scrollPager.delegate = self
        followingView = FollowView(withListType: .following)
        followersView = FollowView(withListType: .followers)
        
        scrollPager.addSegmentsWithTitlesAndViews(segments: [
            (String(format: "following_count".localized(), 0), followingView!),
            (String(format: "followers_count".localized(), 0), followersView!)
            ])
        
        if isFollowers {
            scrollPager.setSelectedIndex(index: UserListType.followers.rawValue, animated: true)
        }
    }
    
    //MARK:- Load User ist
    func loadUserList() {
        followingView?.loadUserList(completion: { (count, error) in
            self.scrollPager.setTitleForTabIndex(title: String(format: "following_count".localized(), count), index: 0)
        })
        
        followersView?.loadUserList(completion: { (count, error) in
            self.scrollPager.setTitleForTabIndex(title: String(format: "followers_count".localized(), count), index: 1)
        })
    }
    
    
    override func refreshUI() {
        loadUserList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadUserList()
    }
    
   
    
    func initialSetUp() {
        setScreenTitle("friends".localized())
        if fromMenu {
            addHambergerButton()
        }else{
            addBackButton()
        }
        
        inviteView.addTapGesture(target: self, action: #selector(showInviteUserVC))
        qrView.addTapGesture(target: self, action: #selector(showQRCodeVC))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension FollowViewController : ScrollPagerDelegate {
    
    // MARK: - ScrollPagerDelegate -
    
    func scrollPager(scrollPager: ScrollPager, changedIndex: Int) {
    }
    
    func showInviteUserVC() {
        let inviteUserVC = storyboard?.instantiateViewController(withIdentifier: String.className(InviteUserViewController.self)) as! InviteUserViewController
        navigationController?.pushViewController(inviteUserVC, animated: true)
    }
    
    func showQRCodeVC() {
        let qrCodeVC = storyboard?.instantiateViewController(withIdentifier: String.className(MyQRCodeViewController.self)) as! MyQRCodeViewController
        navigationController?.pushViewController(qrCodeVC, animated: true)
    }
    
    func showUserProfileVC() {
        let sakeStoryBoard = UIStoryboard(name: StoryboardName.LeftMenu, bundle: nil)
        let profileVC = sakeStoryBoard.instantiateViewController(withIdentifier: String.className(UserProfileViewController.self)) as! UserProfileViewController
        navigationController?.pushViewController(profileVC, animated: true)
    }
    
}

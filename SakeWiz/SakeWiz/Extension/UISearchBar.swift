//
//  UISearchBar.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/23/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit


extension UISearchBar {
    
    
    /**
     Add custom accessory view to display when the UISearchBar becomes the first responder
     
     This method add a UIToolbar as a 'inputAccessoryView' with 'Done' button on the top of UISearchBar keyboard, when UISearchBar become first responder.
     
     
     :parameters:
     * target: - The object that recieves the action message.
     * action: - The action to send to target when this item is selected.
     
     */
    func addToolBar(target:Any?, action:Selector?) {
        
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 50))
        toolBar.barTintColor = .appThemColor()
        toolBar.isTranslucent = false
        toolBar.tintColor = .white
        
        toolBar.items = [
            
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: " ▼ ".localized(), style: .plain, target: target, action: action)]
        
        toolBar.sizeToFit()
        self.inputAccessoryView = toolBar
    }
}

//
//  UserDashboard.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class UserDashboard: Mappable {
    
    var userName: String?
    var userHandle: String?
    var userProfileImg: String?
    var favSakeCount: Int?
    var favBreweryCount: Int?
    var favBarCount: Int?
    var followerCount: Int?
    var followingCount: Int?
    var labelCount: Int?
    var pointsNeededForNextRank: Int?
    var wizPoints: Int?
    var userRank: String?
    var sakeIdentified: Int?
    var sakeUnidentified: Int?
    var notificationCount: Int?
    var recommendedProducts: [Product] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        userName <- map["userName"]
        userHandle <- map["userHandle"]
        userProfileImg <- map["userProfileImg"]
        favSakeCount <- map["favSakeCount"]
        favBreweryCount <- map["favBreweryCount"]
        favBarCount <- map["favBarCount"]
        followerCount <- map["followerCount"]
        followingCount <- map["followingCount"]
        labelCount <- map["labelCount"]
        pointsNeededForNextRank <- map["pointsNeededForNextRank"]
        wizPoints <- map["wizPoints"]
        userRank <- map["userRank"]
        sakeIdentified <- map["sakeIdentified"]
        sakeUnidentified <- map["sakeUnidentified"]
        notificationCount <- map["notificationCount"]
        recommendedProducts <- map["recommendedProducts"]
    }
        
}






//
//  WizUser.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import ObjectMapper


class WizUser: Mappable {
    
    var userId: String?
    var name: String?
    var userHandle: String?
    var imgUrl: String?
    var follows: Int?
    var followedBy: Int?
    var reviewCount: Int?
    var motto: String?
    var rank: String?
    var wizPoints: Int?
    var country: String?
    var state: String?
    var favProductCount: Int?
    var favPlaceCount: Int?
    var userIsFollowing: Bool = true
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userId <- map["userId"]
        name <- map["name"]
        userHandle <- map["userHandle"]
        imgUrl <- map["imgUrl"]
        follows <- map["follows"]
        followedBy <- map["followedBy"]
        reviewCount <- map["reviewCount"]
        motto <- map["motto"]
        rank <- map["rank"]
        wizPoints <- map["wizPoints"]
        country <- map["country"]
        state <- map["state"]
        favProductCount <- map["favProductCount"]
        favPlaceCount <- map["favPlaceCount"]
        userIsFollowing <- map["userIsFollowing"]
        
    }
    
    func setFollowed(followed: Bool) {
        userIsFollowing = followed
    }
    
    func isFollowed() -> Bool {
        return userIsFollowing
    }
    
    
}


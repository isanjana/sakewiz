//
//  Utils.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/11/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import Alamofire

class Utils: NSObject {
    
    class func getBase64Key(str: String) -> String {
        let base64Str = str.toBase64()
        let base64 = "Basic " + base64Str
        return base64
    }
    
    //MARK:- User Defaults
    class func setObjectInUserDefaults(_ value:Any , forKey key:String) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey:key)
        userDefaults.synchronize()
    }
    
    class func removeObjectFromDefaults(key:String) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: key)
        userDefaults.synchronize()
    }
    
    
    class func getAuthToken() -> String? {
        if let token = UserDefaults.standard.object(forKey: UserDefaultConstant.kAuthToken) as? String {
            return token
        }
        return nil
    }
    
    class func isUserLoggedIn() -> Bool {
        
        if let _ = Utils.getAuthToken() {
            return true
        }
        
        return false
    }
    
    class func getUserID() -> String? {
        if let id = UserDefaults.standard.object(forKey: UserDefaultConstant.kUserID) as? String {
            return id
        }
        return nil
    }
    
    class func getUserHandel() -> String? {
        if let hndl = UserDefaults.standard.object(forKey: UserDefaultConstant.kHNDL) as? String {
            return hndl
        }
        return nil
    }
    
    class func getUserPassword() -> String? {
        if let pwd = UserDefaults.standard.object(forKey: UserDefaultConstant.kPassword) as? String {
            return pwd
        }
        return nil
    }
    
    //MARK:- Locale
    class func getLanguageCode() -> String {
        
        if let currentLocale = UserDefaults.standard.object(forKey: UserDefaultConstant.kLocaleCode) as? String {
            return currentLocale
        }
        
        if let deviceLng = NSLocale.current.languageCode {
            return deviceLng
        }
        
        return LocaleConstant.ENGISH
    }
    
    class func changeLocale(locale:String) {
        
        switch locale {
        case LocaleConstant.CHINA:
            Utils.setObjectInUserDefaults(LocaleConstant.CHINA, forKey: UserDefaultConstant.kLocaleCode)
            break
            
        case LocaleConstant.JAPAN:
            Utils.setObjectInUserDefaults(LocaleConstant.JAPAN, forKey: UserDefaultConstant.kLocaleCode)
            break
            
        case LocaleConstant.SOUTH_KOREA:
             Utils.setObjectInUserDefaults(LocaleConstant.SOUTH_KOREA, forKey: UserDefaultConstant.kLocaleCode)
            break
            
        default:
            Utils.setObjectInUserDefaults(LocaleConstant.ENGISH, forKey: UserDefaultConstant.kLocaleCode)
            break
        }
        
    }
    
    class func getLocaleField(dict:[String:Any]) -> String {
        
        if dict.isEmpty {
            return ""
        }
        
        let languageCode = Utils.getLanguageCode()
        if let value = dict[languageCode] as? String {
            return value
        }
        
        for language in LocaleConstant.LANGUAGES {
            if let value = dict[language] as? String {
                return value
            }
        }
        
        return ""
    }
    
    //MARK:- Time Ago
   class func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!)y"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1y"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!)m"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1m"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)w"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1w"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)d"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1d"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)h"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1h"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!)m"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1m"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!)s"
        } else {
            return "now"
        }
        
    }
    
    
    //MARK:- UIKit Helpers
    //MARK:- Get Image From Color
    class func imageWithColor(color:UIColor) -> UIImage {
        
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }

}

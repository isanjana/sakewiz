//
//  RegisterViewModel.swift
//  SakeWiz
//
//  Created by sanjana on 11/09/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation

class RegisterViewModel {
    var username = ""
    var email = ""
    var password = ""
}

extension RegisterViewModel {
    
    func updateUsername(username: String) {
        self.username = username
    }
    
    func updateEmail(email: String) {
        self.email = email
    }
    
    func updatePassword(password: String) {
        self.password = password
    }
    
    func validate() -> UserValidationState {
        
        if !username.isValidHandle() {
            return .Invalid("handel_invalid".localized())
        }
        
        if !email.isValidEmail() {
            return .Invalid("email_invalid".localized())
        }
        
        if !password.isValidPassword() {
            return .Invalid("handel_invalid".localized())
        }
        
        return .Valid
    }
    
    func register(completion: @escaping (_ user: User?, _ error: APIError?) -> Void) {
        UserService.sharedInstance.register(username: username.lowercased(), password: password, email: email) { (user, error) in
            
            if let error = error {
                completion(nil, error)
                return;
            }
            
            if let user = user {
                completion(user, nil)
            }
            
        }
    }

}




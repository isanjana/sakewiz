//
//  PlaceDetailsView.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/18/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import SwiftyStarRatingView


@objc public protocol DetailsViewDelegate: NSObjectProtocol {
    @objc optional func favouriteDidClicked()
    @objc optional func addNoteDidClicked()
    @objc optional func viewNoteDidClicked()
    @objc optional func tasteChartDidClicked()
    
}

@IBDesignable class PlaceDetailsView: NibView {
    
    //MARK:- Properties
    @IBOutlet weak var placeDetailImageView: UIImageView!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var ratingCountLabel: UILabel!
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var viewNoteButton: CenteredButton!
    @IBOutlet weak var favButton: CenteredButton!
    public weak var delegate: DetailsViewDelegate!
    
    
    //MARK:- Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func updateView(viewModel:PlaceDetailsViewModel) {
        if case .brewery = viewModel.entityType {
            if let noteView = self.noteView {
                noteView.removeFromSuperview()
            }
        }
        else {
            viewNoteButton.isSelected = viewModel.hasUserNote
            viewNoteButton.isUserInteractionEnabled = viewModel.hasUserNote
        }
        
        if let imageUrl = viewModel.getPlaceImageUrl {
            placeDetailImageView.imageFromUrl(imageUrl)
        }
        placeNameLabel.text = viewModel.getPlaceName
        ratingView.value = CGFloat(viewModel.getAvgRate)
        ratingCountLabel.text = String(format: "%.1f", viewModel.getAvgRate)
        favButton.isSelected = viewModel.isFavourite
        
        
    }
    
    //MARK:- Favourite
    @IBAction func favouriteAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.favouriteDidClicked?()
    }
    
    //MARK:- View Note
    @IBAction func viewNoteAction(_ sender: UIButton) {
        delegate?.viewNoteDidClicked?()
    }
    

}

//
//  QRCodeScannerViewController.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/25/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import UIKit
import BSQRCodeReader
import AVFoundation

class QRCodeScannerViewController: BaseViewController {
    
    //MARK:- Properties
    @IBOutlet weak var qrCodeContainer: UIView!
    var scanLineAnimation: ScanLineAnimation?
    @IBOutlet weak var codeReader: BSQRCodeReader! {
        didSet {
            codeReader.delegate = self
            codeReader.backgroundColor = .black
        }
    }
    
    var userSearchViewModel = UserSearchViewModel()

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
        setScreenTitle("qr_code_scanner".localized())
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //Start Scanning
        startScanning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AudioPlayer.shared.releasePlayer()
        stopScanning()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension QRCodeScannerViewController : BSQRCodeReaderDelegate {
    
    //MARK:- BSQRCodeReaderDelegate
    func didCaptureQRCodeWithContent(_ content: String) -> Bool {
    
        // return true if you want to stop the scanning.
        // return false if you want to continue the scanning.
        
        if DEBUG.isDebuggingEnabled {print("QR_Content = \(content)")}
        AudioPlayer.shared.playSound()
        stopScanAnimation()
        showProgress(title: "")
        let hndl = content.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        var isError = false
        userSearchViewModel.searchUserByHandle(userHandle: hndl.lowercased()) { (error) in
            self.hideProgress()
            if error != nil {
                self.showBannerAlert(title: error!.getTitle(), message: error!.getMessage(), style: .warning, delegate: nil)
                self.startScanning()
                isError = true
            }
            if !isError {
                if let userID = self.userSearchViewModel.getUserId() {
                    EventBus.postToMainThread(TaskName.ShowUserProfileViewController, userInfo: ["id":userID])
                }
            }
            
        }
        
        return true
    }
    
    
    //MARK:- Helper Methods
    func startScanning() {
        stopScanning()
        codeReader.startScanning()
        startScanAnimation()
    }
    
    func stopScanning() {
        stopScanAnimation()
        codeReader.stopScanning()
    }
    
    func startScanAnimation() {
        if scanLineAnimation == nil {
            scanLineAnimation = ScanLineAnimation()
            scanLineAnimation!.startAnimating(parentView: codeReader, image:UIImage(named: "ScanLine"))
        }
    }
    
    func stopScanAnimation() {
        if (scanLineAnimation != nil) {
            scanLineAnimation!.stopStepAnimating()
            scanLineAnimation = nil
        }
    }
    
    func showUserProfileVC() {
        if let navController = self.navigationController {
            navController.popViewController(animated: false)
            let sakeStoryBoard = UIStoryboard(name: StoryboardName.LeftMenu, bundle: nil)
            let profileVC = sakeStoryBoard.instantiateViewController(withIdentifier: String.className(UserProfileViewController.self)) as! UserProfileViewController
            let userID = userSearchViewModel.getUserId()
            let userProfileViewModel = UserProfileViewModel(withUserId: userID)
            profileVC.userProfileViewModel = userProfileViewModel
            navController.pushViewController(profileVC, animated: true)
        }
        
    }
}


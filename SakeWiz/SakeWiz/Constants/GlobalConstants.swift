//
//  GlobalConstants.swift
//  SakeWiz
//
//  Created by Praveen Balpande on 9/5/17.
//  Copyright © 2017 Saaramobitech. All rights reserved.
//

import Foundation
import UIKit


// MARK: - Enums
enum UserListType: Int {
    case notSet = -1, following = 0, followers
}

enum EntityType: Int {
    case notSet = -1, sake = 0, brewery, bar
}

enum PlaceDetailsSection: Int {
    case placeDetailHeader = 0, recommend = 1, gallery, rating, availablity, reviews
}

// MARK: - DEBUG
struct DEBUG {
    static let isDebuggingEnabled            = true //make false for production
}


// MARK: - StoryboardName
struct StoryboardName {
    //Please don't rename these strings.
    static let MAIN                          = "Main"
    static let SignIn                        = "SignIn"
    static let Dashboard                     = "Dashboard"
    static let LeftMenu                      = "LeftMenu"
    static let PlaceDeatails                 = "PlaceDetails"
    static let UnknownSake                   = "UnknownSake"
    static let Favourite                     = "Favourite"
    
}

// MARK: - Menu Constant
struct ValueConstant {

    //SERVICE VALUES
    static let SEARCH_COUNT                  = 10
    static let UNKNOWN_SAKE_COUNT            = 10
    
    //UIKIT VALUES
    static let DEFAULT_RADIUS:CGFloat        = 5
    
}



// MARK: - Menu Constant
struct MenuConstant {
    static let Dashboard                  = 1
    static let Search                     = 2
    static let Profile                    = 3
    static let Followers                  = 4
    static let Private_Notes              = 5
    static let Saved_Sake                 = 6
    static let News_Feed                  = 7
    static let My_Purchases               = 8
    static let Settings                   = 9
    static let Logout                     = 10
}


// MARK: - User Default Constant
struct UserDefaultConstant {
    static let kAuthToken                = "kAuthToken"
    static let kLocaleCode               = "kLocaleCode"
    static let kUserID                   = "kUserID"
    static let kHNDL                     = "kHNDL"
    static let kPassword                 = "kPassword"
}

// MARK: - ServiceConstant
struct ServiceConstant {
    static let X_AUTH_TOKEN              = "x-auth-token"
    static let SYSTEM_USER               = "system-user"
    static let PASSWORD                  = "abc123"
    static let EMAIL                     = "email"
    static let HNDL                      = "hndl"
    static let PWD                       = "pwd"
    static let LANG                      = "lang"
    static let USER_HANDEL               = "userHandle"
    static let ERROR_KEY                 = "error"
    static let ERROR_VALUE               = "Error"
    static let MESSAGE                   = "message"
    static let AUTHORIZATION             = "Authorization"
    static let ACCEPT_KEY                = "Accept"
    static let ACCEPT_VALUE              = "application/json"
    static let X_SECURITY_HEADRE         = "x-security-header"
    static let SECURITY_HEADRE_VALUE     = "8550f3c5-c4da-4137-94e5-ff113fac145f"
    static let RATE                      = "rate"
    static let COMMENT                   = "comment"
    static let TEXT                      = "text"
    static let COUNT                     = "count"
}

struct APIEndPointConstant {
    static let BASE_URL                  = "https://api.developer.sakewiz.com"
    static let LOGIN                     = "/user/login"
    static let REGISTER                  = "/user"
    static let RESET_PASSWORD            = "/account/password/reset/notify"
    static let LOGIN_PROFILE             = "/user"
    static let DASHBOARD                 = "/facade/user"
    
   
}

// MARK: - Locale Constant
struct LocaleConstant {
    static let CHINA                     = "za"
    static let JAPAN                     = "ja"
    static let SOUTH_KOREA               = "ko"
    static let ENGISH                    = "en"
    static let LANGUAGES                 = ["JAPAN","UNITED_KINGDOM","CHINA","SOUTH_KOREA"]
    static let LANGUAGES_CODE            = ["ja", "en", "zh", "ko"]
}

// MARK: - Locale Constant
struct ImageUrlConstant {
    
    static let BASE_IMAGE_URL = "https://s3-ap-northeast-1.amazonaws.com/com.sakewiz.images"
    static let RANK_IMAGE_URL = "https://s3-ap-northeast-1.amazonaws.com/com.sakewiz.static/images/user-rank/30x30/{rank}.png"
    static let SCAN_IMAGE_URL = "https://s3-ap-northeast-1.amazonaws.com/com.sakewiz.images.scans"
    static let USERAFFLTCODE_IMAGE_URL = "https://s3-ap-northeast-1.amazonaws.com/com.sakewiz.static/images/affltuser/{userAffltCode}.png"
    
}



